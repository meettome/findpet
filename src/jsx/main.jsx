import axios from 'axios';

const valFromPath = (obj, path) =>{

	if(/\+/g.test(path)){
		let fields = path.split(/\+/g);
		let res=[];

		fields.forEach(field=>{
			let levels = field.split(/\./g);

			if(levels.length>1)
				res.push(valFromPath(obj[levels.shift()],levels.join('.')));
			else
				res.push(obj[levels.join('.')]);
		});

		return res.join(' - ');
	} else {
		let levels=path.split(/\./ig);
		if(levels.length>1)
			return valFromPath(obj[levels.shift()],levels.join('.'));
		else
			return obj[path];
	}

}

let dynSelects = Array.from(document.querySelectorAll('select.dynamic'));

const updateDynamicSelects = (selectGroup) => {
	selectGroup.forEach(sel=>{
		let opts={
			endpoint: sel.getAttribute('data-endpoint'),
			labelField: sel.getAttribute('data-labelfield'),
			valueField: sel.getAttribute('data-valuefield'),
			currentValue: sel.getAttribute('data-current'),
			api_token: sel.closest('form').getAttribute('data-api_token'),
			target: sel.getAttribute('data-targetid'),
			updateEvent: sel.getAttribute('data-updateevent'),
			updateEventType: sel.getAttribute('data-updateeventtype')
		};

		if(opts.updateEvent){
			let target = document.getElementById(opts.target);
			if(target){
				switch(opts.updateEvent){
					case "updateCitySelect":
					sel.addEventListener('change',(event) =>{
						let dynSelects = document.getElementById('filterCity');
						if(event.target.value>0){
							dynSelects.setAttribute('data-endpoint',`api/cities/${event.target.value}`);
						} else {
							dynSelects.removeAttribute('data-endpoint');
						}
						updateDynamicSelects([dynSelects]);
					});
					break;
				}
				target.setAttribute('data-endpoint',`api/cities/${opts.currentValue}`);
			}

		}

		if(opts.updateEventType){
			let target = document.getElementById(opts.target);
			if(target){
				switch(opts.updateEventType){
					case "updateBreedSelect":
					sel.addEventListener('change',(event) =>{
						let dynSelects = document.getElementById('filterBreed');
						if(event.target.value>0){
							dynSelects.setAttribute('data-endpoint',`api/breed/${event.target.value}`);
						} else {
							dynSelects.removeAttribute('data-endpoint');
						}
						updateDynamicSelects([dynSelects]);
					});
					
					break;
				}
				target.setAttribute('data-endpoint',`api/breed/${opts.currentValue}`);
			}

		}

		if(opts.endpoint){
			axios.get(`/${opts.endpoint}`,{
				headers: {
					'Authorization': `Bearer ${opts.api_token}`
				}
			}).then(response=>{
				let data = response.data;
				Array.from(sel.querySelectorAll('option')).filter(opt=>!['','0'].includes(opt.getAttribute('value'))).forEach(opt=>opt.remove());
				for(let [indx,entry] of data.entries()){
					let opt = document.createElement('option');
					opt.setAttribute('value',valFromPath(entry,opts.valueField));
					if(opts.currentValue == valFromPath(entry,opts.valueField))
						opt.setAttribute('selected','');
					opt.textContent = valFromPath(entry,opts.labelField);
					sel.appendChild(opt);
				}
			}).catch(error=>{
				console.log(error);
			});
		} else
		Array.from(sel.querySelectorAll('option')).filter(opt=>opt.getAttribute('value')!=0).forEach(opt=>opt.remove());
	});
}

updateDynamicSelects(dynSelects);

Array.from(document.querySelectorAll("form.filter")).forEach(frm=>{
	let resetBtn = frm.querySelector('input[data-action="reset"]');
	if(resetBtn)
		resetBtn.addEventListener('click',e=>{
			e.preventDefault();

			window.location.href = window.location.pathname;
		});
});

/*
<div class="row">
	<div class="col-md-5 form-group">
		<label class="form-control-label" for="input-ddd-{{$indx}}">{{ __('ui.fields.ddd') }}</label>
		<input type="number" name="ddd[]" id="input-ddd-{{$indx}}" class="form-control form-control-alternative" placeholder="{{ __('DDD') }}" value="{{ $phone['ddd'] }}" required>
	</div>
	<div class="col-md-5 form-group">
		<label class="form-control-label" for="input-phone-{{$indx}}">{{ __('ui.fields.phone') }}</label>
		<input type="number" name="phone[]" id="input-phone-{{$indx}}" class="form-control form-control-alternative" placeholder="{{ __('Número') }}" value="{{ $phone['number'] }}" required>
	</div>
	<div class="col-md-2 phone-btn">
		<i class="fas fa-trash" data-action="delete"></i>
	</div>
</div>

*/
Array.from(document.querySelectorAll(".btn[data-action]")).forEach(btn=>{
	switch(btn.getAttribute('data-action')){
		case "addPhone":
		btn.addEventListener('click',(evt)=>{
			evt.preventDefault();

			let form = btn.closest('form');
			let container = form.querySelector('.row.phones > div');

			let row = document.createElement('div');
			let dddDiv = document.createElement('div');
			let dddLabel = document.createElement('label');
			let dddInput = document.createElement('input');

			let index = parseInt(form.getAttribute('data-phonecount'));

			let numberDiv = document.createElement('div');
			let numberLabel = document.createElement('label');
			let numberInput = document.createElement('input');

			let btnDiv = document.createElement('div');
			let trashA = document.createElement('a');

			index++;

			form.setAttribute('data-phonecount',index);

			row.setAttribute('class','row phone');

			dddDiv.setAttribute('class', 'col-md-5 form-group');
			dddLabel.setAttribute('class', 'form-control-label');
			dddLabel.setAttribute('for', `input-ddd-${index}`);
			dddLabel.textContent = 'DDD';

			dddInput.setAttribute('type', 'number');
			dddInput.setAttribute('name', 'ddd[]');
			dddInput.setAttribute('id', `input-ddd-${index}`);
			dddInput.setAttribute('class', 'form-control form-control-alternative');
			dddInput.setAttribute('placeholder', 'DDD');
			dddInput.setAttribute('required', '');

			numberDiv.setAttribute('class', 'col-md-5 form-group');
			numberLabel.setAttribute('class', 'form-control-label');
			numberLabel.setAttribute('for', `input-ddd-${index}`);
			numberLabel.textContent = 'Número';

			numberInput.setAttribute('type', 'number');
			numberInput.setAttribute('name', 'phone[]');
			numberInput.setAttribute('id', `input-number-${index}`);
			numberInput.setAttribute('class', 'form-control form-control-alternative');
			numberInput.setAttribute('placeholder', 'Número');
			numberInput.setAttribute('required', '');

			btnDiv.setAttribute('class', 'col-md-2 phone-btn');
			trashA.setAttribute('class', 'fas fa-trash');
			trashA.setAttribute('data-action', 'delete');

			dddDiv.appendChild(dddLabel);
			dddDiv.appendChild(dddInput);

			numberDiv.appendChild(numberLabel);
			numberDiv.appendChild(numberInput);

			btnDiv.appendChild(trashA);

			row.appendChild(dddDiv);
			row.appendChild(numberDiv);
			row.appendChild(btnDiv);

			container.appendChild(row);
		});
		break;
	}
});


$(document).ready(function(){
	$('.main-content').on({
		click: function(evt){
			evt.preventDefault();

			$(this).parents('form').attr('data-phonecount', $(this).parents('form').attr('data-phonecount') - 1 );
			$(this).parents('.row.phone').remove();
		}
	},'form .phone-btn a[data-action="delete"]');

	$('.main-content').on({
		click:function(evt){
			evt.preventDefault();

			let form = $(this).parents('form')
			let actInput = $(form).find('input[type="hidden"][name="action"]');

			if($(this).attr('data-action')=='reset'){
				$(form).reset();
			}
			if($(this).attr('data-action'))
				$(actInput).val($(this).attr('data-action'));

			$(form).submit();
		}
	},'.buttonBar input[type="submit"]');

	$('form input.cpf').mask('000.000.000-00', {reverse: false});
});
