<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Arr;

trait HasChildModel {
	public function newFromBuilder($attributes = [], $connection = null)
	{
		$morphMap = Relation::morphMap();
		$entryType = Arr::get((array)$attributes, 'type');
		if(class_exists($morphMap[$entryType]) && is_subclass_of($morphMap[$entryType], self::class)){
			$model = new $morphMap[$entryType];
		} else {
			$model = $this->newInstance();
		}

		$model->exists = true;
		$model->setRawAttributes((array) $attributes, true);
		$model->setConnection($connection ?? $this->connection);

		return $model;
	}

	protected static function boot()
	{
		parent::boot();

		static::creating(function($query){
			$morphMap = array_flip(Relation::morphMap());
			var_dump($morphMap[get_called_class()]);
			$query->type = $morphMap[get_called_class()];
		});
	}
}
