<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Str;

trait HasParentModel
{
	public function getTable()
	{
		if (! isset($this->table)) {
			return str_replace('\\', '', Str::snake(Str::plural(class_basename($this->getParentClass()))));
		}
		return $this->table;
	}

	public function getForeignKey()
	{
		return Str::snake(class_basename($this->getParentClass())).'_'.$this->primaryKey;
	}

	public function joiningTable($related, $instance = NULL)
	{
		$models = [
			Str::snake(class_basename($related)),
			Str::snake(class_basename($this->getParentClass())),
		];
		sort($models);
		return strtolower(implode('_', $models));
	}

	protected function getParentClass()
	{
		return (new \ReflectionClass($this))->getParentClass()->getName();
	}

	public function getMorphClass()
	{
		$morphMap = array_flip(Relation::morphMap());
		if(isset($morphMap[$this->getParentClass()]))
			return $morphMap[$this->getParentClass()];
		else
			return $this->getParentClass();
	}
}
