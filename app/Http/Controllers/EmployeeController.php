<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Models\Ong;
use App\Http\Requests\EmployeeRequest;
use Illuminate\Support\Facades\Mail;
use App\Models\Clinic;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        if(auth()->user()->role=="ong"){
            $ong = Ong::where('user_id', '=', auth()->user()->id)->get();

            $users = User::orderby('users.name', 'asc')->join('ong_user', 'ong_user.user_id', '=', 'users.id')->where('ong_user.ong_id', '=', $ong[0]->id)->paginate(15);
        }else{
            $clinic = Clinic::where('user_id', '=', auth()->user()->id)->get();
            
            $users = User::orderby('users.name', 'asc')->join('clinic_user', 'clinic_user.user_id', '=', 'users.id')->where('clinic_user.clinic_id', '=', $clinic[0]->id)->paginate(15);
        }
        return view('employee.users.index', ['users' => $users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('employee.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    
    public function store(EmployeeRequest $request)
    {
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->role = 'employee';
        $user->password = Hash::make($request->password);
        $user->save();

        if(auth()->user()->role == "ong"){
            $ong = Ong::where('user_id', '=', auth()->user()->id)->get();
            $name = $ong[0]->name;
            $user->ongs()->sync([$ong[0]->id]);
        }else{

            $clinic = Clinic::where('user_id', '=', auth()->user()->id)->get();
            $name = $clinic[0]->name;
            $user->clinics()->sync([$clinic[0]->id]);
        }
        Mail::send(new \App\Mail\ConfirmationUser($request));

        return redirect()->route('empregado.index')->withStatus(__('Usuario adicionado com Sucesso.'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $data = [
            "user" => $user
        ];
        return view('employee.users.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->save();

        return redirect()->route('empregado.index')->withStatus(__('Usuario Editado com Sucesso.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function destroy(Request $request)
    {
        $user = User::find($request->id);
        $user->delete();
        return redirect()->route('empregado.index');
    }
}
