<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Chat;
use App\Models\Chat_Conversation;
use App\Models\Chat_Participation;
use App\Models\Chat_Message;
use App\Models\Chat_Message_Notification;
use App\Models\User;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Arr;
class ChatController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $temconversa = Chat_Participation::where('messageable_id', '=', auth()->user()->id)->get();
        $i=0;
        foreach ($temconversa as $covuser) {
            $temconversaDono = Chat_Participation::where('messageable_id', '=', $request->id)->where('conversation_id', '=', $covuser->conversation_id)->get();
            if(sizeof($temconversaDono)>0) {
                $i++;
                $idconversation = $covuser->conversation_id;
                break;
            }
        }
        $usuario = auth()->user();
        $dono = User::find($request->id);
        if($i==0){
            $conversations = new Chat_Conversation;
            $conversations->private = 1;
            $conversations->data = [];
            $conversations->save();
            $participantes = new Chat_Participation;
            $participantes->conversation_id = $conversations->id;
            $usuario->participant()->save($participantes);
            $participant = new Chat_Participation;
            $participant->conversation_id = $conversations->id;
            $dono->participant()->save($participant);

            $mensagens = Chat_Message::select(DB::raw('users.name, users.id, chat__messages.body, chat__messages.created_at'))
            ->join('chat__participations', function ($join) {
                $join->on('chat__participations.id', '=', 'chat__messages.participation_id');
            })
            ->join('users', function ($join) {
                $join->on('chat__participations.messageable_id', '=', 'users.id');
            })
            ->orderby('chat__messages.id', 'asc')
            ->where('chat__messages.conversation_id', '=', $conversations->id)
            ->get();
            $conversation_id = $conversations->id;
        } else{
            $mensagens = Chat_Message::select(DB::raw('users.name, users.id, chat__messages.body, chat__messages.created_at'))
            ->join('chat__participations', function ($join) {
                $join->on('chat__participations.id', '=', 'chat__messages.participation_id');
            })
            ->join('users', function ($join) {
                $join->on('chat__participations.messageable_id', '=', 'users.id');
            })
            ->orderby('chat__messages.id', 'asc')
            ->where('chat__messages.conversation_id', '=', $idconversation)
            ->get();
            $conversation_id = $idconversation;
        }
        $participacao = Chat_Participation::where('messageable_id', '=', auth()->user()->id)->where('conversation_id', '=', $conversation_id)->get();

        $notificacao =  Chat_Message_Notification::where([['conversation_id', '=', $conversation_id], ['is_seen', '=', '0'], ['participation_id', '=', $participacao[0]->id]])->get();
        foreach ($notificacao as $not) {
            $movimentacao = Chat_Message_Notification::find($not->id);
            $movimentacao->is_seen = 1;
            $movimentacao->save();
        }
        return view('chat.chat', ['mensagem' => $mensagens, "dono"=>$dono, "conversation_id"=>$conversation_id]);
    }	
    public function enviamensagem(request $request){

        $participacao = Chat_Participation::where('messageable_id', '=', auth()->user()->id)->where('conversation_id', '=', $request->conversation_id)->get();

        $participacaodono = Chat_Participation::where('messageable_id', '=', $request->dono)->where('conversation_id', '=', $request->conversation_id)->get();

        $notify =  Chat_Message_Notification::where([['conversation_id', '=', $request->conversation_id], ['is_seen', '=', '0'], ['participation_id', '=', $participacao[0]->id]])->get();
        foreach ($notify as $not) {
            $movimentacao = Chat_Message_Notification::find($not->id);
            $movimentacao->is_seen = 1;
            $movimentacao->save();
        }
        $chat = new Chat_Message();
        $chat->body = $request->mensagem;
        $chat->type = "text";
        $chat->participation_id = $participacao[0]->id;
        $chat->conversation_id = $request->conversation_id;
        $chat->save();

        $notificacao = new Chat_Message_Notification;
        $notificacao->message_id = $chat->id;
        $notificacao->conversation_id = $request->conversation_id;
        $notificacao->participation_id = $participacao[0]->id;
        $notificacao->is_seen = 1;
        $notificacao->is_sender = 1;
        auth()->user()->notificacoes()->save($notificacao);
        
        $dono = User::find($request->dono);

        
        $notificacaodono = new Chat_Message_Notification;
        $notificacaodono->message_id = $chat->id;
        $notificacaodono->conversation_id = $request->conversation_id;
        $notificacaodono->participation_id = $participacao[0]->id;
        $notificacaodono->is_seen = 0;
        $notificacaodono->is_sender = 0;
        $dono->notificacoes()->save($notificacaodono);

        $mensagens = Chat_Message::select(DB::raw('users.name, users.id, chat__messages.body, chat__messages.created_at'))
        ->join('chat__participations', function ($join) {
            $join->on('chat__participations.id', '=', 'chat__messages.participation_id');
        })
        ->join('users', function ($join) {
            $join->on('chat__participations.messageable_id', '=', 'users.id');
        })
        ->orderby('chat__messages.id', 'asc')
        ->where('chat__messages.conversation_id', '=', $request->conversation_id)
        ->get();
        $conversation_id = $request->conversation_id;




        return view('chat.chat', ['mensagem' => $mensagens, "dono"=>$dono, "conversation_id"=>$conversation_id]);
    }

    
    public function show(Request $request)
    {
        $conversa = Chat_Participation::where([['messageable_id', '<>', auth()->user()->id],['conversation_id', '=', $request->id]])->get();
        $usuario = auth()->user();
        $dono = User::find($conversa[0]->messageable_id);

        $mensagens = Chat_Message::select(DB::raw('users.name, users.id, chat__messages.body, chat__messages.created_at'))
        ->join('chat__participations', function ($join) {
            $join->on('chat__participations.id', '=', 'chat__messages.participation_id');
        })
        ->join('users', function ($join) {
            $join->on('chat__participations.messageable_id', '=', 'users.id');
        })
        ->orderby('chat__messages.id', 'asc')
        ->where('chat__messages.conversation_id', '=', $request->id)
        ->get();
        $conversation_id = $request->id;
        
        $participacao = Chat_Participation::where('messageable_id', '=', auth()->user()->id)->where('conversation_id', '=', $conversation_id)->get();

        $notificacao =  Chat_Message_Notification::where([['conversation_id', '=', $conversation_id], ['is_seen', '=', '0'], ['participation_id', '=', $participacao[0]->id]])->get();
        foreach ($notificacao as $not) {
            $movimentacao = Chat_Message_Notification::find($not->id);
            $movimentacao->is_seen = 1;
            $movimentacao->save();
        }
        return view('chat.chat', ['mensagem' => $mensagens, "dono"=>$dono, "conversation_id"=>$conversation_id]);
    }   


}
