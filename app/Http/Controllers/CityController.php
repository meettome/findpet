<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\City;
use Illuminate\Http\Request;

class CityController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(City $model, Request $request)
	{
		$retVars = [
			'cities' => ($request->has('action') && $request->action=='filter')?$model->filter($request)->with(['state'])->orderBy('state_id','ASC')->orderBy('name','ASC')->paginate(15):$model->with(['state'])->orderBy('name','ASC')->paginate(15),
		];

		//var_dump($retVars);
		//echo $request->name;

		if($request->has('state_id') && $request->state_id>0)
			$retVars['state_id'] = $request->state_id;

		return view('city.index', $retVars);
		//return view('city.index', ['cities' => $model->with('state')->orderBy('name','ASC')->paginate(15)]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		return view('city.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request, City $model)
	{
		$model = City::create($request);
		redirect()->route('city.index')->withStatus(__('Cidade adicionada com sucesso.'));
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit(City $city)
	{
		return view('city.edit', compact('city'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, City $city)
	{
		$city->update($request);
		return redirect()->route('city.index')->withStatus(__('Cidade atualizada com sucesso.'));
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		//
	}
}
