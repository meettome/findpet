<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Animal;

class AdoptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function search(Request $request){


        if(sizeof(auth()->user()->employeeong) > 0){
            $owner_id = auth()->user()->employeeong[0]->user_id;
        }else if(sizeof(auth()->user()->employeeclinic) > 0){
            $owner_id = auth()->user()->employeeclinic[0]->user_id;
        }else{
            $owner_id = auth()->user()->id;
        }
        
        if($request->breed_id != null || $request->breed_id != "")
            $animais = Animal::where([['animals.owner_id', '<>', $owner_id], ['animals.type_id', '=', $request->type_id], ['animals.breed_id', '=', $request->breed_id]])->orderby('animals.name', 'asc')->get();
        else $animais = Animal::where([['animals.owner_id', '<>', $owner_id], ['animals.type_id', '=', $request->type_id]])->orderby('animals.name', 'asc')->get();

        return view('adopter.index', ['animals' => $animais, 'title' => 'Animais para adoção', 'breed_id' => $request->breed_id, 'type_id' => $request->type_id]);
    }


    public function index()
    {
        if(sizeof(auth()->user()->employeeong) > 0){
            $owner_id = auth()->user()->employeeong[0]->user_id;
        }else if(sizeof(auth()->user()->employeeclinic) > 0){
            $owner_id = auth()->user()->employeeclinic[0]->user_id;
        }else{
            $owner_id = auth()->user()->id;
        }
        $animais = Animal::where('animals.owner_id', '<>', $owner_id)->orderby('animals.name', 'asc')->get();
        return view('adopter.index', ['animals' => $animais, 'title' => 'Animais para adoção']);
    }


    public function dog(){
        if(sizeof(auth()->user()->employeeong) > 0){
            $owner_id = auth()->user()->employeeong[0]->user_id;
        }else if(sizeof(auth()->user()->employeeclinic) > 0){
            $owner_id = auth()->user()->employeeclinic[0]->user_id;
        }else{
            $owner_id = auth()->user()->id;
        }
        $animais = Animal::orderby('animals.name', 'asc')->where([
            ['animals.owner_id', '<>', $owner_id],
            ['animals.type_id', '=', '1']
        ])->get();
        return view('adopter.index', ['animals' => $animais, 'title' => 'Cachorros para Adoção']);
    }
    public function cat(){
        if(sizeof(auth()->user()->employeeong) > 0){
            $owner_id = auth()->user()->employeeong[0]->user_id;
        }else if(sizeof(auth()->user()->employeeclinic) > 0){
            $owner_id = auth()->user()->employeeclinic[0]->user_id;
        }else{
            $owner_id = auth()->user()->id;
        }
        $animais = Animal::orderby('animals.name', 'asc')->where([
            ['animals.owner_id', '<>', $owner_id],
            ['animals.type_id', '=', '2']
        ])->get();
        return view('adopter.index', ['animals' => $animais, 'title' => 'Gatos para Adoção']);
    }
    public function rodents(){
        if(sizeof(auth()->user()->employeeong) > 0){
            $owner_id = auth()->user()->employeeong[0]->user_id;
        }else if(sizeof(auth()->user()->employeeclinic) > 0){
            $owner_id = auth()->user()->employeeclinic[0]->user_id;
        }else{
            $owner_id = auth()->user()->id;
        }
        $animais = Animal::orderby('animals.name', 'asc')->where([
            ['animals.owner_id', '<>', $owner_id],
            ['animals.type_id', '=', '3']
        ])->get();
        return view('adopter.index', ['animals' => $animais, 'title' => 'Roedores para Adoção']);
    }
    public function rabbits(){
        if(sizeof(auth()->user()->employeeong) > 0){
            $owner_id = auth()->user()->employeeong[0]->user_id;
        }else if(sizeof(auth()->user()->employeeclinic) > 0){
            $owner_id = auth()->user()->employeeclinic[0]->user_id;
        }else{
            $owner_id = auth()->user()->id;
        }
        $animais = Animal::orderby('animals.name', 'asc')->where([
            ['animals.owner_id', '<>', $owner_id],
            ['animals.type_id', '=', '4']
        ])->get();
        return view('adopter.index', ['animals' => $animais, 'title' => 'Coelhos para Adoção']);
    }
    public function birds(){
        if(sizeof(auth()->user()->employeeong) > 0){
            $owner_id = auth()->user()->employeeong[0]->user_id;
        }else if(sizeof(auth()->user()->employeeclinic) > 0){
            $owner_id = auth()->user()->employeeclinic[0]->user_id;
        }else{
            $owner_id = auth()->user()->id;
        }
        $animais = Animal::orderby('animals.name', 'asc')->where([
            ['animals.owner_id', '<>', $owner_id],
            ['animals.type_id', '=', '5']
        ])->get();
        return view('adopter.index', ['animals' => $animais, 'title' => 'Aves para Adoção']);
    }
    public function turtles(){
        if(sizeof(auth()->user()->employeeong) > 0){
            $owner_id = auth()->user()->employeeong[0]->user_id;
        }else if(sizeof(auth()->user()->employeeclinic) > 0){
            $owner_id = auth()->user()->employeeclinic[0]->user_id;
        }else{
            $owner_id = auth()->user()->id;
        }
        $animais = Animal::orderby('animals.name', 'asc')->where([
            ['animals.owner_id', '<>', $owner_id],
            ['animals.type_id', '=', '6']
        ])->get();
        return view('adopter.index', ['animals' => $animais, 'title' => 'Tartaruga, Jabutis e Cágados para Adoção']);
    }
    public function furao(){
        if(sizeof(auth()->user()->employeeong) > 0){
            $owner_id = auth()->user()->employeeong[0]->user_id;
        }else if(sizeof(auth()->user()->employeeclinic) > 0){
            $owner_id = auth()->user()->employeeclinic[0]->user_id;
        }else{
            $owner_id = auth()->user()->id;
        }
        $animais = Animal::orderby('animals.name', 'asc')->where([
            ['animals.owner_id', '<>', $owner_id],
            ['animals.type_id', '=', '7']
        ])->get();
        return view('adopter.index', ['animals' => $animais, 'title' => 'Furões para Adoção']);
    }
}
