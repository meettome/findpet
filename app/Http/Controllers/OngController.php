<?php

namespace App\Http\Controllers;

use App\Models\Ong;
use Illuminate\Http\Request;

class OngController extends Controller
{

    protected function showOng(){
        return view('auth.ong_register', ['role' => 'ong']);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ongs = Ong::orderby('name', 'asc')->paginate(15);

        // $whatsapp = $ongs->SocialNetworks->where("type_id", "=", "17");
        return view('ongcadastro', ['ongs' => $ongs]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Ong  $ong
     * @return \Illuminate\Http\Response
     */
    public function show($id, $nome)
    {
        $ongs = Ong::find($id);
        $whatsapp = $ongs->SocialNetworks->where("type_id", "=", "17");
        return view('ongdetails', ['ongs' => $ongs, 'whatsapp'=>$whatsapp]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Ong  $ong
     * @return \Illuminate\Http\Response
     */
    public function edit(Ong $ong)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Ong  $ong
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ong $ong)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Ong  $ong
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ong $ong)
    {
        //
    }
}
