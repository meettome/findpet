<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the users
     *
     * @param  \App\User  $model
     * @return \Illuminate\View\View
     */
    public function index(User $model)
    {

        if(auth()->user()->role == "admin")
            $view = view("admin.users.index", ['users' => $model->where('role', '<>', 'employee')->paginate(15)]);
        else  
            $view = redirect()->route('home');
        return $view;
    }

    /**
     * Show the form for creating a new user
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        
        if(auth()->user()->role == "admin")
            $view = view("admin.users.create");
        else  
            $view = redirect()->route('home');
        return $view;
    }

    /**
     * Store a newly created user in storage
     *
     * @param  \App\Http\Requests\UserRequest  $request
     * @param  \App\User  $model
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(UserRequest $request, User $model)
    {
        $model->name = $request->name;
        $model->email = $request->email;
        $model->role = $request->role;
        $model->password = Hash::make($request->password);
        $model->save();

        return redirect()->route('usuario.index')->withStatus(__('Usuario criado com Sucesso.'));
    }

    /**
     * Show the form for editing the specified user
     *
     * @param  \App\User  $user
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $user = User::find($id);
        if(auth()->user()->role == "admin")
            $view = view("admin.users.edit", compact('user'));
        else  
            $view = redirect()->route('home');
        return $view;
    }

    /**
     * Update the specified user in storage
     *
     * @param  \App\Http\Requests\UserRequest  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $model = User::find($id);
        $model->name = $request->name;
        $model->email = $request->email;
        $model->role = $request->role;
        $model->password = Hash::make($request->password);
        $model->save();

        return redirect()->route('usuario.index')->withStatus(__('Usuario atualizado com Sucesso.'));
    }

    /**
     * Remove the specified user from storage
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $user = User::find($id);
        if($user->role=="vet"){
            $clinica=$user->clinicsUsuario;
            if(sizeof($clinica->users)>0){
                foreach($clinica->users as $usuarios){
                    $usuarios->delete();
                }
            }
            $clinica->delete();
        }else if($user->role=="ong"){
            $ong=$user->ongsUsuario;
            if(sizeof($ong->users)>0){
                foreach($ong->users as $usuarios){
                    $usuarios->delete();
                }
            }
            $ong->delete();
        }
        $user->delete();

        return redirect()->route('usuario.index')->withStatus(__('Usuario Deletado com sucesso.'));
    }
}
