<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Social_Network;
use App\Models\Avatar;
use App\Models\Ong;
use App\Models\Address;
use App\Http\Requests\ProfileRequest;
use App\Http\Requests\AvatarRequest;
use App\Http\Requests\PasswordRequest;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Ramsey\Uuid\Uuid;

class ProfileController extends Controller
{
    /**
     * Show the form for editing the profile.
     *
     * @return \Illuminate\View\View
     */
    public function edit(){
        if(auth()->user()->role=='ong'){
            $view = 'ong.profile.edit';
        }else if(auth()->user()->role=='vet'){
            $view = 'clinic.profile.edit';
        }else if(auth()->user()->role=='employee'){
            $view = 'employee.profile.edit';
        }else{
            $view = 'profile.edit';
        }
        return view($view);
    }

    /**
     * Update the profile
     *
     * @param  \App\Http\Requests\ProfileRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request)
    {
        $user = auth()->user();
        $user->name=$request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->phone = $request->cellphone;
        $user->birth_date = $request->birth_date;
        $user->cpf = $request->cpf;
        $user->sex = $request->sex;
        $user->save();
        if($request->zip_code != null){
            if(sizeof($user->find($user->id)->addresses)>0){
                $address = [
                    'zip_code' => $request->zip_code ? preg_replace('/[^0-9]/', '', $request->zip_code) : null,
                    'street' => $request->street,
                    'number' => $request->number,
                    'district' => $request->district,
                    'complement' => $request->complement,
                    'city_id' => $request->city_id,
                    'state_id' => $request->state_id
                ];
                $user->addresses()->update($address);
            }else{
                $address = new Address;
                $address->zip_code = $request->zip_code ? preg_replace('/[^0-9]/', '', $request->zip_code) : null;
                $address->street = $request->street;
                $address->number = $request->number;
                $address->district = $request->district;
                $address->complement = $request->complement;
                $address->city_id = $request->city_id;
                $address->state_id = $request->state_id;
                $user->addresses()->save($address);
            }
        }else{
            if(sizeof($user->find($user->id)->addresses)>0){
                $user->addresses()->delete();
            }
        }

        return back()->withStatus(__('Perfil Atualizado com Sucesso.'));
    }
    public function updateEmployee(Request $request)
    {
        $user = auth()->user();
        $user->name=$request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->phone = $request->cellphone;
        $user->birth_date = $request->birth_date;
        $user->cpf = $request->cpf;
        $user->sex = $request->sex;
        $user->save();
        return back()->withStatus(__('Perfil Atualizado com Sucesso.'));
    }
    public function updateOng(Request $request)
    {
        $user = auth()->user();
        $user->name=$request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->phone = $request->cellphone;
        $user->birth_date = $request->birth_date;
        $user->save();
        $ong = $user->ongsUsuario;
        $ong->name = $request->name;
        $ong->email = $request->email;
        $ong->cnpj = $request->cnpj;
        $ong->site = $request->site;
        $ong->phone = $request->telphone;
        $ong->cellphone = $request->cellphone;
        $ong->save();

        if($request->zip_code != null){
            if(sizeof($user->find($user->id)->addresses)>0 && sizeof($ong->find($ong->id)->addresses)>0){
                $address = [
                    'zip_code' => $request->zip_code ? preg_replace('/[^0-9]/', '', $request->zip_code) : null,
                    'street' => $request->street,
                    'number' => $request->number,
                    'district' => $request->district,
                    'complement' => $request->complement,
                    'city_id' => $request->city_id,
                    'state_id' => $request->state_id
                ];
                $user->addresses()->update($address);
                $ong->addresses()->update($address);
            }else{
                $address = new Address;
                $address->zip_code = $request->zip_code ? preg_replace('/[^0-9]/', '', $request->zip_code) : null;
                $address->street = $request->street;
                $address->number = $request->number;
                $address->district = $request->district;
                $address->complement = $request->complement;
                $address->city_id = $request->city_id;
                $address->state_id = $request->state_id;
                $user->addresses()->save($address);

                $addressong = new Address;
                $addressong->zip_code = $request->zip_code ? preg_replace('/[^0-9]/', '', $request->zip_code) : null;
                $addressong->street = $request->street;
                $addressong->number = $request->number;
                $addressong->district = $request->district;
                $addressong->complement = $request->complement;
                $addressong->city_id = $request->city_id;
                $addressong->state_id = $request->state_id;
                $ong->addresses()->save($addressong);
            }
        }else{
            if(sizeof($user->find($user->id)->addresses)>0){
                $user->addresses()->delete();
                $ong->addresses()->delete();
            }
        }
        if($request->has('url') || $request->has('account')){
            $donate = $ong->donate;
            $donate->url = $request->url;
            $donate->account = $request->account;
            $ong->donate()->save($donate);
        }

        return back()->withStatus(__('Perfil Atualizado com Sucesso.'));
    }
    public function updateClinic(Request $request)
    {
        $user = auth()->user();
        $user->name=$request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->phone = $request->telphone;
        $user->birth_date = $request->birth_date;
        $user->save();
        $clinic = $user->clinicsUsuario;
        $clinic->name = $request->name;
        $clinic->crmv = $request->crmv;
        $clinic->email = $request->email;
        $clinic->cnpj = $request->cnpj;
        $clinic->site = $request->site;
        $clinic->phone = $request->telphone;
        $clinic->cellphone = $request->cellphone;
        $clinic->save();

        if($request->zip_code != null){
            if(sizeof($user->find($user->id)->addresses)>0 && sizeof($clinic->find($clinic->id)->addresses)>0){
                $address = [
                    'zip_code' => $request->zip_code ? preg_replace('/[^0-9]/', '', $request->zip_code) : null,
                    'street' => $request->street,
                    'number' => $request->number,
                    'district' => $request->district,
                    'complement' => $request->complement,
                    'city_id' => $request->city_id,
                    'state_id' => $request->state_id
                ];
                $user->addresses()->update($address);
                $clinic->addresses()->update($address);
            }else{
                $address = new Address;
                $address->zip_code = $request->zip_code ? preg_replace('/[^0-9]/', '', $request->zip_code) : null;
                $address->street = $request->street;
                $address->number = $request->number;
                $address->district = $request->district;
                $address->complement = $request->complement;
                $address->city_id = $request->city_id;
                $address->state_id = $request->state_id;
                $user->addresses()->save($address);

                
                $addressclinic = new  Address();
                $addressclinic->zip_code = $request->zip_code;
                $addressclinic->state_id = $request->state_id;
                $addressclinic->city_id = $request->city_id;
                $addressclinic->street = $request->street;
                $addressclinic->number = $request->number;
                $addressclinic->complement = $request->complement;
                $addressclinic->district = $request->district;
                $clinic->addresses()->save($addressclinic);
            }
        }else{
            if(sizeof($user->find($user->id)->addresses)>0){
                $user->addresses()->delete();
                $clinic->addresses()->delete();
            }
        }


        return back()->withStatus(__('Perfil Atualizado com Sucesso.'));
    }

    public function updateAvatar(AvatarRequest $request){
        $user = User::find(auth()->user()->id);
        if($request->has('photo')){
            if($user->avatar){
                $caminho = File::glob(public_path('\storage\avatar\\'.$user->id.'\\*.*'));
                foreach ($caminho as $pastas) {
                    unlink($pastas);
                }
                $user->avatar()->delete();
            }
            $uuid = Uuid::uuid4()->getHex();
            $imgdata = getimagesize($request->file('photo'));
            $extension = image_type_to_extension($imgdata[2],false);
            $path = Storage::disk('public')->putFileAs('avatar/'.$user->id, $request->file('photo'), "{$uuid}.{$extension}");
            $avatar = new Avatar;
            $avatar->filename = pathinfo($path)['basename'];
            $avatar->dimensions = ['width' => $imgdata[0],'height' => $imgdata[1]];
            $avatar->format = image_type_to_mime_type($imgdata[2]);

            $user->avatar()->save($avatar);

        }


        return back()->withAvatarStatus(__('Avatar atualizado com sucesso!'));
    } 
    public function updateSocial(Request $request){
        $user = User::find(auth()->user()->id);

        if($request->has('type_id')){
            if(sizeof($user->SocialNetworks)>0){
                $user->SocialNetworks()->delete();
            }
            for($i=0; $i<count($request->type_id); $i++){

                if($request->type_id[$i]!=null && $request->type_id[$i]!=''){
                    $social = new Social_Network;
                    $social->type_id = $request->type_id[$i];
                    if($request->type_id[$i]==14 || $request->type_id[$i]==16 || $request->type_id[$i] == 17){
                        $social->phone = $request->user[$i];
                    }else{
                        $social->user = $request->user[$i];
                    }
                    $user->SocialNetworks()->save($social);
                }
            }
        }
        if($request->has('newtype_id')){
            for($i=0; $i<count($request->newtype_id); $i++){
                if($request->newtype_id[$i]!=null && $request->newtype_id[$i]!=''){
                    $social = new Social_Network;
                    $social->type_id = $request->newtype_id[$i];
                    if($request->newtype_id[$i]==14 || $request->newtype_id[$i]==16 || $request->newtype_id[$i] == 17){
                        $social->phone = $request->newuser[$i];
                    }else{
                        $social->user = $request->newuser[$i];
                    }
                    $user->SocialNetworks()->save($social);
                } 
            }
        }
        
        return back()->withAvatarStatus(__('Redes Sociais Atualizadas com sucesso!'));
    }
        /**
         * Change the password
         *
         * @param  \App\Http\Requests\PasswordRequest  $request
         * @return \Illuminate\Http\RedirectResponse
         */
        public function password(PasswordRequest $request)
        {
            auth()->user()->update(['password' => Hash::make($request->get('password'))]);

            return back()->withPasswordStatus(__('Password successfully updated.'));
        }
    }
