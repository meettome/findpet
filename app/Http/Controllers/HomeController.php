<?php

namespace App\Http\Controllers;
use Midnite81\GeoLocation\Contracts\Services\GeoLocation;
use Illuminate\Http\Request;

use App\Models\Animal;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index(GeoLocation $geo, Request $request)
    {   
        $ipLocation = $geo->getCity($request->ip);
    if(auth()->user()){
        if(sizeof(auth()->user()->employeeong) > 0){
            $owner_id = auth()->user()->employeeong[0]->user_id;
        }else if(sizeof(auth()->user()->employeeclinic) > 0){
            $owner_id = auth()->user()->employeeclinic[0]->user_id;
        }else{
            $owner_id = auth()->user()->id;
        }

        $animais = Animal::where('owner_id', '<>', $owner_id)->orderby('animals.name', 'asc')->paginate(15);
        return view('dashboard', ['animals' => $animais, 'localization'=>$ipLocation]);
    }else{
        return view('welcome');
    }   
} 
}
