<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;
use App\Models\Picture;
use App\Models\Animal;
use App\Models\Avatar;
use App\Models\Adoption;
use App\Models\Address;
use App\Models\Lost;
use App\Models\User;
use App\Models\Type;
use App\Models\Breed;
use Ramsey\Uuid\Uuid;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Storage;

class AnimalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        if(auth()->user()->role == 'admin'){
            $animals = Animal::orderby('animals.name', 'asc')->paginate(15);
            return view('animals.index', ['animals' => $animals, 'title' => 'Todos os Animais']);
        }else if(auth()->user()->role=="employee"){
            if(sizeof(auth()->user()->employeeong) > 0){
                $owner_id = auth()->user()->employeeong[0]->user_id;
                $nome = auth()->user()->employeeong[0]->name;
            }else {
                $owner_id = auth()->user()->employeeclinic[0]->user_id;
                $nome = auth()->user()->employeeclinic[0]->name;
            }
            $animals =  Animal::where('owner_id', '=', $owner_id)->orderby('animals.name', 'asc')->paginate(15);
            return view('animals.index', ['animals' => $animals, 'title' => 'Todos os Animais de '.$nome]);
        }else{
            $user = auth()->user()->id;
            $animals =  Animal::where('owner_id', '=', $user)->orderby('animals.name', 'asc')->paginate(15);
            return view('animals.index', ['animals' => $animals, 'title' => 'Todos os seus animais']);
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return(view('animals.create'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $animal = new Animal();
        if(auth()->user()->role=="employee"){
            if(sizeof(auth()->user()->employeeong) > 0){
                $owner_id = auth()->user()->employeeong[0]->user_id;
            }else {
                $owner_id = auth()->user()->employeeclinic[0]->user_id;
            }
            $animal->owner_id = $owner_id;
        }else{
            $animal->owner_id = auth()->user()->id;
        }

        $animal->name = $request->name;
        $animal->note = $request->note;
        $animal->sex = $request->sex;
        $animal->birth_date = $request->birth_date;
        $animal->breed_id = $request->breed_id;
        $animal->type_id = $request->type_id;
        $animal->color = $request->color;
        $animal->size = $request->size;
        if($request->has('castration')) {
            $animal->castration = $request->castration;
        }else {
            $animal->castration = 0;
        }
        if($request->has('vaccinated'))  {
            $animal->vaccinated = $request->vaccinated;
        }else {
            $animal->vaccinated = 0;
        }
        if($request->has('dewormed')) {
            $animal->dewormed = $request->dewormed;
        }else {
            $animal->dewormed = 0;
        }
        if($request->has('microchipped')) {
            $animal->microchipped = $request->microchipped;
        }else {
            $animal->microchipped = 0;
        }
        $animal->rga = $request->rga;
        $animal->save();
        if($request->has('photo')){
            for($i=0; $i<count($request->allFiles()['photo']); $i++){
                $uuid = Uuid::uuid4()->getHex();
                $imgdata = getimagesize($request->allFiles()['photo'][$i]);
                $extension = image_type_to_extension($imgdata[2],false);
                $path = Storage::disk('public')->putFileAs(
                 'animals/galery/'.$animal->id, $request->allFiles()['photo'][$i], "{$uuid}.{$extension}"
             );
                $photo = new Picture;
                $photo->filename = pathinfo($path)['basename'];
                $photo->dimensions = [
                    'width' => $imgdata[0],
                    'height' => $imgdata[1],
                ];
                $photo->format = image_type_to_mime_type($imgdata[2]);

                $animal->pictures()->save($photo);
            }
        }
        if($request->has('featured')){
            $uuid = Uuid::uuid4()->getHex();
            $imgdata = getimagesize($request->file('featured'));
            $extension = image_type_to_extension($imgdata[2],false);
            $path = Storage::disk('public')->putFileAs(
                'animal/featured/'.$animal->id, $request->file('featured'), "{$uuid}.{$extension}"
            );
            $featured = new Avatar();
            
            $featured->filename = pathinfo($path)['basename'];
            $featured->dimensions = [
                'width' => $imgdata[0],
                'height' => $imgdata[1],
            ];
            $featured->format = image_type_to_mime_type($imgdata[2]);
            $animal->avatar()->save($featured);
        }

        if($request->wish == 'adoption'){
            $adoption = new Adoption();
            $adoption->owner_id = auth()->user()->id;
            $adoption->animal_id = $animal->id;
            $adoption->save();
        }else if($request->wish == 'lost'){
            $lost = new Lost();
            $lost->user_id = auth()->user()->id;
            $lost->animal_id = $animal->id;
            $lost->save();
            if($request->zip_code != null){
                $address = new Address;
                $address->zip_code = $request->zip_code ? preg_replace('/[^0-9]/', '', $request->zip_code) : null;
                $address->street = $request->street;
                $address->number = $request->number;
                $address->district = $request->district;
                $address->complement = $request->complement;
                $address->city_id = $request->city_id;
                $address->state_id = $request->state_id;
                $lost->addresses()->save($address);
            }
        }

        return redirect()->route('animal.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id=null, $name=null)
    {
        $animal = Animal::find($id);
        if(Animal::find($id)->owner->clinicsUsuario != null){
            $usuario =  Animal::find($id)->owner->clinicsUsuario;
            $id = $usuario->user_id;
        }else if(Animal::find($id)->owner->ongsUsuario != null){
            $usuario =  Animal::find($id)->owner->ongsUsuario;
            $id = $usuario->user_id; 
        }else{
            $usuario =  Animal::find($id)->owner;
            $id = $usuario->id; 
        }
        $whatsapp = $usuario->SocialNetworks->where("type_id", "=", "17");
        $data = [
            "animal" => $animal,
            "dono" => $usuario,
            "whatsapp"=>$whatsapp,
            "id" => $id
        ];
        return(view('animals.view', $data));
    }

    public function dono($id){
        $animal = Animal::find($id);

        Mail::send(new \App\Mail\AdocaoInteresse($animal));

        return  redirect()->route('animal.show', ['id'=>$animal->id, 'name'=>$animal->name])->withStatus(__('Email enviado com sucesso.'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Animal $animal)
    {

        $data = [
            "animal" => $animal
        ];
        return(view('animals.edit', $data));
    }

    public function search(Request $request){

        if(sizeof(auth()->user()->employeeong) > 0){
            $owner_id = auth()->user()->employeeong[0]->user_id;
        }else if(sizeof(auth()->user()->employeeclinic) > 0){
            $owner_id = auth()->user()->employeeclinic[0]->user_id;
        }else{
            $owner_id = auth()->user()->id;
        }
        if($request->breed_id != null || $request->breed_id != "")
            $animais = Animal::where([['animals.owner_id', '<>', $owner_id], ['animals.type_id', '=', $request->type_id], ['animals.breed_id', '=', $request->breed_id]])->orderby('animals.name', 'asc')->paginate(15);
        else $animais = Animal::where([['animals.owner_id', '<>', $owner_id], ['animals.type_id', '=', $request->type_id]])->orderby('animals.name', 'asc')->paginate(15);

        return view('dashboard', ['animals' => $animais, 'title' => 'Animais', 'breed_id' => $request->breed_id, 'type_id' => $request->type_id]);
    }
    public function pesquisa(Request $request){

        if(sizeof(auth()->user()->employeeong) > 0){
            $owner_id = auth()->user()->employeeong[0]->user_id;
        }else if(sizeof(auth()->user()->employeeclinic) > 0){
            $owner_id = auth()->user()->employeeclinic[0]->user_id;
        }else{
            $owner_id = auth()->user()->id;
        }
        $parametro = "%$request->parameter%";
        $animais = Animal::select(DB::raw('animals.name as nome, animals.sex as sex, types.name as tipo, animals.id as id'))->join('types', function ($join) {
            $join->on('types.id', '=', 'animals.type_id');
        })->where('types.name', 'like', $parametro)->orderby('animals.name', 'asc')->paginate(15);
        return view('pesquisa', ['animals' => $animais, 'title' => 'Animais', 'breed_id' => $request->breed_id, 'type_id' => $request->type_id]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Animal $animal)
    {
        $animal->name = $request->name;
        $animal->note = $request->note;
        $animal->sex = $request->sex;
        $animal->birth_date = $request->birth_date;
        $animal->breed_id = $request->breed_id;
        $animal->type_id = $request->type_id;
        $animal->color = $request->color;
        $animal->size = $request->size;
        if($request->has('castration')) {
            $animal->castration = $request->castration;
        }else {
            $animal->castration = 0;
        }
        if($request->has('vaccinated'))  {
            $animal->vaccinated = $request->vaccinated;
        }else {
            $animal->vaccinated = 0;
        }
        if($request->has('dewormed')) {
            $animal->dewormed = $request->dewormed;
        }else {
            $animal->dewormed = 0;
        }
        if($request->has('microchipped')) {
            $animal->microchipped = $request->microchipped;
        }else {
            $animal->microchipped = 0;
        }
        $animal->rga = $request->rga;
        $animal->save();
        if($request->has('photo')){

            if($request->wish == 'replace'){
                //Não use caminhos windows para deploy em linux, pode causar erros inesperados.
                $caminho = File::glob(public_path('/storage/animals/galery/'.$animal->id.'/*.*'));
                foreach ($caminho as $pastas) {
                    unlink($pastas);
                }
                $animal->pictures()->delete();
            }
            for($i=0; $i<count($request->allFiles()['photo']); $i++){
                $uuid = Uuid::uuid4()->getHex();
                $imgdata = getimagesize($request->allFiles()['photo'][$i]);
                $extension = image_type_to_extension($imgdata[2],false);
                $path = Storage::disk('public')->putFileAs(
                 'animals/galery/'.$animal->id, $request->allFiles()['photo'][$i], "{$uuid}.{$extension}"
             );
                $photo = new Picture;
                $photo->filename = pathinfo($path)['basename'];
                $photo->dimensions = [
                    'width' => $imgdata[0],
                    'height' => $imgdata[1],
                ];
                $photo->format = image_type_to_mime_type($imgdata[2]);

                $animal->pictures()->save($photo);
            }
        }
        if($request->has('featured')){
            if($animal->avatar){
                //Não use caminhos windows para deploy em linux, pode causar erros inesperados.
                $caminho = File::glob(public_path('/storage/animal/featured/'.$animal->id.'/*.*'));
                foreach ($caminho as $pastas) {
                    unlink($pastas);
                }
            }
            $uuid = Uuid::uuid4()->getHex();
            $imgdata = getimagesize($request->file('featured'));
            $extension = image_type_to_extension($imgdata[2],false);
            $path = Storage::disk('public')->putFileAs(
                'animal/featured/'.$animal->id, $request->file('featured'), "{$uuid}.{$extension}"
            );
            if($animal->avatar){
                $featured = $animal->avatar;
            }else{
                $featured = new Avatar;
            }
            $featured->filename = pathinfo($path)['basename'];
            $featured->dimensions = [
                'width' => $imgdata[0],
                'height' => $imgdata[1],
            ];
            $featured->format = image_type_to_mime_type($imgdata[2]);
            $animal->avatar()->save($featured);
        }

        if($request->wish == 'adoption'){
            if($animal->lost){
                $animal->lost->addresses()->delete();
                $animal->lost->delete();
            }
            if($animal->adoption){
                $adoption="";
            }else{
                $adoption = new Adoption();
                $adoption->owner_id = auth()->user()->id;
                $adoption->animal_id = $animal->id;
                $adoption->save();
            }
        }else if($request->wish == 'lost'){
            if($animal->adoption){
                $animal->adoption->delete();
            }
            if($animal->lost){
                $lost = $animal->lost;
            }else{
                $lost = new Lost();
            }
            $lost->user_id = auth()->user()->id;
            $lost->animal_id = $animal->id;
            $lost->save();
            if($request->zip_code != null){
                if(sizeof($lost->find($lost->id)->addresses)>0){
                    $address = [
                        'zip_code' => $request->zip_code ? preg_replace('/[^0-9]/', '', $request->zip_code) : null,
                        'street' => $request->street,
                        'number' => $request->number,
                        'district' => $request->district,
                        'complement' => $request->complement,
                        'city_id' => $request->city_id,
                        'state_id' => $request->state_id
                    ];
                    $lost->addresses()->update($address);
                }else{
                    $address = new Address;
                    $address->zip_code = $request->zip_code ? preg_replace('/[^0-9]/', '', $request->zip_code) : null;
                    $address->street = $request->street;
                    $address->number = $request->number;
                    $address->district = $request->district;
                    $address->complement = $request->complement;
                    $address->city_id = $request->city_id;
                    $address->state_id = $request->state_id;
                    $lost->addresses()->save($address);
                }


            }
        }
        return redirect()->route('animal.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Animal $animal)
    {
        if($animal->avatar){

            $caminho = File::glob(public_path('/storage/animal/featured/'.$animal->id.'/*.*'));
            foreach ($caminho as $pastas) {
                unlink($pastas);
            }  
            rmdir(public_path('storage/animal/featured/'.$animal->id.'/'));
            $animal->avatar()->delete();
            unset($caminho);
            unset($pastas);
            
        } 
        if(sizeof($animal->pictures)>0){
            $caminho = File::glob(public_path('/storage/animals/galery/'.$animal->id.'/*.*'));
            foreach ($caminho as $pastas) {
                unlink($pastas);
            }  
            rmdir(public_path('storage/animals/galery/'.$animal->id.'/'));
            $animal->pictures()->delete();
            unset($caminho);
            unset($pastas);
            
        } 

        if($animal->adoption){
            $animal->adoption->delete();
        }
        if($animal->lost){
            $animal->lost->delete();
            if($animal->lost->addresses){
                $animal->lost->addresses->delete();
            }
        }
        $animal->delete();
        return redirect()->route('animal.index');
    }
}
