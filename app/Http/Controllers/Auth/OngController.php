<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use App\Models\Ong;
use App\Models\Donate;
use Illuminate\Support\Facades\Mail;
use App\Models\Address;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\Avatar;
use App\Models\Social_Network;
use Ramsey\Uuid\Uuid;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Storage;

class OngController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */



    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return view('auth.register');
    }



    protected function store(Request $request){


        $cnpjbusca = intval(preg_replace('/\W/','',$request->cnpj));

        if(User::where('email', '=', $request->email)->exists()){
            if(Ong::where('cnpj', '=', $cnpjbusca)->exists()){
                return back()
                ->withInput($request->input())
                ->withErrors([
                    'email' => 'Email ja cadastrado',
                    'cnpj' => 'CNPJ ja cadastrado',
                ]);
            }
            return back()
            ->withInput($request->input())
            ->withErrors([
                'email' => 'Email ja cadastrado',
            ]);
        }else  if(Ong::where('cnpj', '=', $cnpjbusca)->exists()){
            return back()
            ->withInput($request->input())
            ->withErrors([
                'cnpj' => 'CNPJ ja cadastrado',
            ]);
        }else{
            $user = new User();
            $user->name=$request->name;
            $user->email = $request->email;
            $user->password = Hash::make($request->password);
            $user->phone = $request->telphone;
            $user->birth_date = $request->birth_date;
            $user->sex = $request->sex;
            $user->role = 'ong';
            $user->email_verified_at = time();
            $ong = new Ong;
            $ong->user_id = $user->id;
            $ong->name = $request->name;
            $ong->email = $request->email;
            $ong->cnpj = $request->cnpj;
            $ong->site = $request->site;
            $ong->phone = $request->telphone;
            $ong->cellphone = $request->cellphone;
            $ong->save();
            $addressong = new Address;
            $addressong->zip_code = $request->zip_code ? preg_replace('/[^0-9]/', '', $request->zip_code) : null;
            $addressong->street = $request->street;
            $addressong->number = $request->number;
            $addressong->district = $request->district;
            $addressong->complement = $request->complement;
            $addressong->city_id = $request->city_id;
            $addressong->state_id = $request->state_id;
            $ong->addresses()->save($addressong);
            if($request->has('url') || $request->has('account')){
                $donate = new Donate;
                $donate->ong_id = $ong->id;
                $donate->url = $request->url;
                $donate->account = $request->account;
                $donate->save();
            }
            if($request->has('photo')){
                $uuid = Uuid::uuid4()->getHex();
                $imgdata = getimagesize($request->file('photo'));
                $extension = image_type_to_extension($imgdata[2],false);
                $path = Storage::disk('public')->putFileAs(
                    'avatar/'.$user->id, $request->file('photo'), "{$uuid}.{$extension}"
                );
                $photo = new Avatar;
                $photo->filename = pathinfo($path)['basename'];
                $photo->dimensions = [
                    'width' => $imgdata[0],
                    'height' => $imgdata[1],
                ];
                $photo->format = image_type_to_mime_type($imgdata[2]);

                $user->avatar()->save($photo);

            }

            if($request->has('type_id')){
                for($i=0; $i<count($request->type_id); $i++){   
                    if($request->type_id[$i]!=null && $request->type_id[$i]!=''){
                        $social = new Social_Network;
                        $social->type_id = $request->type_id[$i];
                        if($request->type_id[$i]==14 || $request->type_id[$i]==16 || $request->type_id[$i] == 17){
                            $social->phone = $request->user[$i];
                        }else{
                            $social->user = $request->user[$i];
                        }
                        $socialnet = new Social_Network;
                        $socialnet->type_id = $request->type_id[$i];
                        if($request->type_id[$i]==14 || $request->type_id[$i]==16 || $request->type_id[$i] == 17){
                            $socialnet->phone = $request->user[$i];
                        }else{
                            $socialnet->user = $request->user[$i];
                        }
                        $user->SocialNetworks()->save($social);
                        $ong->SocialNetworks()->save($socialnet);
                    }
                }
            }

            Mail::send(new \App\Mail\ConfirmRegister($request));
            return  redirect()->route('login')->withStatus(__('Usuario registrado com Sucesso.'));
        }
    }

    protected function showOngs(){
        return view('auth.ong_register');
    }
}
