<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use App\Mail\ResetPasswordEmail;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use App\Models\User;

class ForgotPasswordController extends Controller
{
    use SendsPasswordResetEmails;

    public function showLinkRequestForm()
    {
        return view('auth.passwords.email');
    }

    protected function sendResetLinkResponse(Request $request)
    {
        return redirect()->route('password.request')->with('status');
    }


    protected function sendResetLinkEmail(Request $request)
    {
        if(User::where('email', '=', $request->email)->exists()){
            Mail::send(new \App\Mail\ResetPasswordEmail($request));
            return redirect()->route('login')->withStatus(__('Enviamos um email para você!'));
        }else{
            return back()
                ->withInput($request->only('email', 'remember'))
                ->withErrors([
                    'email' => 'Email não cadastrado, cadastre-se',
                ]);
        }
    }

    protected function sendResetLinkFailedResponse(Request $request)
    {
        return redirect()->route('password.request')
                ->withInput($request->only('email'));
    }
}

