<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {
        try{
            $user = User::where('email',$request->email);
            
            if(Auth::attempt($request->only(['email','password']), $request->has('remember'))){
                return redirect()->route('home');
            } else if(Auth::attempt($request->only(['email','password']))) {
                return redirect()->route('home');
            }else{
                return back()
                ->withInput($request->only('email', 'remember'))
                ->withErrors([
                    'email' => 'As credenciais informadas não são válidas',
                ]);
            }
        } catch (ModelNotFoundException $e) {
            return back()
            ->withInput($request->only('email', 'remember'))
            ->withErrors([
                'email' => 'As credenciais informadas não são válidas',
            ]);
        }
    }

    public function logout(Request $request)
    {
        Auth::logout();
        return redirect()->route('home');
    }

    public function showLoginForm()
    {
        if(Auth::user())
            return redirect()->route('home');
        else
            return view('auth.login');
    }
}

