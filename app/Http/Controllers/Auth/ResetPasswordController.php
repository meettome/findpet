<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use App\Models\User;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    public function showResetForm(Request $request, $token = null)
    {   
        if($password = DB::table('password_resets')->where([
            ['token', '=',  $token],
            ['email', '=', $request->email],
        ])->exists()){
            $password = DB::table('password_resets')->where([
                ['token', '=',  $token],
                ['email', '=', $request->email],
            ])->get();
            $start_date = new \DateTime(date('Y-m-d G:i:s'));
            $since_start = $start_date->diff(new \DateTime($password[0]->created_at));
            if($since_start->h<24){
                return view('auth.passwords.reset')->with(
                    ['token' => $token, 'email' => $request->email]
                );
            }else{
                return redirect()->route('password.request')
                ->withInput($request->only('email'))->withErrors([
                    'email' => 'O tempo expirou, por favor, envie novamente',
                ]);
            }
        }else{
            return redirect()->route('password.request')
            ->withInput($request->only('email'))->withErrors([
                'email' => 'Você ja alterou a senha',
            ]);
        }
    }
    protected function reset(Request $request)
    {
        if($request->password == $request->password_confirmation){       
            $user = User::where('email', $request->email)->first();
            $user->password = Hash::make($request->password);
            $user->save();

            DB::table('password_resets')->where([['email', $request->email],['token', $request->token]])->delete();

            return redirect()->route('login')
            ->withStatus(__('Senha Alterada com Sucesso!'));
        }else{
            return back()
            ->withInput($request->only('email', 'remember'))
            ->withErrors([
                'password_confirmation' => 'Senhas não correspondem!',
            ]);
        }
    }

}
