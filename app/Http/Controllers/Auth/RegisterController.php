<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use App\Models\Address;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Models\Avatar;
use App\Models\Social_Network;
use Ramsey\Uuid\Uuid;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Storage;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */



    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return view('auth.register');
    }



    protected function store(Request $request){

        $cpfbusca = intval(preg_replace('/\W/','',$request->cpf));

        if(User::where('email', '=', $request->email)->exists()){
            if(User::where('cpf', '=', $cpfbusca)->exists()){
                return back()
                ->withInput($request->input())
                ->withErrors([
                    'email' => 'Email ja cadastrado',
                    'cpf' => 'CPF ja cadastrado',
                ]);
            }
            return back()
            ->withInput($request->input())
            ->withErrors([
                'email' => 'Email ja cadastrado',
            ]);
        }else  if(User::where('cpf', '=', $cpfbusca)->exists()){
            return back()
            ->withInput($request->input())
            ->withErrors([
                'cpf' => 'CPF ja cadastrado',
            ]);
        } else {
            $user = new User();
            $user->name=$request->name;
            $user->email = $request->email;
            $user->password = Hash::make($request->password);
            $user->phone = $request->telphone;
            $user->birth_date = $request->birth_date;
            $user->cpf = $request->cpf;
            $user->sex = $request->sex;
            $user->role = 'user';
            $user->email_verified_at = time();
            $user->save();

            if($request->has('zip_code')){
                $address = new  Address();
                $address->zip_code = $request->zip_code;
                $address->state_id = $request->state_id;
                $address->city_id = $request->city_id;
                $address->street = $request->street;
                $address->number = $request->number;
                $address->complement = $request->complement;
                $address->district = $request->district;
                $user->addresses()->save($address);
            }
            if($request->has('photo')){
                $uuid = Uuid::uuid4()->getHex();
                $imgdata = getimagesize($request->file('photo'));
                $extension = image_type_to_extension($imgdata[2],false);
                $path = Storage::disk('public')->putFileAs(
                    'avatar/'.$user->id, $request->file('photo'), "{$uuid}.{$extension}"
                );
                $photo = new Avatar;
                $photo->filename = pathinfo($path)['basename'];
                $photo->dimensions = [
                    'width' => $imgdata[0],
                    'height' => $imgdata[1],
                ];
                $photo->format = image_type_to_mime_type($imgdata[2]);

                $user->avatar()->save($photo);

            }

            if($request->has('type_id')){
                for($i=0; $i<count($request->type_id); $i++){   
                    if($request->type_id[$i]!=null && $request->type_id[$i]!=''){
                        $social = new Social_Network;
                        $social->type_id = $request->type_id[$i];
                        if($request->type_id[$i]==14 || $request->type_id[$i]==16 || $request->type_id[$i] == 17){
                            $social->phone = $request->user[$i];
                        }else{
                            $social->user = $request->user[$i];
                        }
                        $user->SocialNetworks()->save($social);
                    }
                }
            }

            Mail::send(new \App\Mail\ConfirmRegister($request));

            return  redirect()->route('home')->withStatus(__('Usuario registrado com Sucesso.'));
        }
    }



    protected function showPerfil(){
        return view('auth.select');
    }
    protected function showUsuario(){
        return view('auth.register', ['role' => 'user']);
    }
}
