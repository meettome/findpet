<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class confirmationController extends Controller
{

	public function confirmacao($email = null)
	{
		$user = User::where('email',$email)->where('email_verified_at',null)->first();
		if($user){
			$user->email_verified_at = time();
			$user->save();
			return redirect()->route('login')->withStatus(__('Email confirmado com sucesso.'));
		} else {
			return redirect()->route('login')->withStatus(__('Este link de confirmação não é válido.'));
		}
	}
}
