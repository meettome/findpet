<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

use App\Http\Requests\EmailRequest;
use App\Models\Email;
use App\Mail\Contact;

class EmailController extends Controller
{


    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create()
    {
        return view('contact');  
    }

    protected function store(EmailRequest $request){
        $email = new Email;
        $email->name = $request->name;
        $email->email = $request->email;
        $email->subject = $request->subject;
        $email->message = $request->message;
        $email->save();
        Mail::send(new \App\Mail\Contact($email));
        return redirect()->route('contact')->withStatus(__('Email Enviado com sucesso!'));
    }
    protected function dono(Request $request){
        $email = new Email;
        $email->name = $request->name;
        $email->email = $request->email;
        $email->subject = $request->subject;
        $email->message = $request->message;
        $email->save();
        Mail::send(new \App\Mail\Contact($email));
        return redirect()->route('contact')->withStatus(__('Email Enviado com sucesso!'));
    }
}
