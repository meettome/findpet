<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Animal;
use Barryvdh\DomPDF\Facade as PDF;

class PdfviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id=null, $name=null)
    {

        $animal = Animal::find($id);

        $donousuario = User::find($animal->owner_id);
        if(auth()->user()->role=="vet"){
            $usuario = auth()->user()->clinicUsuario;
            $cnpj = true;
        }else if(auth()->user()->role=="ong"){
            $usuario = auth()->user()->ongUsuario;
            $cnpj = true;
        }else{
            $usuario = auth()->user();
            $cnpj = false;
        } 
        if($donousuario->role=="vet"){
            $dono = $donousuario->clinicUsuario;
            $cpf = true;
        }else if($donousuario->role=="ong"){
            $dono = $donousuario->ongUsuario;
            $cpf = true;
        }else{
            $dono = $donousuario;
            $cpf = false;
        }
        $dados = "";
        if($animal->castration ==1){
            $dados = 'castrado,';
            if($animal->vaccinated ==1){
                if($animal->dewormed ==1){ 
                    $dados .= ', vacinado e vermifugado,';
                }else{
                    $dados .= ' e vacinado,';
                }
            }else{
                $dados .= ',';
            }
        }else if($animal->vaccinated ==1){
            if($animal->dewormed ==1){ 
                $dados .= 'vacinado e vermifugado,';
            }else{
                $dados .= 'vacinado,';
            }
        }else{
            $dados .= 'vermifugado,';
        }
        $data = [
            "animal" => $animal,
            "dono" => $dono,
            "usuario" => $usuario,
            "clin" => $cnpj,
            "user" => $cpf,
            "dados" => $dados
        ];
        return PDF::loadView('pdf.view', $data)
        ->stream();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
