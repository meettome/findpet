<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Animal;
use Illuminate\Support\Facades\Mail;

class LostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function search(Request $request){
        

        if(sizeof(auth()->user()->employeeong) > 0){
            $owner_id = auth()->user()->employeeong[0]->user_id;
        }else if(sizeof(auth()->user()->employeeclinic) > 0){
            $owner_id = auth()->user()->employeeclinic[0]->user_id;
        }else{
            $owner_id = auth()->user()->id;
        }
        if($request->breed_id != null || $request->breed_id != "")
            $animais = Animal::where([['animals.owner_id', '<>', $owner_id], ['animals.type_id', '=', $request->type_id], ['animals.breed_id', '=', $request->breed_id]])->orderby('animals.name', 'asc')->get();
        else $animais = Animal::where([['animals.owner_id', '<>', $owner_id], ['animals.type_id', '=', $request->type_id]])->orderby('animals.name', 'asc')->get();

        return view('lost.index', ['animals' => $animais, 'title' => 'Animais perdidos', 'breed_id' => $request->breed_id, 'type_id' => $request->type_id]);
    }

    public function index()
    {
        if(sizeof(auth()->user()->employeeong) > 0){
            $owner_id = auth()->user()->employeeong[0]->user_id;
        }else if(sizeof(auth()->user()->employeeclinic) > 0){
            $owner_id = auth()->user()->employeeclinic[0]->user_id;
        }else{
            $owner_id = auth()->user()->id;
        }
        $animais = Animal::orderby('animals.name', 'asc')->where('animals.owner_id', '<>', $owner_id)->get();
        return view('lost.index', ['animals' => $animais, 'title' => 'Animais Perdidos']);
    }

    public function dog(){
        if(sizeof(auth()->user()->employeeong) > 0){
            $owner_id = auth()->user()->employeeong[0]->user_id;
        }else if(sizeof(auth()->user()->employeeclinic) > 0){
            $owner_id = auth()->user()->employeeclinic[0]->user_id;
        }else{
            $owner_id = auth()->user()->id;
        }
        $animais = Animal::orderby('animals.name', 'asc')->where([
            ['animals.owner_id', '<>', $owner_id],
            ['animals.type_id', '=', '1']
        ])->get();
        return view('lost.index', ['animals' => $animais, 'title' => 'Cachorros para Perdidos']);
    }
    public function cat(){
        if(sizeof(auth()->user()->employeeong) > 0){
            $owner_id = auth()->user()->employeeong[0]->user_id;
        }else if(sizeof(auth()->user()->employeeclinic) > 0){
            $owner_id = auth()->user()->employeeclinic[0]->user_id;
        }else{
            $owner_id = auth()->user()->id;
        }
        $animais = Animal::orderby('animals.name', 'asc')->where([
            ['animals.owner_id', '<>', $owner_id],
            ['animals.type_id', '=', '2']
        ])->get();
        return view('lost.index', ['animals' => $animais, 'title' => 'Gatos para Perdidos']);
    }
    public function rodents(){
        if(sizeof(auth()->user()->employeeong) > 0){
            $owner_id = auth()->user()->employeeong[0]->user_id;
        }else if(sizeof(auth()->user()->employeeclinic) > 0){
            $owner_id = auth()->user()->employeeclinic[0]->user_id;
        }else{
            $owner_id = auth()->user()->id;
        }
        $animais = Animal::orderby('animals.name', 'asc')->where([
            ['animals.owner_id', '<>', $owner_id],
            ['animals.type_id', '=', '3']
        ])->get();
        return view('lost.index', ['animals' => $animais, 'title' => 'Roedores para Perdidos']);
    }
    public function rabbits(){
        if(sizeof(auth()->user()->employeeong) > 0){
            $owner_id = auth()->user()->employeeong[0]->user_id;
        }else if(sizeof(auth()->user()->employeeclinic) > 0){
            $owner_id = auth()->user()->employeeclinic[0]->user_id;
        }else{
            $owner_id = auth()->user()->id;
        }
        $animais = Animal::orderby('animals.name', 'asc')->where([
            ['animals.owner_id', '<>', $owner_id],
            ['animals.type_id', '=', '4']
        ])->get();
        return view('lost.index', ['animals' => $animais, 'title' => 'Coelhos para Perdidos']);
    }
    public function birds(){
        if(sizeof(auth()->user()->employeeong) > 0){
            $owner_id = auth()->user()->employeeong[0]->user_id;
        }else if(sizeof(auth()->user()->employeeclinic) > 0){
            $owner_id = auth()->user()->employeeclinic[0]->user_id;
        }else{
            $owner_id = auth()->user()->id;
        }
        $animais = Animal::orderby('animals.name', 'asc')->where([
            ['animals.owner_id', '<>', $owner_id],
            ['animals.type_id', '=', '5']
        ])->get();
        return view('lost.index', ['animals' => $animais, 'title' => 'Aves para Perdidos']);
    }
    public function turtles(){
        if(sizeof(auth()->user()->employeeong) > 0){
            $owner_id = auth()->user()->employeeong[0]->user_id;
        }else if(sizeof(auth()->user()->employeeclinic) > 0){
            $owner_id = auth()->user()->employeeclinic[0]->user_id;
        }else{
            $owner_id = auth()->user()->id;
        }
        $animais = Animal::orderby('animals.name', 'asc')->where([
            ['animals.owner_id', '<>', $owner_id],
            ['animals.type_id', '=', '6']
        ])->get();
        return view('lost.index', ['animals' => $animais, 'title' => 'Tartaruga, Jabutis e Cágados para Perdidos']);
    }
    public function furao(){
        if(sizeof(auth()->user()->employeeong) > 0){
            $owner_id = auth()->user()->employeeong[0]->user_id;
        }else if(sizeof(auth()->user()->employeeclinic) > 0){
            $owner_id = auth()->user()->employeeclinic[0]->user_id;
        }else{
            $owner_id = auth()->user()->id;
        }
        $animais = Animal::orderby('animals.name', 'asc')->where([
            ['animals.owner_id', '<>', $owner_id],
            ['animals.type_id', '=', '7']
        ])->get();
        return view('lost.index', ['animals' => $animais, 'title' => 'Furões para Perdidos']);
    }


    public function localization(Request $request, $id)
    {
        $animal = Animal::find($id);
        $animal->lost->addresses[0]->zip_code = $request->zip_code;
        $animal->lost->addresses[0]->district = $request->district;
        $animal->lost->addresses[0]->street = $request->street;
        $animal->lost->addresses[0]->number = $request->number;
        $animal->lost->addresses[0]->complement = $request->complement;
        $animal->lost->addresses[0]->state_id = $request->state_id;
        $animal->lost->addresses[0]->city_id = $request->city_id;
        $animal->lost->addresses[0]->save();

        Mail::send(new \App\Mail\AnimalEncontrado($animal));
        return  redirect()->route('animal.show', ['id'=>$animal->id, 'name'=>$animal->name])->withStatus(__('Email enviado com sucesso.'));
    }
}
