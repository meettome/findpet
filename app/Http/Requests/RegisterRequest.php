    <?php

namespace App\Http\Requests;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'min:3'],
            'email' => [
                'required', 'email', Rule::unique((new User)->getTable())->ignore($this->route()->user->id ?? null)
            ],
            'password' => ['required', 'min:6', 'confirmed'],
            'password_confirmation' => ['required', 'min:6'],
            'telphone' => ['required', 'min:10'],
            'birth_date' => ['date'],
            'cpf' => ['min:11'],
            'sex' => [Rule::in(['male', 'female', 'other'])],
            'zip_code' => ['required', Rule::notIn(['00000-000', '11111-111', '22222-222', '33333-333', '44444-444', '55555-555', '66666-666', '77777-777', '88888-888', '99999-999'])],
            'state_id' => ['required', 'numeric'],
            'city_id' => ['required', 'numeric'],
            'street' => ['required', 'min:3'],
            'number' => ['min:1',  'string'],
            'type_id' => ['array'],
            'user' => ['array'],
            'complement' => ['min:5'],
            'district' => ['required', 'min:3'],
            'photo' => ['mimes:jpeg, jpg, png, gif']
        ];
    }

    public function messages()
    {
        return [
            'email.unique' => __('O email informado já consta em nossos registros'),
            'password.confirmed' => __('A confirmação de nova senha não confere'),
            'password_confirmation.min'=>__('A confirmação de senha precisa ter pelo menos 6 caracteres'),
            'telphone.json' =>__('O Telefone está errado'),
            'whatsapp.json' =>__('O whatsapp está errado'),
            'cpf.min' =>__('O cpf ta muito pequeno'),
        ];
    }
}
