<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class EmailRequest extends FormRequest
{ 
	public function rules()
	{
		return [
			'name' => ['required', 'min:3'],
			'email' => ['required', 'email'],
			'subject' => ['required', 'min:10'],
			'message' => ['required', 'min:10'],
		];
	}
	public function messages()
	{
		return [
			'name.min' => __('O nome deve ter ao menos 3 caracteres'),
			'subject.min' => __('O assunto deve ter ao menos 10 caracteres'),
			'message.min'=>__('A mensagem deve conter ao menos 10 caracteres'),
			'name.required' =>__('Campo Obrigatorio'),
			'email.required' =>__('Campo Obrigatorio'),
			'email.email' =>__('Coloque um email valido'),
			'message.required' =>__('Campo Obrigatorio')
		];
	}
}
