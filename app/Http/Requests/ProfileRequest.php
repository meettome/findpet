<?php

namespace App\Http\Requests;

use App\Models\User;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class ProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'min:3'],
            'email' => ['required', 'email', Rule::unique((new User)->getTable())->ignore(auth()->id())],
            'telphone' => ['required', 'min:10'],
            'birth_date' => ['required', 'date'],
            'cpf' => ['required', 'min:11'],
            'sex' => ['required', Rule::in(['male', 'female', 'other'])],
            'zip_code' => [Rule::notIn(['00000-000', '11111-111', '22222-222', '33333-333', '44444-444', '55555-555', '66666-666', '77777-777', '88888-888', '99999-999'])],
            'state_id' => [''],
            'city_id' => [''],
            'street' => [''],
            'number' => [''],
            'district' => [''],
        ];
    }
}
