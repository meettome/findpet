<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Type_Social_Network extends Model
{
    
    public function type(){
        return $this->hasOne('App\Models\Social_Network');
    }
}
