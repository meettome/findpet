<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Lost extends Model
{
   	public function animals(){
        return $this->belongsTo("App\Models\Animal");
    }

    public function users(){
        return $this->belongsTo("App\Models\User");
    }
    public function addresses()
    {
        return $this->morphMany('App\Models\Address', 'addressable');
    }
}
