<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
	public $timestamps=false;
	public function breeds(){
		return $this->hasMany("App\Models\Breed");
	}

    public function animals()
    {
        return $this->hasMany('App\Models\Animals');
    }
}
