<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Chat_Participation extends Model
{
    protected $fillable = [
		'conversation_id'
	];

	protected $hidden = [
	];

	protected $casts = [
		'settings' => 'json',
	];
	
    public function mensagens()
    {
        return $this->hasMany('App\Models\Chat_Message', 'chat__messages');
    }
    public function messageable(){
    	return $this->morphTo();
    }
}
