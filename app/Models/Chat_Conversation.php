<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Chat_Conversation extends Model
{
	protected $fillable = [
		'private', 'direct_message'
	];

	protected $hidden = [
	];

	protected $casts = [
		'data' => 'json',
	];
}
