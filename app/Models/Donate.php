<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Donate extends Model
{
    
    public function ongs()
    {
        return $this->belongsTo('App\Models\Ong', 'ong_id');
    }
}
