<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Notifications\ResetPassword;
use App\Notifications\ResetUserPasswordEmail;
use Musonza\Chat\Traits\Messageable;

class User extends Authenticatable
{
    use Notifiable;
    use Messageable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'telphone', 'whatsapp', 'photo', 'birth_date', 'cpf', 'sex'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'role', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function getCpfAttribute()
    {
        return preg_replace('/(\d{3})(\d{3})(\d{3})(\d{2})/','$1.$2.$3-$4',str_pad($this->attributes['cpf'], 11, '0', STR_PAD_LEFT));
    }

    public function getCellphoneAttribute()
    {
        return preg_replace('/(\d{2})(\d{5})(\d{4})/','($1)$2-$3',str_pad($this->attributes['phone'], 11, '0', STR_PAD_LEFT));
    }
    public function getWhatsappAttribute()
    {
        return preg_replace('/(\d{3})(\d{5})(\d{4})/','($1)$2-$3',str_pad($this->attributes['whatsapp'], 12, '0', STR_PAD_LEFT));
    }

    public function setCpfAttribute($value)
    {
        $this->attributes['cpf']=intval(preg_replace('/\W/','',$value));
    }

    public function setPhoneAttribute($value)
    {
        $this->attributes['phone']=intval(preg_replace('/\W/','',$value));
    }
    
    public function setWhatsappAttribute($value)
    {
        $this->attributes['whatsapp']=intval(preg_replace('/\W/','',$value));
    }

    public function getAvatarImageAttribute()
    {
        $sexes = ['female', 'male'];

        if($this->avatar){
            return '/storage/avatar/'.$this->id.'/'.$this->avatar->filename;
        }else{
            return "/avatars/".($this->attributes['sex']??$sexes[array_rand($sexes)])."/".str_pad(random_int(1, 6),3,'0',STR_PAD_LEFT).".svg";
        }
    }
    public function avatar()
    {
        return $this->morphOne('App\Models\Avatar', 'imageable')->where('type','avatar');
    }

    public function getIsAdminAttribute()
    {
        return $this->attributes['role']==='admin';
    }
    
    public function addresses()
    {
        return $this->morphOne('App\Models\Address', 'addressable');
    }
    public function participant()
    {
        return $this->morphMany('App\Models\Chat_Participation', 'messageable');
    }
    public function notificacoes()
    {
        return $this->morphMany('App\Models\Chat_Message_Notification', 'messageable');
    }
    public function getParticipanteAttribute()
    {
        return $this->participant;
    }
    public function animals()
    {
        return $this->hasMany('App\Models\Animal', 'animals');
    }
    public function SocialNetworks()
    {
        return $this->morphMany('App\Models\Social_Network', 'networks');
    }
    public function clinics(){
        return $this->belongsToMany('App\Models\Clinic');
    }
    public function ongs(){
        return $this->belongsToMany('App\Models\Ong');
    }

    public function getEmployeeOngAttribute()
    {
        return $this->ongs;
    }
    public function getEmployeeClinicAttribute()
    {
        return $this->clinics;
    }
    public function ongsUsuario(){
        return $this->hasOne('App\Models\Ong');
    }

    public function getOngUsuarioAttribute()
    {
        return $this->ongsUsuario;
    }
    public function clinicsUsuario(){
        return $this->hasOne('App\Models\Clinic');
    }

    public function getclinicUsuarioAttribute()
    {
        return $this->clinicsUsuario;
    }

    public function getPerfilAttribute()
    {
        if($this->attributes['role']=='admin'){
            return "Administrador";
        }else if($this->attributes['role']=='ong'){
            return "Ong";
        }else if($this->attributes['role']=='vet'){
            return "Veterinario";

        }else if($this->attributes['role']=='employee'){
            if(sizeof($this->ongs)>0){
                $funcionario = $this->ongs;
            }else{
                $funcionario = $this->clinics;
            }
            return $funcionario;
        }else{
            return "Usuario";
        }
    }


    public function sendPasswordResetNotification($token)
    {
            $this->notify(new ResetUserPasswordEmail($token, $this->email));
    }
}
