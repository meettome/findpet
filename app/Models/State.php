<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{

	protected $fillable = [
		'name', 'capital'
	];

	protected $hidden = [
	];

	protected $casts = [
		'capital' => 'boolean',
	];

	public $timestamps=false;

	public function cities()
	{
		return $this->hasMany('App\Models\City');
	}

	public function applications()
	{
		return $this->hasManyThrough(
			'App\Models\Application',
			'App\Models\City'
		);
	}


	public function log()
	{
		return $this->morphMany('App\Models\Log','loggable');
	}

	public function clinics()
	{
		return $this->hasManyThrough(
			'App\Models\Address',
			'App\Models\City'
		);
	}

}
