<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ong extends Model
{
    
    public function users(){
        return $this->belongsToMany('App\Models\User');
    }
    public function perfil(){
        return $this->belongsTo('App\Models\User', 'user_id');
    }
    public function addresses()
    {
    	return $this->morphOne('App\Models\Address', 'addressable');
    }

    public function getCellphoneAttribute()
    {
        return preg_replace('/(\d{2})(\d{5})(\d{4})/','($1)$2-$3',str_pad($this->attributes['cellphone'], 11, '0', STR_PAD_LEFT));
    }
    public function getDoacaoAttribute()
    {
        return $this->donate;
    }
    public function donate()
    {
        return $this->hasOne('App\Models\Donate');
    }
    public function SocialNetworks()
    {
        return $this->morphMany('App\Models\Social_Network', 'networks');
    }
    public function setCnpjAttribute($value)
    {
        $this->attributes['cnpj']=intval(preg_replace('/\W/','',$value));
    }

    public function getCnpjAttribute()
    {
        return preg_replace('/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/','$1.$2.$3/$4.$5',str_pad($this->attributes['cnpj'], 14, '0', STR_PAD_LEFT));
    }
}
