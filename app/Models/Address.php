<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $fillable = [
        'zip_code', 'state_id', 'city_id', 'street', 'number', 'district'
    ];
    public function addressable(){
    	return $this->morphTo();
    }
	public $timestamps=false;
    
	public function city()
	{
		return $this->belongsTo('App\Models\City');
	}

	public function state()
	{
		return $this->belongsTo('App\Models\State');
	}
}
