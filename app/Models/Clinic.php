<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Clinic extends Model
{
    protected $fillable = [
        'cnpj', 'name', 'email', 'site', 'phone'
    ];
    public function users(){
        return $this->belongsToMany('App\Models\User');
    }
    public function addresses()
    {
    	return $this->morphMany('App\Models\Address', 'addressable');
    }

    public function SocialNetworks()
    {
        return $this->morphMany('App\Models\Social_Network', 'networks');
    }
    public function setCnpjAttribute($value)
    {
        $this->attributes['cnpj']=intval(preg_replace('/\W/','',$value));
    }

    public function getCnpjAttribute()
    {
        return preg_replace('/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/','$1.$2.$3/$4-$5',str_pad($this->attributes['cnpj'], 14, '0', STR_PAD_LEFT));
    }

    public function getCellphoneAttribute()
    {
        return preg_replace('/(\d{2})(\d{5})(\d{4})/','($1)$2-$3', str_pad($this->attributes['cellphone'], 11, '0', STR_PAD_LEFT));
    }

}
