<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Animal extends Model
{
	public function types(){
		return $this->belongsTo('App\Models\Type', 'type_id');
	}
	public function breeds(){
		return $this->belongsTo('App\Models\Breed', "breed_id");
	}

	public function adoption(){
		return $this->hasOne("App\Models\Adoption");
	}
	public function lost(){
		return $this->hasOne("App\Models\Lost");
	}
	public function logo()
	{
		return $this->morphOne('App\Models\Logo', 'imageable')->where('type','logo');
	}

	public function avatar()
	{
		return $this->morphOne('App\Models\Avatar', 'imageable')->where('type','avatar');
	}
	public function getAvatarsAttribute()
	{
		return $this->avatar->filename;
	}

	public function getTamanhoAttribute(){
		if($this->attributes['size'] == 'micro'){
			return "Muito Pequeno";
		}else if($this->attributes['size'] == 'small'){
			return "Pequeno";
		}else if($this->attributes['size'] == 'medium'){
			return "Médio";
		}else if($this->attributes['size'] == 'big'){
			return "Grande";
		}else if($this->attributes['size'] == 'giant'){
			return "Muito Grande";
		}
	}
	public function getSexoAttribute(){
		if($this->attributes['sex'] == 'male'){
			return "Macho";
		}else {
			return "Fêmea";
		}
	}
	public function getIdadeAttribute(){
		$data = explode("-",$this->birth_date);
		$anoanterior = date('Y')-1;
		$ultimoAniversario = $anoanterior."-".$data[1]."-".$data[2];
		$dataatual = date('Y-m-d');
		$data1 = new \DateTime($ultimoAniversario);
		$data2 = new \DateTime($dataatual);
		$idade = $data1->diff($data2);

		$intervalo = $data1->diff( $data2 );
		if($intervalo->m>=12){
			$anoanterior = date('Y');
			$ultimoAniversario = $anoanterior."-".$aniversario;
			$data1 = new DateTime($ultimoAniversario);
			$intervalo = $data1->diff( $data2 );
			$mes = $intervalo->m;
		}else{
			$mes = $intervalo->m;
		}

		if($idade->y==0){
			return $idade->m." Meses";
		}else if($idade->m > 1){
			if($idade->y>1){
				return $idade->y." Anos e ".$idade->m." Meses";
			}else{
				return $idade->y." Ano e ".$idade->m." Meses";
			}
		}else if($idade->m == 1){
			if($idade->y>1){
				return $idade->y." Anos e ".$idade->m."Mês";
			}else{
				return $idade->y." Ano e ".$idade->m." Mês";
			}
		}else if($idade->y>1){
			return $idade->y." Anos";
		}else{
			return $idade->y." Ano";
		}
	}
	public function owner(){
		return $this->belongsTo('App\Models\User', 'owner_id');
	}
	public function pictures()
	{
		return $this->morphMany('App\Models\Picture', 'imageable')->where('type','picture');
	}
}
