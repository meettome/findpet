<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Chat_Message_Notification extends Model
{
    protected $fillable = [
		'message_id', 'conversation_id', 'participation_id', 'is_seen', 'is_sender'
	];

	protected $hidden = [
	];


	public function participante()
	{
		return $this->belongsTo('App\Models\Chat_Participation', 'participation_id');
	}
	public function mensagem()
	{
		return $this->belongsTo('App\Models\Chat_Message', 'message_id');
	}



	public function messageable(){
		return $this->morphTo();
	}
}
