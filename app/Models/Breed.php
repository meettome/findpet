<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Breed extends Model
{
	public $timestamps=false;
	
	public function types(){
		return $this->belongsTo("App\Models\Type", "type_id");
	}

	public function animals()
	{
		return $this->hasMany('App\Models\Animals');
	}
}
