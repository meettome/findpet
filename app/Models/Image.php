<?php

namespace App\Models;

use App\Casts\Images\Dimension;
use App\Traits\HasChildModel;
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    use HasChildModel;

    protected $fillable = [
    	'filename', 'format', 'dimensions'
    ];

    protected $casts=[
    	'dimensions' => Dimension::class,
    ];

    public function imageable()
    {
    	return $this->morphTo();
    }

    public function user()
    {
    	return $this->belongsTo('App\Models\User');
    }

    public function getFilenameAttribute()
    {
        return $this->attributes['filename'];
    }
}
