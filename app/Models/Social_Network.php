<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Social_Network extends Model
{

	public function Networks(){
		return $this->morphTo();
	}
	public function getPhoneAttribute()
    {
        return preg_replace('/(\d{2})(\d{5})(\d{4})/','($1)$2-$3',str_pad($this->attributes['phone'], 11, '0', STR_PAD_LEFT));
    }
	public function getWhatsappAttribute()
    {
        return $this->attributes['phone'];
    }
	public function type()
	{
		return $this->belongsTo('App\Models\Type_Social_Network');
	}
}
