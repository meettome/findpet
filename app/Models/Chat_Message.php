<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Chat_Message extends Model
{

	protected $fillable = [
		'participation_id', 'body', 'type'
	];

	protected $hidden = [
	];
	public function notificacoes()
	{
		return $this->hasMany('App\Models\Chat_Message_Notification');
	}
	
	public function participante()
	{
		return $this->belongsTo('App\Models\Chat_Participation');
	}
}
