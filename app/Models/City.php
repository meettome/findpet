<?php

namespace App\Models;

use App\Filters\CitiesFilter;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class City extends Model
{
	protected $fillable = [
		'name', 'capital'
	];

	protected $hidden = [
	];

	protected $casts = [
		'capital' => 'boolean',
	];

	public $timestamps=false;

	public function state()
	{
		return $this->belongsTo('App\Models\State');
	}

	public function scopeCapitais($query)
	{
		return $query->where('capital',true);
	}


	public function scopeFilter(Builder $builder, $request)
	{
		return (new CitiesFilter($request))->filter($builder);
	}

}
