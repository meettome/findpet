<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Adoption extends Model
{
    public function animals(){
        return $this->belongsTo("App\Models\Animal");
    }
    public function adopters(){
        return $this->belongsTo("App\Models\User", 'owner_id');
    }
    public function owners(){
        return $this->belongsTo("App\Models\User", 'adopter_id');
    }
}
