<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use App\Models\Email;
use Illuminate\Queue\SerializesModels;

class Contact extends Mailable
{
    use Queueable, SerializesModels;

    private $email;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Email $email)
    {
        $this->email = $email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->subject($this->email->subject);
        $this->to($this->email->email, $this->email->name);
        $this->from('almeidaaa34@gmail.com', 'FindYourPet');
        $this->cc('almeidaaa34@gmail.com');
        return $this->markdown('mail.contact', ['email'=>$this->email]);
    }
}
