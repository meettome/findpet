<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Http\Request;

class ConfirmRegister extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    private $user;
    
    public function __construct(Request $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->subject("Seja bem-vindo ao FindYourPet");
        $this->to($this->user->email, $this->user->name);
        $this->from('findpetescolhacerta@gmail.com', 'FindYourPet');
        $this->cc('almeidaaa34@gmail.com');
        return $this->markdown('mail.confirmation', ['user'=>$this->user]);
    }
}
