<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class ResetPasswordEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    private $usuario;
    private $token;
    public function __construct(Request $usuario)
    {
        $this->usuario = $usuario;
        $this->token = hash_hmac('sha256', Str::random(40), $usuario->email);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        DB::table('password_resets')->insert(['email'=>$this->usuario->email, 'token'=>$this->token]);
        $this->subject("Recuperação de senha");
        $this->to($this->usuario->email);
        $this->from('almeidaaa34@gmail.com', 'FindYourPet');
        $this->cc('almeidaaa34@gmail.com');
        return $this->markdown('mail.userresetpassword', ['user'=>$this->usuario, 'token'=>$this->token]);
    }
}
