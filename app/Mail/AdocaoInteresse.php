<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Http\Request;
use App\Models\Animal;

class AdocaoInteresse extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    private $animal;
    
    public function __construct(Animal $animal)
    {
        $this->animal = $animal;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $usuario = auth()->user();

        if($this->animal->owner->clinicsUsuario != null){
            $user =  $this->animal->owner->clinicsUsuario;
        }else if($this->animal->owner->ongsUsuario != null){
            $user =  $this->animal->owner->ongsUsuario; 
        }else{
            $user =  $this->animal->owner;
        }
        $this->subject("Interesse em seu Pet ".$this->animal->name);
        $this->to($user->email, $user->name);
        $this->from('findpetescolhacerta@gmail.com', 'FindYourPet');
        $this->cc('almeidaaa34@gmail.com');
        return $this->markdown('mail.adocaointeresse', ['usuario'=>$usuario, 'user' => $user , 'animal'=>$this->animal]);
    }
}
