<?php

namespace App\Casts\General;

use Illuminate\Contracts\Database\Eloquent\CastsAttributes;

class ZipCode implements CastsAttributes {
	public function get($model, $key, $value, $attributes)
	{
		return preg_replace('/(\d{2})(\d{3})(\d{3})/','$1.$2-$3',$value);
	}

	public function set($model, $key, $value, $attributes)
	{
		return preg_replace('/\D/','',$value);
	}
}
