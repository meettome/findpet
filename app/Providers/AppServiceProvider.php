<?php

namespace App\Providers;

use App\Console\Commands\ModelMakeCommand;
use Illuminate\Support\ServiceProvider;
use App\Models\User;
use Illuminate\Support\Facades\View;
use Illuminate\Database\Eloquent\Relations\Relation;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    
    public function register()
    {
        $this->app->extend('command.model.make', function ($command, $app) {
            return new ModelMakeCommand($app['files']);
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Relation::morphMap([
            'avatar' => 'App\Models\Avatar',
            'City' => 'App\Models\City',
            'image' => 'App\Models\Image',
            'State' => 'App\Models\State',
            'Animal' => 'App\Models\Animal',
            'Adopter' => 'App\Models\Adopter',
            'Lost' => 'App\Models\Lost',
            'logo' => 'App\Models\Logo',
            'picture' => 'App\Models\Picture',
        ]);
    }
    
}
