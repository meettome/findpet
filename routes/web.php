<?php
use Illuminate\Support\Facades\Mail;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	return view('welcome');
})->name('site');

Route::get('/sobre', function () {
	return view('about');
})->name('about');

Route::get('/servicos', function () {
	return view('services');
})->name('services');

Auth::routes();

Route::put('/email', 'EmailController@store')->name('email');
Route::get('/fale-conosco', 'EmailController@create')->name('contact');
// Auth Routes
Route::get('login','Auth\LoginController@showLoginForm')->name('login');
Route::post('login','Auth\LoginController@login');
Route::post('logout','Auth\LoginController@logout')->name('logout');

Route::get('/sistema/', 'HomeController@index')->name('home');
Route::get('/cadastro', 'Auth\RegisterController@showPerfil')->name('register');
Route::get('/cadastro/veterinario', 'Auth\ClinicController@showVeterinario')->name('vet');
Route::post('/cadastro/veterinario', 'Auth\ClinicController@store')->name('RegisterVet');
Route::get('/cadastro/ongs', 'Auth\OngController@showOngs')->name('ongs');
Route::get('/ongs', 'OngController@index')->name('ongscadastradas');
Route::get('/ongs/{id}/{name}', 'OngController@show')->name('ongs.show');
Route::post('/cadastro/ongs', 'Auth\OngController@store')->name('RegisterOngs');
Route::get('/cadastro/usuario', 'Auth\RegisterController@showUsuario')->name('fisico');
Route::post('/cadastro', 'Auth\RegisterController@store')->name('register');
Route::post('/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('/senha/alterar-senha', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::get('/password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('/password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');


Route::get('/confirmacao/{email?}', 'confirmationController@confirmacao')->name('public.confirm');

Route::group(['middleware' => 'auth'], function () {
	
	
	Route::post('/sistema/chat', 'ChatController@index')->name('chat');
	Route::post('/sistema/mensagem', 'ChatController@show')->name('conversa');
	Route::post('/sistema/chat/enviar', 'ChatController@enviamensagem')->name('mensagem');
	Route::resource('/sistema/usuario', 'UserController', ['except' => ['show']]);
	Route::resource('/sistema/animal', 'AnimalController', ['except' => ['show']]);
	Route::get('/sistema/perfil', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
	Route::get('/sistema/animal/view/{id}/{name}', 'AnimalController@show')->name('animal.show');
	Route::get('/sistema/contrato/{id}/{name}', 'PdfviewController@index')->name('contrato');
	Route::get('/sistema/animal/adicionar', ['as' => 'animal.adicionar', 'uses' => 'AnimalController@create']);
	Route::put('/sistema/perfil', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
	Route::put('/sistema/perfil/password', ['as' => 'profile.password', 'uses' => 'ProfileController@password']);
	Route::get('/sistema/adocao/search', ['as' => 'adopter.search', 'uses' => 'AdoptionController@search']);
	Route::get('/sistema/perdido/search', ['as' => 'lost.search', 'uses' => 'LostController@search']);
	Route::get('/sistema/animal/search', ['as' => 'animal.search', 'uses' => 'AnimalController@search']);
	Route::get('/sistema/animal/pesquisa', ['as' => 'animal.pesquisa', 'uses' => 'AnimalController@pesquisa']);
	Route::put('/sistema/perfil/veterinario', ['as' => 'profile.veterinario', 'uses' => 'ProfileController@updateClinic']);
	
	Route::get('/sistema/dono/{id}', 'AnimalController@dono')->name('emaildono');
	Route::put('/sistema/animal/perdido/{id}', 'LostController@localization')->name('lost.addresses');

	Route::put('/sistema/perfil/employee', ['as' => 'profile.employee', 'uses' => 'ProfileController@updateEmployee']);
	Route::put('/sistema/perfil/ong', ['as' => 'profile.ong', 'uses' => 'ProfileController@updateong']);
	Route::put('/sistema/perfil/socialnetworks', ['as' => 'profile.social', 'uses' => 'ProfileController@updateSocial']);

	Route::get('/sistema/perdidos', ['as' => 'perdidos', 'uses' => 'LostController@index']);
	Route::get('/sistema/perdidos/cachorros', ['as' => 'perdidos.dog', 'uses' => 'LostController@dog']);
	Route::get('/sistema/perdidos/furoes', ['as' => 'perdidos.furao', 'uses' => 'LostController@furao']);
	Route::get('/sistema/perdidos/gatos', ['as' => 'perdidos.cat', 'uses' => 'LostController@cat']);
	Route::get('/sistema/perdidos/roedores', ['as' => 'perdidos.rodents', 'uses' => 'LostController@rodents']);
	Route::get('/sistema/perdidos/coelhos', ['as' => 'perdidos.rabbits', 'uses' => 'LostController@rabbits']);
	Route::get('/sistema/perdidos/aves', ['as' => 'perdidos.birds', 'uses' => 'LostController@birds']);
	Route::get('/sistema/perdidos/tartarugas-jabutis-e-cagados', ['as' => 'perdidos.turtles', 'uses' => 'LostController@turtles']);

	Route::get('/sistema/adocao', ['as' => 'adocao', 'uses' => 'AdoptionController@index']);
	Route::get('/sistema/adocao/cachorros', ['as' => 'adocao.dog', 'uses' => 'AdoptionController@dog']);
	Route::get('/sistema/adocao/furoes', ['as' => 'adocao.furao', 'uses' => 'AdoptionController@furao']);
	Route::get('/sistema/adocao/gatos', ['as' => 'adocao.cat', 'uses' => 'AdoptionController@cat']);
	Route::get('/sistema/adocao/roedores', ['as' => 'adocao.rodents', 'uses' => 'AdoptionController@rodents']);
	Route::get('/sistema/adocao/coelhos', ['as' => 'adocao.rabbits', 'uses' => 'AdoptionController@rabbits']);
	Route::get('/sistema/adocao/aves', ['as' => 'adocao.birds', 'uses' => 'AdoptionController@birds']);
	Route::get('/sistema/adocao/tartarugas-jabutis-e-cagados', ['as' => 'adocao.turtles', 'uses' => 'AdoptionController@turtles']);

	Route::put('/sistema/perfil/avatar', ['as' => 'profile.updateAvatar', 'uses' => 'ProfileController@updateAvatar']);

	Route::resource('/sistema/state', 'StateController', ['except' => ['show']]);
	Route::resource('/sistema/city', 'CityController', ['except' => ['show']]);

	Route::resource('/sistema/usuario', 'UserController', ['except' => ['show']]);
	Route::resource('/sistema/empregado', 'EmployeeController', ['except' => ['show']]);
	Route::get('/sistema/empregado/adicionar', ['as' => 'employee.adicionar', 'uses' => 'EmployeeController@create']);
});

