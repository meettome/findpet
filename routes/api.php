<?php

use Illuminate\Http\Request;
use App\Models\City;
use App\Models\Breed;
use App\Models\Type;
use App\Models\Type_Social_Network;
use App\Models\State;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/




Route::get('/type', function(){
	return Type::orderBy('name','ASC')->get();
});

Route::get('/breed/{id?}', function($type_id = null){
	if($type_id)
		return Breed::where('type_id',$type_id)->orderBy('type_id','ASC')->orderBy('name', 'ASC')->get();
	else
		return Breed::all();
})->where('id','\d+');


Route::get('/cities/{id?}', function($state_id = null){
	if($state_id)
		return City::where('state_id',$state_id)->orderBy('state_id','ASC')->orderBy('name', 'ASC')->get();
	else
		return City::all();
})->where('id','\d+');


Route::get('/states', function(){
	return State::orderBy('name','ASC')->get();
});

Route::get('/typesnetworks', function(){
	return Type_Social_Network::orderBy('id','ASC')->get();
});

Route::middleware('auth:api')->get('/user', function (Request $request) {
	return $request->user();
});
