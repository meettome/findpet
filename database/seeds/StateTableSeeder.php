<?php

use Illuminate\Database\Seeder;

class StateTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$states = [
		  ['uf' => 'AC','name' => 'Acre','capital' => 0],
		  ['uf' => 'AL','name' => 'Alagoas','capital' => 0],
		  ['uf' => 'AP','name' => 'Amapá','capital' => 0],
		  ['uf' => 'AM','name' => 'Amazonas','capital' => 0],
		  ['uf' => 'BA','name' => 'Bahia','capital' => 0],
		  ['uf' => 'CE','name' => 'Ceará','capital' => 0],
		  ['uf' => 'DF','name' => 'Distrito Federal','capital' => 1],
		  ['uf' => 'ES','name' => 'Espírito Santo','capital' => 0],
		  ['uf' => 'GO','name' => 'Goiás','capital' => 0],
		  ['uf' => 'MA','name' => 'Maranhão','capital' => 0],
		  ['uf' => 'MT','name' => 'Mato Grosso','capital' => 0],
		  ['uf' => 'MS','name' => 'Mato Grosso do Sul','capital' => 0],
		  ['uf' => 'MG','name' => 'Minas Gerais','capital' => 0],
		  ['uf' => 'PA','name' => 'Pará','capital' => 0],
		  ['uf' => 'PB','name' => 'Paraíba','capital' => 0],
		  ['uf' => 'PR','name' => 'Paraná','capital' => 0],
		  ['uf' => 'PE','name' => 'Pernambuco','capital' => 0],
		  ['uf' => 'PI','name' => 'Piauí','capital' => 0],
		  ['uf' => 'RJ','name' => 'Rio de Janeiro','capital' => 0],
		  ['uf' => 'RN','name' => 'Rio Grande do Norte','capital' => 0],
		  ['uf' => 'RS','name' => 'Rio Grande do Sul','capital' => 0],
		  ['uf' => 'RO','name' => 'Rondônia','capital' => 0],
		  ['uf' => 'RR','name' => 'Roraima','capital' => 0],
		  ['uf' => 'SC','name' => 'Santa Catarina','capital' => 0],
		  ['uf' => 'SP','name' => 'São Paulo','capital' => 0],
		  ['uf' => 'SE','name' => 'Sergipe','capital' => 0],
		  ['uf' => 'TO','name' => 'Tocantins','capital' => 0]
		];
 
		DB::table('states')->insert($states);
	}
}
