<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
    	$this->call([
    		StateTableSeeder::class,
    		CityTableSeeder::class,
    		UsersTableSeeder::class,
            TypeTableSeeder::class,
            BreedTableSeeder::class,
            TypeSocialNetworksTableSeeder::class,
            ClinicTableSeeder::class,
            OngTableSeeder::class,
            SocialNetworksSeederTable::class,
            DonateTableSeeder::class,
            EmployeersTableSeeder::class,
    	]);
    }
}
