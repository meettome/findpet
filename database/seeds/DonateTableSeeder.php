<?php

use Illuminate\Database\Seeder;

class DonateTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        DB::table('donates')->insert([
        	'ong_id'=>1,
        	'url'=>"donate.com.br",
        	'account'=>"Banco do Brasil
        	Conta: 155151
        	Agencia:525"
        ]);
    }
}
