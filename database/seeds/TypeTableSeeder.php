<?php

use Illuminate\Database\Seeder;

class TypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
    {
    	$type = [
    		['name' => 'Cachorro'],
    		['name' => 'Gato'],
    		['name' => 'Roedores'],
    		['name' => 'Coelhos'],
    		['name' => 'Aves'],
    		['name' => 'Tartarugas, Jabutis e Cágados'],
            ['name' => 'Furões']
    	];

    	DB::table('types')->insert($type);
    }
}
