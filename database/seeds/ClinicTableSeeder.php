<?php

use Illuminate\Database\Seeder;

class ClinicTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $usuario = [
            [
                'name' => 'FindYourPet',
                'user_id' => 3,
                'email' => 'findpetescolhacerta@gmail.com',
                'site' => 'findyourpet.com.br',
                'crmv' => '1234586',
                'cnpj'=>'12345678911234',
                'phone'=>'82988433824',
                'cellphone'=>'82988433824',
                'created_at' => now(),
                'updated_at' => now()
            ],
        ];
        DB::table('clinics')->insert($usuario);
    }
}
