<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $usuario = [
            [
                'name' => 'André Albuquerque',
                'email' => 'admin@argon.com',
                'email_verified_at' => now(),
                'role' => 'user',
                'cpf'=>'09290105402',
                'phone'=>'82988433824',
                'birth_date'=>'1997/06/13',
                'sex'=>'male',
                'password' => Hash::make('secret'),
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'name' => 'FindPet',
                'email' => 'findpetescolhacerta@gmail.com',
                'email_verified_at' => now(),
                'role' => 'ong',
                'cpf'=>null,
                'phone'=>'82988433824',
                'birth_date'=>'2020/06/13',
                'sex'=>null,
                'password' => Hash::make('secret'),
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'name' => 'FindPet',
                'email' => 'producao@yac.com.br',
                'email_verified_at' => now(),
                'role' => 'vet',
                'cpf'=>null,
                'phone'=>'82988433824',
                'birth_date'=>'2020/06/13',
                'sex'=>null,
                'password' => Hash::make('secret'),
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'name' => 'André Almeida',
                'email' => 'herois.da.historia13@gmail.com',
                'email_verified_at' => now(),
                'role' => 'employee',
                'cpf'=>'09290105455',
                'phone'=>'82988433824',
                'birth_date'=>'1997/06/13',
                'sex'=>'male',
                'password' => Hash::make('secret'),
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'name' => 'André',
                'email' => 'jose.albuquerque092@academico.fat-al.edu.br',
                'email_verified_at' => now(),
                'role' => 'employee',
                'cpf'=>'88290105455',
                'phone'=>'82988433824',
                'birth_date'=>'1997/06/13',
                'sex'=>'male',
                'password' => Hash::make('secret'),
                'created_at' => now(),
                'updated_at' => now()
            ]
        ];
        DB::table('users')->insert($usuario);
    }
}
