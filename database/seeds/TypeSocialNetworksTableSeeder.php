<?php

use Illuminate\Database\Seeder;

class TypeSocialNetworksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$type = [
    		['type' => 'Facebook', 'icon' => 'fab fa-facebook social-icon'],
    		['type' => 'Instagram', 'icon' =>  'fab fa-instagram social-icon'],
    		['type' => 'Reddit',  'icon' => 'fab fa-reddit social-icon'],
    		['type' => 'Twitter', 'icon' => 'fab fa-twitter social-icon'],
    		['type' => 'Snapchat', 'icon' => 'fab fa-snapchat social-icon'],
    		['type' => 'Skype', 'icon' => 'fab fa-skype social-icon'],
    		['type' => 'Linkedin', 'icon' => 'fab fa-linkedin social-icon'],
    		['type' => 'Blogger', 'icon' => 'fab fa-blogger-b social-icon'],
    		['type' => 'Discord', 'icon' => 'fab fa-discord social-icon'],
    		['type' => 'Discourse', 'icon' => 'fab fa-discourse social-icon'],
    		['type' => 'Flickr', 'icon' => 'fab fa-flickr social-icon'],
    		['type' => 'Flipboard', 'icon' => 'fab fa-flipboard social-icon'],
    		['type' => 'Steam', 'icon' => 'fab fa-steam social-icon'],
    		['type' => 'Telegram', 'icon' => 'fab fa-telegram social-icon'],
    		['type' => 'Twitch', 'icon' => 'fab fa-twitch social-icon'],
    		['type' => 'Viber', 'icon' => 'fab fa-viber social-icon'],
    		['type' => 'Whatsapp', 'icon' => 'fab fa-whatsapp social-icon'],
    		['type' => 'Youtube', 'icon' => 'fab fa-youtube social-icon']
    	];

    	DB::table('type__social__networks')->insert($type);
    }
    
}
