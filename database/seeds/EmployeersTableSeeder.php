<?php

use Illuminate\Database\Seeder;

class EmployeersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        DB::table('clinic_user')->insert([
            'clinic_id'=>1,
            'user_id'=>5
        ]);
        DB::table('ong_user')->insert([
            'ong_id'=>1,
            'user_id'=>4
        ]);
    }
}
