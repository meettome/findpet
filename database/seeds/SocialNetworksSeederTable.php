<?php

use Illuminate\Database\Seeder;

class SocialNetworksSeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$user=[
    		[
    			'networks_type'=>"App\Models\User",
    			'networks_id'=>1,
    			'type_id' => 17,
    			'phone'=>"82988433824",
                'user'=> null,
                'created_at' => now(),
                'updated_at' => now()
    		],
    		[
    			'networks_type'=>"App\Models\Clinic",
    			'networks_id'=>1,
    			'type_id' => 17,
    			'phone'=>"82988433824",
                'user'=> null,
                'created_at' => now(),
                'updated_at' => now()
    		],
    		[
    			'networks_type'=>"App\Models\Ong",
    			'networks_id'=>1,
    			'type_id' => 17,
    			'phone'=>"82988433824",
                'user'=> null,
                'created_at' => now(),
                'updated_at' => now()
    		],
    		[
    			'networks_type'=>"App\Models\Ong",
    			'networks_id'=>1,
    			'type_id' => 1,
                'phone'=> null,
    			'user'=>"FindYourPet",
                'created_at' => now(),
                'updated_at' => now()
    		],
    		[
    			'networks_type'=>"App\Models\Clinic",
    			'networks_id'=>1,
    			'type_id' => 1,
                'phone'=> null,
    			'user'=>"FindYourPet",
                'created_at' => now(),
                'updated_at' => now()
    		],
    	];
    	DB::table('social__networks')->insert($user);
    }
}
