<?php

use Illuminate\Database\Seeder;

class OngTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       
        $usuario = [
            [
                'name' => 'FindYourPet',
                'user_id' => 2,
                'email' => 'producao@yac.com.br',
                'site' => 'findyourpet.com.br',
                'cnpj'=>'12345678911234',
                'phone'=>'82988433824',
                'cellphone'=>'82988433824',
                'created_at' => now(),
                'updated_at' => now()
            ],
        ];
        DB::table('ongs')->insert($usuario);
    }
}
