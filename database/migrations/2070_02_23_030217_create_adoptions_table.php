<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdoptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adoptions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('owner_id')->unsigned();
            $table->bigInteger('animal_id')->unsigned();
            $table->string('document')->nullable()->default(null);
            $table->bigInteger('adopter_id')->unsigned()->nullable()->default(null);
            $table->timestamps();
            $table->foreign('animal_id','animal_id_adoption')->references('id')->on('animals')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('owner_id','owner_id_adoption')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('adopter_id','adopter_id_adoption')->references('id')->on('users')->onDelete('set null')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('adoptions');
    }
}
