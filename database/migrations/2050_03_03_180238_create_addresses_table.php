<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->morphs('addressable');
            $table->string('zip_code')->nullable()->default(null);
            $table->bigInteger('state_id')->unsigned()->nullable()->default(null);
            $table->bigInteger('city_id')->unsigned()->nullable()->default(null);
            $table->string('street')->nullable()->default(null);
            $table->string('number')->nullable()->default(null);
            $table->string('district')->nullable()->default(null);
            $table->string('complement')->nullable()->default(null);
            $table->string('longitude')->nullable()->default(null);
            $table->string('latitude')->nullable()->default(null);

            $table->foreign('state_id','states_from_addresses')->references('id')->on('states')->onDelete('set null')->onUpdate('cascade');
            $table->foreign('city_id','cities_from_addresses')->references('id')->on('cities')->onDelete('set null')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addresses');
    }
}
