<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSocialNetworksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('social__networks', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->morphs('networks');
            $table->bigInteger('type_id')->unsigned()->nullable()->default(null);
            $table->string('user')->nullable()->default(null);
            $table->string('phone')->nullable()->default(null);

            $table->foreign('type_id','networks_from_type')->references('id')->on('type__social__networks')->onDelete('set null')->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('social__networks');
    }
}
