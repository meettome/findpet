<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDonatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('donates', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->bigInteger('ong_id')->unsigned();
            $table->string('url')->nullable()->default(null);
            $table->text('account')->nullable()->default(null);
            $table->timestamps();

            $table->foreign('ong_id','Ongs_from_Donate')->references('id')->on('ongs')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('donates');
    }
}
