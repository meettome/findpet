<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOngsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ongs', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('cnpj')->nullable()->default(null);
            $table->string('name');
            $table->string('email')->nullable()->unique()->default(null);
            $table->string('site')->nullable()->default(null);
            $table->string('phone')->nullable()->default(null);
            $table->string('cellphone')->nullable()->default(null);
            $table->timestamps();
            $table->foreign('user_id','perfil_from_ong')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ongs');
    }
}
