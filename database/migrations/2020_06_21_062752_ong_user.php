<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class OngUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ong_user', function (Blueprint $table) {
            $table->bigInteger('ong_id')->unsigned();
            $table->bigInteger('user_id')->unsigned();

            $table->foreign('user_id','user_from_ongs')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('ong_id','cong_from_user')->references('id')->on('ongs')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
