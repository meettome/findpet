<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnimalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('animals', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->bigInteger('owner_id')->unsigned();
            $table->string('name');
            $table->text('note');
            $table->enum('sex',['male','female'])->nullable()->default(null);
            $table->boolean('castration')->default(0);
            $table->boolean('vaccinated')->default(0);
            $table->boolean('dewormed')->default(0);
            $table->boolean('microchipped')->default(0);
            $table->string('rga')->nullable()->default(null);
            $table->date('birth_date')->nullable()->default(null);
            $table->bigInteger('breed_id')->unsigned()->nullable()->default(null);
            $table->bigInteger('type_id')->unsigned()->nullable()->default(null);
            $table->string('color');
            $table->enum('size',['micro','small','medium','big','giant'])->nullable()->default(null);

            $table->timestamps();

            $table->foreign('owner_id','user_from_animal')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('type_id','type_from_animal')->references('id')->on('types')->onDelete('set null')->onUpdate('cascade');
            $table->foreign('breed_id','breed_from_animal')->references('id')->on('breeds')->onDelete('set null')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('animals');
    }
}
