<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted' => 'O campo :attribute precisa ser aceito.',
    'active_url' => 'O atributo :attribute não é uma URL válida.',
    'after' => 'O atributo :attribute precisa ser uma data após :date.',
    'after_or_equal' => 'O atributo :attribute precisa ser uma data igual ou após :date.',
    'alpha' => 'O atributo :attribute pode conter somente letras.',
    'alpha_dash' => 'O atributo:attribute pode conter somente letras, números, traços ou sublinhados.',
    'alpha_num' => 'O atributo :attribute pode somente conter letras e números.',
    'array' => 'O atributo :attribute precisa ser um array.',
    'before' => 'O atributo :attribute precisa ser uma data anterior a :date.',
    'before_or_equal' => 'O atributo :attribute precisa ser uma data anterioi ou igual à :date.',
    'between' => [
        'numeric' => 'O atributo :attribute precisa estar entre :min e :max.',
        'file' => 'O atributo :attribute precisa estar entre :min e :max kilobytes.',
        'string' => 'O atributo :attribute precisa estar entre :min e :max caracteres.',
        'array' => 'O atributo :attribute precisa conter entre :min e :max items.',
    ],
    'boolean' => 'O campo :attribute precisa ser verdadeiro ou falso.',
    'confirmed' => 'A confirmação do atributo :attribute não confere.',
    'date' => 'O atributo :attribute não é uma data válida.',
    'date_equals' => 'O atributo :attribute precisa ser uma data igual à :date.',
    'date_format' => 'O atributo :attribute não corresponde ao formato :format.',
    'different' => 'O atributo :attribute e :other precisam ser diferentes.',
    'digits' => 'O atributo :attribute precisa ser de :digits dígitos.',
    'digits_between' => 'O atributo :attribute precisa ter de :min e :max dígitos.',
    'dimensions' => 'O atributo :attribute tem dimensões de imagem inválidas.',
    'distinct' => 'O campo :attribute contém um valor duplicado.',
    'email' => 'O atributo :attribute precisa ser um endereço de email válido.',
    'ends_with' => 'O atributo :attribute precisa terminar com um dos seguintes valores: :values',
    'exists' => 'O atributo selectionado :attribute é inválido.',
    'file' => 'O atributo :attribute precisa ser um arquivo.',
    'filled' => 'O campo :attribute é requerido e precisa conter um valor.',
    'gt' => [
        'numeric' => 'O atributo :attribute precisa ser maior que :value.',
        'file' => 'O atributo :attribute precisa ser maior que :value kilobytes.',
        'string' => 'O atributo :attribute precisa conter mais de :value caracteres.',
        'array' => 'O atributo :attribute precisa conter mais de :value items.',
    ],
    'gte' => [
        'numeric' => 'O atributo :attribute precisa ser maior ou igual à :value.',
        'file' => 'O atributo :attribute precisa ser maior ou igual à :value kilobytes.',
        'string' => 'O atributo :attribute precisa conter :value caracteres ou mais.',
        'array' => 'O atributo :attribute precisa conter :value items ou mais.',
    ],
    'image' => 'O atributo :attribute precisa ser uma imagem.',
    'in' => 'O atributo selecionado :attribute é inválido.',
    'in_array' => 'O campo :attribute não existe em :other.',
    'integer' => 'O atributo :attribute precisa ser um inteiro.',
    'ip' => 'O atributo :attribute precisa ser um endereço IP válido.',
    'ipv4' => 'O atributo :attribute precisa ser um endereço IPV4 válido.',
    'ipv6' => 'O atributo :attribute precisa ser um endereço IPV6 válido.',
    'json' => 'O atributo :attribute precisa ser uma string JSON válida.',
    'lt' => [
        'numeric' => 'O atributo :attribute precisa ser menor que :value.',
        'file' => ' O atributo :attribute precisa ser menor que :value kilobytes.',
        'string' => 'O atributo :attribute precisa conter menos de :value caracteres.',
        'array' => 'O atributo :attribute precisa conter menos de :value items.',
    ],
    'lte' => [
        'numeric' => 'O atributo :attribute precisa ser menor ou igual à :value .',
        'file' => 'O atributo :attribute precisa ser menor ou igual à :value kilobytes.',
        'string' => 'O atributo :attribute precisa ser menor ou igual à :value caracteres.',
        'array' => 'O atributo :attribute precisa ser menor ou igual à :value items.',
    ],
    'max' => [
        'numeric' => 'O atributo :attribute não pode ser maior que :max.',
        'file' => 'O atributo :attribute não pode ser maior que :max kilobytes.',
        'string' => 'O atributo :attribute não pode ser maior que :max caracteres.',
        'array' => 'O atributo :attribute não pode conter mais de :max items.',
    ],
    'mimes' => 'O atributo :attribute precisa ser um arquivo do tipo: :values.',
    'mimetypes' => 'O atributo :attribute precisa ser um arquivo do tipo: :values.',
    'min' => [
        'numeric' => 'O atributo :attribute precisa ser pelo menos :min.',
        'file' => 'O atributo :attribute precisa ser de pelo menos :min kilobytes.',
        'string' => 'O atributo :attribute precisa conter pelo menos :min caracteres.',
        'array' => 'O atributo :attribute precisa conter pelo menos :min items.',
    ],
    'not_in' => 'O atributo selecionado :attribute é inválido.',
    'not_regex' => 'O formato do atributo :attribute é inválido.',
    'numeric' => 'O atributo :attribute precisa ser um número.',
    'password' => 'A senha está incorreta.',
    'present' => 'O campo :attribute precisa estar presente.',
    'regex' => 'O formato do atributo :attribute é inválido.',
    'required' => 'O campo :attribute é requerido.',
    'required_if' => 'O campo :attribute é requerido quando :other é :value.',
    'required_unless' => 'O campo :attribute é requerido a não ser que :other contenha um dos valores: :values.',
    'required_with' => 'O campo :attribute é requerido quando :values está presente.',
    'required_with_all' => 'O campo :attribute é requerido quando um dos valores :values estão presentes.',
    'required_without' => 'O campo :attribute é requerido quando o valor :values não esteja presente.',
    'required_without_all' => 'O campo :attribute é requerido quando nenhum dos valores :values estejam presentes.',
    'same' => 'O atributo :attribute e :other precisam ser iguais.',
    'size' => [
        'numeric' => 'O atributo :attribute ter o tamanho de :size.',
        'file' => 'O atributo :attribute precisa ser de :size kilobytes.',
        'string' => 'O atributo :attribute precisa conter :size caracteres.',
        'array' => 'O atributo :attribute precisa conter :size items.',
    ],
    'starts_with' => 'O atributo :attribute precisa começar com um dos valores: :values',
    'string' => 'O atributo :attribute precisa ser uma string.',
    'timezone' => 'O atributo :attribute precisa ser uma timezone válida.',
    'unique' => 'O atributo :attribute já foi utilizado.',
    'uploaded' => 'O upload atributo :attribute falhou.',
    'url' => 'O formato do atributo :attribute não é uma URL válida.',
    'uuid' => 'O atributo :attribute precisa ser uma UUID válida.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */
    'check_hashed_pass' => 'Estas credenciais não foram encontradas em nossos registros.',

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [],

];
