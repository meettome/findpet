<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'Sua senha foi redefinida com sucesso.',
    'sent' => 'O link para redefinição de senha foi enviada para seu email.',
    'token' => 'Este token para redefinição de senha é inválido.',
    'user' => "Ops! Não encontramos um usuário com este email.",
    'throttled' => 'Por favor, aguarde antes de tentar novamente.',

];
