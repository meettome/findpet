@extends('layouts.guestapp', ['class' => 'bg-white',
'menu' =>'bg-transparent', 'itens' => 'text-white', 'title' => 'Home'])

@section('content')

@include('layouts.headers.banner', ['descricao' => 'Seja bem vindo ao FindYourPet', 'banner' => 'bg-background-image'])

<div class="bg-gradient-secondary">
    <div class="container mt--10 pb-5 py-6 pl-4 pr-4">
        <div class="row justify-content-center">
            <div class="col-lg-6 col-md-8">
                <div class="form-group">
                    <h1 class="text-white text-center">Apresentação</h1>
                </div>
                <div class="form-group">
                    <p class="text-white text-center">Conforme uma pesquisa feita pela Organização Mundial da Saúde (OMS), em 2014, estima-se que no Brasil existem mais de 30 milhões de animais abandonados, sendo 10 milhões de gatos e 20 milhões de cachorros, os animais que se encontram em situação de rua provavelmente já fizeram parte de uma família e possuíam um lar, porém vieram a ser abandonados por seus próprios donos devido a  uestões socioeconômicas, religiosas ou culturais. Devido a isso o abandono de animais domésticos tem aumentado a cada dia, passando a ser um desafio do bem-estar dos animais e de saúde pública.</p>
                </div>
                <div class="form-group text-center">
                    <a class="btn btn-secondary" href="{{ route('about') }}" >Saiba Mais</a>
                </div>

            </div>
        </div>
    </div>
</div>
<div class="bg-gradient-default fusion-image-hovers">
    <div class="row">
        <div class="hover-type-liftup fusion-column-inner-bg container col mt--10 pb-5 py-8 pl-4 pr-4">
            <div class="row justify-content-center">
                <div class="col-lg-10 col-md-10">
                    <div class="form-group">
                        <h1 class="text-white text-center">Serviços</h1>
                    </div>
                    <div class="form-group">
                        <p class="text-white text-justify">Temos como principal objetivo facilitar a adoção dos animais, e ajudar na busca de seu Pet perdido. Dentro de nossos sistema tempos alguns serviços, que talvez ajude a você, seja na procura de um veterinario mais proximo, ou na adoção ou então na localização de seu Pet, confira todos os nossos serviços clicando no botão de saiba mais.</p>
                    </div>
                    <div class="form-group text-center">
                        <a class="btn btn-secondary" href="{{ route('services') }}" >Saiba Mais</a>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-md-6 hover-type-liftup fusion-column-inner-bg bg-background-catdog  pb-5 py-8 ">
        </div>
    </div>
</div>
@endsection
