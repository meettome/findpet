<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Contrato {{$animal->name}}</title>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Raleway', sans-serif;
            font-weight: 100;
            height: 200vh;
            margin: 40 30;
        }
        h1{
            color: black;
            font-size: 12pt;
            margin-bottom: 30px;
        }
        p{
            font-size: 12pt;
            color: black; 
            font-weight: 400; 
            text-align: justify;
            margin-bottom: 30px;
        }


        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 12px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }
    </style>
</head>
<body>
    <div class="flex-center position-ref full-height">
        <div class="form-group">
            <h1 style="text-align: center;">CONTRATO DE ADOÇÃO DE ANIMAL DOMÉSTICO</h1>
        </div>
        <div class="form-group">
            <p style="color: black; font-weight: 400"><strong>I - <u>Da Identificação das Partes Contratantes e do Objeto do Contrato:</u></strong></p>
        </div>
        <div class="form-group">
            <p style="color: black; font-weight: 400; margin-bottom: 20px">Pelo presente instrumento particular de <strong>ADOÇÃO DE ANIMAL DOMÉSTICO</strong>, de um lado, na condição de <strong>ADOTANTE</strong>, {{$usuario->name}}, inscrito no @if($clin)CNPJ nº {{$usuario->cnpj}}@else CPFº nº {{$usuario->cpf}}@endif, @if(sizeof($usuario->addresses)>0)residente e domiciliado(a) à {{$usuario->addresses[0]->street}}, {{ $usuario->addresses[0]->number }}, @if($usuario->addresses[0]->complement!=null){{$usuario->addresses[0]->complement}},@endif {{$usuario->addresses[0]->district}}, {{$usuario->addresses[0]->city->name}} - {{$usuario->addresses[0]->state->uf}}, Cep: {{$usuario->addresses[0]->zip_code}},@endif com telefone de número {{$usuario->phone}}, e, de outro lado, como <strong>PROTETOR(A) DOADOR(A)</strong> {{$dono->name}},  @if($user)CNPJ nº {{$dono->cnpj}}@else CPFº nº {{$usuario->cpf}}@endif ficam  justos e contratados, nos seguintes termos:</p>
            <p style="color: black; font-weight: 400; text-align: justify;"><strong>CLÁUSULA 1ª</strong> -  Por livre e espontânea vontade, sem coação ou influência de quem quer que seja, a parte <strong>ADOTANTE</strong>, de forma gratuita, ajusta com a segunda, de ora em diante denominada <strong>PROTETOR (A) DOADOR(A)</strong>, contrato relativo à adoção de animal doméstico, aqui denominado <strong>ADOTADO</strong>, da espécie {{$animal->types->name}}, {{$animal->sexo}}, {{$dados}} @if($animal->rga!=null || $animal->rga != "")RGA n° {{$animal->rga}},@endif @if($animal->microchipped==1){{_('microchipado,')}} @endif com raça definida, nesta última hipótese da raça {{$animal->breeds->name}}, resgatado diretamente de(a) _________________________________, por _____________________________, e advindo para adoção  de(a) {{$animal->name}}, com idade de {{$animal->idade}}, com pelagem na cor {{$animal->color}}, e as demais características que seguem: __________________________________________, aceitando as condições e os termos estabelecidos.</p>
        </div>


        <div class="form-group">
            <p><strong>I – <u>Das Obrigações:</u></strong></p>
        </div>
        <div class="form-group">
            <p><strong>CLÁUSULA 2ª</strong> –  O Adotante assumirá a obrigação de propiciar ao ADOTADO educação, bem-estar, cuidados com sua saúde física e psicológica e todos os demais que se fizerem necessários para lhe possibilitar uma vida digna, atento aos termos do artigo 225, caput, da Constituição da República Federativa do Brasil de 1988, em vigor, a qual dispõe que <strong><i>"Todos têm direito ao meio ambiente ecologicamente equilibrado, bem de uso comum do povo e essencial à sadia qualidade de vida, impondo-se ao Poder Público e à coletividade o dever de defendê-lo e preservá-lo para as presentes e futuras gerações"</strong></i>.</p>
            <p><strong>CLÁUSULA 3ª</strong> - O ADOTANTE declara que ao assinar este contrato, após sua leitura completa, está ciente, é maior de 21 anos e fica de acordo com as cláusulas ora presentes, sendo responsável por seu cumprimento, sujeito às sanções da Lei Federal nº 9605 de 12 de fevereiro de 1998 (Lei de Crimes Ambientais), ciente de que <strong><i><u>abandonar, soltar, deixar fugir, colocar em risco de morte, não alimentar, acorrentar, bater e amedrontar são formas de maus tratos com pena prevista na lei</u></i></strong>,  comprometendo-se veementemente a resguardar o bem-estar do <strong>ADOTADO</strong>, na saúde e na doença, até a sua velhice.</p>
            <p><strong>CLÁUSULA 4ª</strong> – Na hipótese de não ter sido o <strong>ADOTADO</strong> ainda esterilizado, por não ter idade suficiente para tanto, o <strong>ADOTANTE</strong> fica obrigado a esterilizá-lo, ciente de que, em havendo a procriação do animal, o que não é permitido, restará caracterizado descumprimento de condição contratual, e a guarda do animal poderá ser retomada imediatamente pelo(a) <strong>PROTETOR(A) DOADOR(A)</strong>, bem como a guarda de todos os seus filhotes, os quais deverão ser entregues conjuntamente com o animal  ora adotado, caso em que os custos havidos da busca e apreensão do animal, esterilização e encaminhamento para adoção dos filhotes serão ressarcidos ao protetor(a) doador(a). </p>
            <p><strong>CLÁUSULA 5ª</strong> – O <strong>ADOTANTE</strong> compromete-se ainda a levar o ADOTADO para consulta veterinária, quando necessitar, e para vacinação de polivalente e antirrábica, pelo menos uma vez por ano, salvo pactuado em contrário. Ainda compromete-se a colocar redes de proteção ou telas nas janelas e sacadas ou, nas casas, cercas ou muros, que impeçam a passagem do animal para a rua, quando resultar perigo, podendo dispor em contrário. E, em hipótese alguma, o adotante poderá fazer ou mandar fazer qualquer tipo de cirurgia de mutilação, tais como corte de orelhas, rabos, cordas vocais; ou no caso de gatos, extração das garras. </p>
            <p><strong>II – <u>Da Responsabilidade e Encargos</u></strong></p>
            <p><strong>CLÁUSULA 6º</strong> - A partir da <strong>ADOÇÃO</strong>, a responsabilidade pelo <strong>ADOTADO</strong> e os respectivos encargos financeiros com alimentação, vacinações, tratamentos, cirurgias, consultas médicas-veterinárias, são exclusivamente do(a) <strong>ADOTANTE</strong>, o qual deve procurar imediatamente um Veterinário credenciado para promover as vacinações, tratamentos e vermifugações necessárias</p>
            <p><strong>CLÁUSULA 7ª</strong> - As obrigações ora contratadas não acarretam qualquer ônus para o(a) <strong>PROTETOR(A) DOADOR(A)</strong>, seja pessoa física ou entidade de proteção animal. Ademais, desde que acordado anteriormente pelas partes, poderão ser cobrados os custos acarretados pelo transporte do animal ADOTADO e respectiva entrega ao(à) <strong>ADOTANTE</strong>, que deverá ser realizada em sua residência ou em outro local acordado entre as partes, exceto se, em ficando o novo lar em cidade diversa, ocorrer devolução infundada e imediata por exigência do <strong>ADOTANTE</strong>, hipótese em que poderão ser cobrados tais custos. </p>
            <p><strong>III - <u>Da proteção permanente:</u></strong></p>
            <p><strong>CLÁUSULA 8ª</strong> – O ADOTADO continuará sob proteção permanente do(a) <strong>PROTETOR(A) DOADOR(A)</strong>, podendo ser por ele visitado ou por seu representante, em qualquer época, devendo, porém, a data ser previamente combinada com o <strong>ADOTANTE</strong>. Declara ainda que todos seus familiares estão de acordo com a adoção.</p>
            <p><strong>CLÁUSULA 9ª</strong> –  Se por ocasião de uma das visitas forem verificados maus-tratos de qualquer natureza, o <strong>ADOTADO</strong> será retirado de sua guarda, imediatamente, bem como efetuados os registros necessários à sujeição do <strong>ADOTANTE</strong> às sanções previstas na legislação pertinente</p>
            <p><strong>CLÁUSULA 10ª</strong>  - Na hipótese de o <strong>ADOTADO</strong> não se adaptar ao novo lar, antes de qualquer atitude, o(a) <strong>ADOTANTE</strong> fica obrigado(a) a comunicar ao(à) <strong>PROTETOR(A) DOADOR(A)</strong> e, se for o caso,  devolvê-lo, ficando vedada a doação a terceira pessoa sem a sua permissão. De igual forma, sempre que solicitado, havendo dúvidas em relação ao bem-estar do animal ou notícias de maus-tratos, deverá o(a) <strong>ADOTANTE</strong> prestar-lhe informações claras e objetivas sobre o <strong>ADOTADO</strong>, por telefone ou e-mail.</p>
            <p><strong>CLÁUSULA 11ª</strong>  - Em sendo verificada negligência, imprudência, maus-tratos, abandono, facilitação de fuga do <strong>ADOTADO</strong>, poderá o(a) <strong>PROTETOR(A) DOADOR(A)</strong> tomar as medidas cabíveis à sua localização e recuperação, tais como realizar buscas, distribuir panfletos, utilizar-se de todos os meios de comunicação e, após a captura, submetê-lo a consulta e tratamento veterinário, hipóteses em que todos os custos serão de plena e total responsabilidade do(a) <strong>ADOTANTE</strong> que praticou ou que permitiu a prática de quaisquer dos atos supra descritos.</p>
            <p><strong>IV - <u>Da Rescisão e Multa Contratual:</u></strong></p>
            <p><strong>CLÁUSULA 12ª</strong> – O descumprimento de qualquer das cláusulas estabelecidas gera automaticamente a rescisão contratual, autorizando a busca e apreensão imediata do animal (extrajudicialmente ou judicialmente). Ademais, a ocorrência de infração contratual implicará na aplicação de <strong>MULTA</strong> de R$ 200,00, tendo por termo inicial a data da devolução do animal adotado, a ser paga pelo <strong>ADOTANTE</strong> ao(a) <strong>PROTETOR(A) DOADOR(A)</strong>, valor este a ser utilizado nos gastos imediatos com o <strong>ADOTADO</strong> em situação transitória até a nova adoção.</p>
            <p><strong>V - <u>Das Condições Gerais:</u></strong></p>
            <p><strong>CLÁUSULA 13ª</strong> – O presente contrato passa a vigorar entre as partes a partir da assinatura do mesmo.</p>
            <p><strong>CLÁUSULA 14ª</strong> – Para dirimir quaisquer controvérsias oriundas do presente Contrato, as partes elegem o Foro da Comarca de .............................../.</p>
            <p><strong>E ASSIM</strong>, por estarem justos e contratados, <strong>ADOTANTE e PROTETOR(A) DOADOR(A)</strong> assinam o presente instrumento particular de <strong>ADOÇAO DE ANIMAL DOMÉSTICO</strong>, em duas vias de igual teor, na presença de duas testemunhas que a tudo assistiram e também o subscrevem.</p>
            <p style="text-align: center; margin-bottom: 20px"><strong>LOCAL E DATA</strong></p>
            <p style="text-align: center; margin-bottom: 20px">_______________________________________</p>
            <p style="text-align: center; margin-bottom: 20px"><strong>ADOTANTE</strong></p>
            <p style="text-align: center; margin-bottom: 20px">_______________________________________</p>
            <p style="text-align: center; margin-bottom: 20px"><strong>PROTETOR (A) DOADOR (A)</strong></p>
            <p style="text-align: center; margin-bottom: 20px">_______________________________________</p>
            <p style="text-align: center; margin-bottom: 20px"><strong>TESTEMUNHAS:</strong></p>
            <p style="text-align: center; margin-bottom: 20px">_______________________________________</p>
            <p style="text-align: center; margin-bottom: 20px">_______________________________________</p>


        </div>

    </div>
</body>
</html>