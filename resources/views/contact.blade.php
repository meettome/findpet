@extends('layouts.guestapp', ['class' => 'bg-white',
'menu' =>'bg-transparent', 'itens' => 'text-muted', 'title' => 'Fale Conosco'])

@section('content')

@include('layouts.headers.banner', ['descricao' => '', 'banner' => 'bg-background-contact'])
<div class="bg-gradient-secondary">
    <div class="mt--5 pb-5 ">
        <div class="row justify-content-center">
            <div class="col-lg-7 col-md-7">
                <div class="card bg-secondary shadow border-0">

                    <div class="card-body px-lg-5 pt-5 pb-3">
                        <div class="text-center text-white mb-3">
                            <h1 class="text-white text-center">Tem alguma duvida? fale conosco!</h1>
                            <br>
                            <p class="text-white text-justify">Registre seus dados no formulario a seguir e descreva sua duvida, logo, logo você será notificado via email.</p>
                        </div>

                    </div>
                    <div class="card-body form-group text-center px-lg-5">

                        @if (session('status'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {{ session('status') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @endif
                        <form role="form" method="POST" action="{{ route('email') }}"> 
                            @csrf
                            @METHOD('PUT')
                            <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                                <div class="input-group input-group-alternative mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="ni ni-hat-3"></i></span>
                                    </div>
                                    <input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" placeholder="{{ __('Nome Completo') }}" type="text" name="name" value="{{ old('name') }}" required>
                                </div>
                                @if ($errors->has('name'))
                                <span class="invalid-feedback" style="display: block;" role="alert">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group {{ $errors->has('email') ? ' has-danger' : '' }}">
                                <div class="input-group input-group-alternative mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                                    </div>
                                    <input class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="{{ __('Email') }}" type="email" name="email" value="{{ old('email') }}" required>
                                </div>
                                @if ($errors->has('email'))
                                <span class="invalid-feedback" style="display: block;" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group {{ $errors->has('subject') ? ' has-danger' : '' }}">
                                <div class="input-group input-group-alternative mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="ni ni-chat-round"></i></span>
                                    </div>
                                    <input class="form-control{{ $errors->has('subject') ? ' is-invalid' : '' }}" placeholder="{{ __('Assunto') }}" type="subject" name="subject" value="{{ old('subject') }}" required>
                                </div>
                                @if ($errors->has('subject'))
                                <span class="invalid-feedback" style="display: block;" role="alert">
                                    <strong>{{ $errors->first('subject') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group {{ $errors->has('message') ? ' has-danger' : '' }}">
                                <div class="input-group input-group-alternative mb-3">

                                    <textarea class="form-control{{ $errors->has('message') ? ' is-invalid' : '' }}" placeholder="{{ __('Mensagem') }}" rows="6" name="message" required>{{ old('message') }}</textarea>
                                </div>
                                @if ($errors->has('message'))
                                <span class="invalid-feedback" style="display: block;" role="alert">
                                    <strong>{{ $errors->first('message') }}</strong>
                                </span>
                                @endif
                            </div>

                            <div class="text-center">
                                <button type="submit" class="btn btn-primary mt-4 text-uppercase">{{ __('Enviar') }}</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
