@extends('layouts.guestapp', ['class' => 'bg-white',
'menu' =>'bg-transparent', 'itens' => 'text-white', 'title' => 'Sobre'])

@section('content')
@include('layouts.headers.banner', ['descricao' => 'Sobre o FindYourPet', 'banner' => 'bg-background-cat'])

<div class="bg-gradient-secondary fusion-image-hovers">
    <div class="row">
        <div class="hover-type-liftup fusion-column-inner-bg container col-md-6 mt--10 pb-5 py-8 pl-4 pr-4">
            <div class="row justify-content-center">
                <div class="col-lg-10 col-md-10">
                    <div class="form-group">
                        <h1 class="text-white text-center">{{ __('Apresentação') }}</h1>
                    </div>
                    <div class="form-group">
                        <h3 class="text-white text-justify">{{ __('Conforme uma pesquisa feita pela Organização Mundial da Saúde (OMS), em 2014, estima-se que no Brasil existem mais de 30 milhões de animais abandonados, sendo 10 milhões de gatos e 20 milhões de cachorros, os animais que se encontram em situação de rua provavelmente já fizeram parte de uma família e possuíam um lar, porém vieram a ser abandonados por seus próprios donos devido a  uestões socioeconômicas, religiosas ou culturais. Devido a isso o abandono de animais domésticos tem aumentado a cada dia, passando a ser um desafio do bem-estar dos animais e de saúde pública.') }}</h3>
                        <blockquote class="text-white text-justify">{{ __('Segundo dados do Instituto Brasileiro de Geografia e Estatística (IBGE), em 44% das casas dos brasileiros vive pelo menos um cachorro. Estes números revelam, portanto, que a população de cães domésticos no Brasil chega a 52,2 milhões. Parece uma análise positiva, considerando a grande quantidade de animais, mas não é. Enquanto parte dos peludos vivem felizes e quentinhos entre quatro paredes, estudos da OMS mostram que 20 milhões deles habitam as ruas das cidades, acometidos por doenças, maus tratos e se reproduzindo sem controle.  (GORAYEB, 2017, G1 Grande Minas)') }}</blockquote>

                    </div>

                </div>
            </div>
        </div>

        <div class="col-md-6 hover-type-liftup fusion-column-inner-bg bg-background-catdoguin pb-5 py-8" style="padding: 0">
        </div>
    </div>
</div>
<div class="bg-gradient-default fusion-image-hovers">
    <div class="row">
        <div class="col-md-6 hover-type-liftup fusion-column-inner-bg bg-background-furao  pb-5 py-8 d-sm-none " style="padding: 0">
        </div>
        <div class="hover-type-liftup fusion-column-inner-bg container col-md-6 mt--10 pb-5 py-8 pl-4 pr-4">
            <div class="row justify-content-center">
                <div class="col-lg-10 col-md-10">
                    <div class="form-group">
                        <h3 class="text-white text-justify">{{ __('Devido a estes pontos esse projeto, inicialmente chamado "Find Pet", irá ser um site e aplicativo, para todas as plataformas (IOS ou Android) e irá buscar diminuir a quantidade de animais nas ruas e dar um lar a eles, por meio da função "adoção" onde os usuários que querem doar um animal, por não ter mais condição de cuidar do pet, vão a nosso aplicativo ou site e possam anunciar que está doando seu pet, o usuário que estiver buscando vai colocar as características e a raça do pet para buscar, e por meio de uma Inteligência Artificial (IA) o aplicativo ou site vai buscar no banco o pet que mais tem relação com a busca do usuário. Após encontrar, o software irá dar "match" no animal que bate com as características e mostrará o nome, perfil e foto do pet em questão, caso o usuário goste, ele irá entrar em contato com o dono do pet, fazendo um contrato de adoção com o dono, para garantir que o animal irá ter os devidos cuidados.') }}</h3>
                        <blockquote class="text-white text-justify">{{ __('Os cuidadores de cães e gatos são unânimes ao afirmar: o amor por um animal não tem raça ou cor. A relação com os amiguinhos de quatro patas deveria envolver carinho, alegrias e algumas responsabilidades, não mais que isto. Infelizmente, os mesmos critérios estéticos que muitas vezes excluem pessoas dos meios sociais, têm sido levados até os bichinhos, através dos humanos. (GORAYEB, 2017, G1 Grande Minas)') }}</blockquote>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-md-6 d-md-none fusion-column-inner-bg bg-background-furao  pb-5 py-8 " style="padding: 0">
        </div>
    </div>
</div>
<div class="bg-gradient-warning fusion-image-hovers">
    <div class="row">
        <div class="hover-type-liftup fusion-column-inner-bg container col-md-6 mt--10 pb-5 py-8 pl-4 pr-4">
            <div class="row justify-content-center">
                <div class="col-lg-10 col-md-10">
                    <div class="form-group">
                        <h3 class="text-white text-justify">{{ __('Além da função "Adotar" esse sistema irá contar com a função "Procura-se" e "Procura-se veterinário"; a função "Procura-se" irá ajudar as pessoas que estão procurando seu pet perdido. Caso ele tenha fugido, o usuário cadastra os dados do pet, colocando a sua última localização e algum usuário (caso) encontre esse pet e vê-lo no sistema, ele entra em contato com o dono do pet; e a função "Procura-se veterinário" irá buscar ajudar o dono do pet a encontrar uma clínica veterinária mais próxima de sua localização, as clínicas que tiverem interesse em utilizar o aplicativo poderão cadastrar seus dados e localização, para que donos de pet possa encontrá-la.') }}</h3>
                    </div>

                </div>
            </div>
        </div>

        <div class="col-md-6 hover-type-liftup fusion-column-inner-bg bg-background-hamster  pb-5 py-8 " style="padding: 0">
        </div>
    </div>
</div>

@endsection
