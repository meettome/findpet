@extends('layouts.app', ['title' => __($title)])


@section('content')
@include('profile.partials.header', [
'title' => __($title),
'class' => 'col-lg-12'
])
<div class="container-fluid mt--7">
    <div class="row">
        <div class="col">

            <div class="card shadow border-0">
                <div class="card-header bg-secondary border-0">

                    <form method="get" action="{{ route('lost.search') }}" autocomplete="off" >
                        @csrf
                        <div class="row align-items-center">
                            <div class="col-md-5{{ $errors->has('type_id') ? ' has-danger' : '' }}">
                                <div class="input-group input-group-alternative mb-3">
                                    <select id="filterType" name="type_id" class="dynamic form-control" data-endpoint="api/type" data-labelfield="name" data-valuefield="id" data-current="{{ $type_id ?? old('type_id') }}" data-targetid="filterBreed" data-updateeventtype="updateBreedSelect">
                                        <option value="">Selecione um Tipo</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-5{{ $errors->has('breed_id') ? ' has-danger' : '' }}">
                                <div class="input-group input-group-alternative mb-3">
                                    @if(isset($type_id))
                                    <select id="filterBreed" name="breed_id" class="dynamic form-control" data-endpoint="{{ $type_id?"api/breed/{$type_id}":"" }}" data-labelfield="name" data-valuefield="id" data-current="{{ $breed_id ?? old('breed_id')  }}">
                                        @else
                                        <select id="filterBreed" name="breed_id" class="dynamic form-control" data-labelfield="name" data-valuefield="id" data-current="{{ $breed_id ?? old('breed_id') }}">
                                            @endif
                                            <option value="">Selecione uma raça</option>
                                        </select>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-2 text-center">
                                <button type="submit" class="btn btn-primary mb-3">{{ __('Pesquisar') }}</button>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
            <div class="form-group row justify-content-center" style="margin-top: 20px">
                <?php $i=1 ?>
                <?php for ($j=0; $j<sizeof($animals);$j++) { ?>
                    @if($animals[$j]->lost)
                    <div class="col-md-4"> 
                        <div class="form-group text-center bg-gradient-default ">
                            <div class="fusion-image-hovers" style="padding-top: 1.25rem;">
                                @if($animals[$j]->avatars)
                                <div class="form-group bg-background-animals rounded-circle hover-type-liftup  justify-content-center" style="background: url('/storage/animal/featured/{{$animals[$j]->id}}/{{$animals[$j]->avatars}}')">
                                </div>
                                @else
                                <div class="form-group bg-background-animals rounded-circle hover-type-liftup  justify-content-center" style="background: url('/animals/avatar.png')">
                                </div>
                                @endif
                            </div>
                            <div class="card-header bg-transparent">
                                <div class="form-group">
                                    <h2 class="text-center text-white">{{$animals[$j]->name}}</h2>
                                    <h4 class="text-center text-white">{{$animals[$j]->sexo}}</h4>
                                    <a class="btn btn-primary" href="{{ route('animal.show', ['id'=>$animals[$j]->id, 'name'=>$animals[$j]->name]) }}">Visualizar animal</a>

                                </div>
                            </div>
                        </div>
                    </div>
                    <?php if($i%3==0) { ?> 
                    </div>
                    <div class="form-group row">
                    <?php } $i++; ?>
                    @endif
                <?php } ?>

            </div>
        </div>
    </div>


    @include('layouts.footers.auth')

    @push('js')

    <script src="/js/poly.js" type="text/javascript" charset="utf-8"></script>
    <script src="/js/vendor.js" type="text/javascript" charset="utf-8"></script>
    <script src="/js/main.js" type="text/javascript" charset="utf-8"></script>
    <script src="/js/jquery.mask.js" type="text/javascript" charset="utf-8"></script>
    @endpush
</div>
@endsection