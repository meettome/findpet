@extends('layouts.guestapp', ['class' => 'bg-white',
'menu' =>'bg-transparent', 'itens' => 'text-white', 'title' => 'Sobre'])

@section('content')

@include('layouts.headers.banner', ['descricao' => 'Serviços do FindYourPet', 'banner' => 'bg-background-catverso'])

<div class="bg-gradient-secondary fusion-image-hovers">
    <div class="row">
        <div class="hover-type-liftup fusion-column-inner-bg container col-md-6 mt--10 pb-5 py-8 pl-4 pr-4">
            <div class="row justify-content-center">
                <div class="col-lg-10 col-md-10">
                    <div class="form-group">
                        <h1 class="text-white text-center">{{ __('Adoção') }}</h1>
                    </div>
                    <div class="form-group">
                        <h3 class="text-white text-justify">{{ __('Nosso serviço de adoção conta com uma inteligencia artificial que identifica seu tipo de animal preferido, atraves de sua seleção de animais') }}</h3>

                    </div>

                </div>
            </div>
        </div>
        <div class="col-md-6 hover-type-liftup fusion-column-inner-bg bg-background-passarinho  pb-5 py-8" style="padding: 0">

        </div>
    </div>
</div>
</div>
<div class="bg-gradient-default fusion-image-hovers">

    <div class="row">

        <div class="col-md-6 hover-type-liftup fusion-column-inner-bg bg-background-viralata  pb-5 py-8 d-sm-none" style="padding: 0">

        </div>
        <div class="hover-type-liftup fusion-column-inner-bg container col-md-6 mt--10 pb-5 py-8 pl-4 pr-4">
            <div class="row justify-content-center">
                <div class="col-lg-10 col-md-10">
                    <div class="form-group">
                        <h1 class="text-white text-center">{{ __('Localização de animais') }}</h1>
                    </div>
                    <div class="form-group">
                        <h3 class="text-white text-justify">{{ __('Nosso serviço de Localização de Animais tem o proposito de ajuda-lo a encontrar seu animal o mais rapido possivel, identificando a sua ultima localização, registrada tanto por você como pelo o usuario que o encontrou.') }}</h3>

                    </div>

                </div>
            </div>
        </div>
        <div class="col-md-6 fusion-column-inner-bg bg-background-viralata  pb-5 py-8 d-md-none" style="padding: 0">

        </div>
    </div>
</div>
<div class="bg-gradient-warning fusion-image-hovers">
    <div class="row">
        <div class="hover-type-liftup fusion-column-inner-bg container col-md-6 mt--10 pb-5 py-8 pl-4 pr-4">
            <div class="row justify-content-center">
                <div class="col-lg-10 col-md-10">
                    <div class="form-group">
                        <h1 class="text-white text-center">{{ __('Localização de Veterinario / Clinicas mais proximo(as)') }}</h1>
                    </div>
                    <div class="form-group">
                        <h3 class="text-white text-justify">{{ __('Localização de Veterinario / Clinicas tem o intuito de mostra-lo onde tem um veterinario mais proximo, contando com a ajuda dos veterinarios registrados no sistema.') }}</h3>

                    </div>

                </div>
            </div>
        </div>

        
        <div class="col-md-6 hover-type-liftup fusion-column-inner-bg bg-background-catdoidao  pb-5 py-8" style="padding: 0">

        </div>

    </div>
</div>
<div class="bg-gradient-danger d-sm-none">
    <div class="fusion-column-inner-bg container col-md-12 mt--10 pb-5 py-8 pl-4 pr-4">
        <div class="row justify-content-center">
            <div class="col-lg-10 col-md-10">
                <div class="form-group">
                    <h1 class="text-white text-center">{{ __('O que esta esperando? faça ja o login.') }}</h1>
                    <div class="form-group text-center">
                        <a class="btn btn-success text-white text-uppercase" href="{{ route('login') }}" >Clique aqui e faça seu login</a>
                    </div>
                </div>
                <div class="form-group">
                    <h1 class="text-white text-center">{{ __('Caso não tenha cadastro, faça ja, rapido e facil') }}</h1>
                    <div class="form-group text-center">
                        <a class="btn btn-primary text-white text-uppercase" href="{{ route('register') }}" >Clique aqui e faça seu Cadastro</a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<div class="bg-gradient-danger d-md-none">
    <div class="fusion-column-inner-bg container col-md-12 mt--10 pb-5 py-8 pl-4 pr-4">
        <div class="row justify-content-center">
            <div class="col-lg-10 col-md-10">
                <div class="form-group">
                    <h2 class="text-white text-center">{{ __('O que esta esperando? faça ja o login.') }}</h2>
                    <div class="form-group text-center">
                        <a class="btn btn-success  w-100 text-white text-uppercase" href="{{ route('login') }}" style="font-size: 11px">Clique aqui e faça seu login</a>
                    </div>
                </div>
                <div class="form-group">
                    <h1 class="text-white text-center">{{ __('Caso não tenha cadastro, faça ja, rapido e facil') }}</h1>
                    <div class="form-group text-center">
                        <a class="btn btn-primary w-100 text-white text-uppercase" href="{{ route('register') }}"  style="font-size: 11px">Clique aqui e faça seu Cadastro</a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

@endsection
