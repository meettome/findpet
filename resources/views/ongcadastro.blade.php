@extends('layouts.guestapp', ['class' => 'bg-white',
'menu' =>'bg-transparent',  'itens' => 'text-white', 'title' => 'Ongs Cadastradas'])

@section('content')

@include('layouts.headers.banner', ['descricao' => 'Ongs Cadastradas', 'banner' => 'bg-background-image'])
<div class="bg-gradient-secondary">
    <div class="mt--7 pb-5  justify-content-center">

        <div class="col-lg-12 col-md-12">
            <div class="card-body form-group text-center px-lg-5">

                    <div class="form-group row justify-content-center">
                        <?php $i=1 ?>
                        <?php for ($j=0; $j<sizeof($ongs);$j++) { ?>
                            <div class="col-md-4"> 
                                <div class="form-group text-center bg-gradient-default ">
                                    @if($ongs[$j]->perfil->avatar)
                                    <div class="fusion-image-hovers" style="padding-top: 1.25rem;">
                                        <div class="form-group bg-background-ongs rounded-circle hover-type-liftup  justify-content-center" style="background: url('/storage/avatar/{{$ongs[$j]->perfil->id}}/{{$ongs[$j]->perfil->avatar->filename}}');">
                                        </div>
                                    </div>
                                    @endif
                                    <div class="card-header bg-transparent">
                                        <div class="form-group">
                                            <h2 class="text-center text-white">{{$ongs[$j]->name}}</h2>
                                            <a class="btn btn-primary" href="{{ route('ongs.show', ['id'=>$ongs[$j]->id, 'name'=>$ongs[$j]->name]) }}">Visualizar Ong</a>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php if($i%3==0) { ?> 
                            </div>
                            <div class="form-group row">
                            <?php } $i++; ?>
                        <?php } ?>
                    </div>
                    <div class="form-group row justify-content-center" style="margin-top: 20px">
                        <div class="form-group">{{ $ongs->links() }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

