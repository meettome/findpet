<div class="header {{ __($banner)}} py-7 py-lg-10">
    <div class="container">
        <div class="header-body text-center mt-7 mb-7">
            <div class="row justify-content-center">
                <div class="col-lg-5 col-md-6">
                    <h1 class="text-white" {{ $style ?? '' }}>{{ __($descricao) }}</h1>
                </div>
            </div>
        </div>
    </div>
</div>