<div class="header header-clip bg-gradient-primary py-9 py-lg-10">
    <div class="container">
        <div class="header-body text-center mb-5">
            <div class="row justify-content-center">
                <div class="col-lg-5 col-md-6">
                    <h1 class="text-muted text-uppercase">{{ __($descricao ?? '') }}</h1>
                </div>
            </div>
        </div>
    </div>
</div>