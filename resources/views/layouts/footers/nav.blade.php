<div class="row align-items-center justify-content-xl-between">
    <div class="col-xl-6">
        <div class="copyright text-center text-xl-left text-muted">
            &copy; {{ now()->year }} Find Your Pet
        </div>
    </div>
    <div class="col-xl-6">
        <ul class="nav nav-footer justify-content-center justify-content-xl-end">
            <li class="nav-item">
                <a href="{{ route('home') }}" class="nav-link" target="_self">Home</a>
            </li>
            <li class="nav-item">
                <a href="{{ route('about') }}" class="nav-link" target="_self">Sobre</a>
            </li>
            <li class="nav-item">
                <a href="{{ route('services') }}" class="nav-link" target="_self">Serviços</a>
            </li>
        </ul>
    </div>
</div>