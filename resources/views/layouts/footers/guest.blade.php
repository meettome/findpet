<footer class="py-5 bg-white">
    <div class="container">
        @include('layouts.footers.nav')
    </div>
</footer>