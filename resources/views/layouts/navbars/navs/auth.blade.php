<!-- Top navbar -->

<nav class="navbar navbar-top navbar-expand-md navbar-dark" id="navbar-main">
    <div class="container-fluid">
        <!-- Brand -->
        <a class="h4 mb-0 text-white text-uppercase d-none d-lg-inline-block" href="{{ route('home') }}">{{ __($title ?? '') }}</a>
        <!-- Form -->
        <form action="{{route('animal.pesquisa')}}" method="get" class="navbar-search navbar-search-dark form-inline mr-3 d-none d-md-flex ml-lg-auto">
            @csrf
            <div class="form-group mb-0">
                <div class="input-group input-group-alternative">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-search"></i></span>
                    </div>
                    <input class="form-control" name="parameter" placeholder="Buscar Animal" type="text">
                </div>
            </div>
        </form>

        <style type="text/css">

          .num {
              position: absolute;
              right: 12px;
              top: -6px;
              color: white;
              padding: 5px;
              font-size: 9px;
          }
      </style>
      <!-- Notify -->
      <ul class="navbar-nav align-items-center d-none d-md-flex">
        <li class="nav-item dropdown">
            <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <div class="media align-items-center">
                    <span class="rounded-circle">
                        <i class="ni ni-bell-55 text-white" style="font-size: 20px"></i>
                        <span class="num"><?php $i=0 ?>
                        @foreach(auth()->user()->notificacoes as $notificacoes)
                        @if($notificacoes->is_seen==0) <?php $i++; ?> @endif
                        @endforeach
                        {{$i}}
                    </span>
                </span>

            </div>
        </a>
        <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right" style="padding: 0; overflow-y: auto; max-height: 200px; min-height: 30px">

            @foreach(auth()->user()->notificacoes as $notificacoes)
            @if($notificacoes->is_seen==0) 
            <form action="{{route('conversa')}}" method="post" enctype="multipart/form-data">
                @csrf
                <?php 
                $contador = strlen($notificacoes->mensagem->body);
                $limite =10;
                if ( $contador >= $limite ) {   
                    $textobody = substr($notificacoes->mensagem->body, 0, strrpos(substr($notificacoes->mensagem->body, 0, $limite), ' ')) . '...';

                }else{
                    $textobody =$notificacoes->mensagem->body;
                }
                ?>
                <input type="hidden" name="id" value="{{$notificacoes->conversation_id}}">
                <button class="bg-blue text-white" style="width: 100%; cursor: pointer;">{{$notificacoes->messageable->name}} diz: {{$textobody}}</button>
            </form>
            @endif
            @endforeach
        </div>
    </li>
</ul>
<!-- User -->
<ul class="navbar-nav align-items-center d-none d-md-flex">
    <li class="nav-item dropdown">
        <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <div class="media align-items-center">
                <span class="avatar avatar-sm rounded-circle">
                    <img alt="Avatar" src="{{ auth()->user()->avatarImage }}" >
                </span>
                <div class="media-body ml-2 d-none d-lg-block">
                    <span class="mb-0 text-sm text-white  font-weight-bold">{{ auth()->user()->name }}</span>
                </div>
            </div>
        </a>
        <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
            <a href="{{ route('profile.edit') }}" class="dropdown-item">
                <i class="ni ni-single-02"></i>
                <span>{{ __('Meu perfil') }}</span>
            </a>
            <!--a href="#" class="dropdown-item">
            <i class="ni ni-settings-gear-65"></i>
            <span>{{ __('Configurações') }}</span>
            </a>
            <a href="#" class="dropdown-item">
            <i class="ni ni-calendar-grid-58"></i>
            <span>{{ __('Atividade') }}</span>
            </a>
            <a href="#" class="dropdown-item">
            <i class="ni ni-support-16"></i>
            <span>{{ __('Suporte') }}</span>
        </a-->
        <div class="dropdown-divider"></div>
        <a href="{{ route('logout') }}" class="dropdown-item" onclick="event.preventDefault();
        document.getElementById('logout-form').submit();">
        <i class="ni ni-user-run"></i>
        <span>{{ __('Sair') }}</span>
    </a>
</div>
</li>
</ul>
</div>
</nav>
