<nav class="navbar navbar-top navbar-horizontal navbar-expand-md navbar-dark {{$menu ?? ''}}">
    <div class="container px-4">
        <a class="navbar-brand" href="{{ route('site') }}">
            <img src="{{ asset('argon') }}/img/brand/logo.png" />
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-collapse-main" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbar-collapse-main">
            <!-- Collapse header -->
            <div class="navbar-collapse-header d-md-none">
                <div class="row">
                    <div class="col-9 collapse-brand" align="center">
                        <a href="{{ route('site') }}">
                            <img src="{{ asset('argon') }}/img/brand/logo.png">
                        </a>
                    </div>
                    <div class="col-2 collapse-close">
                        <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbar-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle sidenav" style="background: #ff000000;">
                            <span></span>
                            <span></span>
                        </button>
                    </div>
                </div>
            </div>
            <!-- Navbar items -->
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link nav-link-icon {{$itens ?? 'bg-blue-itens'}}" href="{{ route('site') }}">
                        <i class="ni ni-planet"></i>
                        <span class="nav-link-inner--text">{{ __('Home') }}</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link nav-link-icon {{$itens ?? 'bg-blue-itens'}}" href="{{ route('about') }}">
                        <i class="fas fa-icon fa-paw"></i>
                        <span class="nav-link-inner--text">{{ __('Sobre') }}</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link nav-link-icon {{$itens ?? 'bg-blue-itens'}}" href="{{ route('ongscadastradas') }}">
                        <i class="fas fa-icon fa-paw"></i>
                        <span class="nav-link-inner--text">{{ __('Ongs Cadastradas') }}</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link nav-link-icon {{$itens ?? 'bg-blue-itens'}}" href="{{ route('services') }}">
                        <i class="ni ni-send"></i>
                        <span class="nav-link-inner--text">{{ __('Serviços') }}</span>
                    </a>
                </li> 
                @auth()
                <li class="nav-item">
                    <a class="nav-link nav-link-icon {{$itens ?? 'bg-blue-itens'}}" href="{{ route('home') }}">
                        <i class="ni ni-circle-08"></i>
                        <span class="nav-link-inner--text">{{ __('Dashboard') }}</span>
                    </a>
                </li>
                @endauth

                @guest()
                <li class="nav-item">
                    <a class="nav-link nav-link-icon {{$itens ?? 'bg-blue-itens'}}" href="{{ route('register') }}">
                        <i class="ni ni-circle-08"></i>
                        <span class="nav-link-inner--text">{{ __('Cadastro') }}</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link nav-link-icon {{$itens ?? 'bg-blue-itens'}}" href="{{ route('login') }}">
                        <i class="ni ni-key-25"></i>
                        <span class="nav-link-inner--text">{{ __('Login') }}</span>
                    </a>
                </li>
                @endguest
                <li class="nav-item">
                    <a class="nav-link nav-link-icon {{$itens ?? 'bg-blue-itens'}}" href="{{ route('contact') }}">
                        <i class="fas fa-icon fa-phone"></i>
                        <span class="nav-link-inner--text">{{ __('Fale Conosco') }}</span>
                    </a>
                </li>

                @auth()
                <li class="nav-item">
                    <a href="{{ route('logout') }}" class="nav-link nav-link-icon {{$itens ?? 'bg-blue-itens'}}" onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                    <i class="ni ni-user-run"></i>
                    <span class="nav-link-inner--text">{{ __('Sair') }}</span>
                </a>
            </li>
            @endauth
            
        </ul>
    </div>
</div>
</nav>