<nav class="navbar navbar-vertical fixed-left navbar-expand-md navbar-light bg-gradient-primary" id="sidenav-main">
    <div class="container-fluid">
        <!-- Toggler -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <!-- Brand -->
        <a class="navbar-brand pt-0 pb-0 d-sm-none mt-3" href="{{ route('home') }}">
            <img src="{{ asset('argon') }}/img/brand/blue.png" class="navbar-brand-img" alt="...">
        </a>
        <!-- User -->
        <ul class="nav align-items-center d-md-none">
            <li class="nav-item dropdown">
                <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <div class="media align-items-center">
                        <span class="avatar avatar-sm rounded-circle">
                            <img alt="Image placeholder" src="{{ auth()->user()->avatarImage }}">
                        </span>
                    </div>
                </a>
                <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
                    <div class=" dropdown-header noti-title">
                        <h6 class="text-overflow m-0">{{ __('Bem Vindo!') }}</h6>
                    </div>
                    <a href="{{ route('profile.edit') }}" class="dropdown-item">
                        <i class="ni ni-single-02"></i>
                        <span>{{ __('Perfil') }}</span>
                    </a>
                    <!--a href="#" class="dropdown-item">
                        <i class="ni ni-settings-gear-65"></i>
                        <span>{{ __('Settings') }}</span>
                    </a>
                    <a href="#" class="dropdown-item">
                        <i class="ni ni-calendar-grid-58"></i>
                        <span>{{ __('Activity') }}</span>
                    </a>
                    <a href="#" class="dropdown-item">
                        <i class="ni ni-support-16"></i>
                        <span>{{ __('Support') }}</span>
                    </a-->
                    <div class="dropdown-divider"></div>
                    <a href="{{ route('logout') }}" class="dropdown-item" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        <i class="ni ni-user-run"></i>
                        <span>{{ __('Sair') }}</span>
                    </a>
                </div>
            </li>
        </ul>
        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="sidenav-collapse-main">
            <!-- Collapse header -->
            <div class="navbar-collapse-header d-md-none">
                <div class="row">
                    <div class="col-6 collapse-brand">
                        <a href="{{ route('home') }}">
                            <img src="{{ asset('argon') }}/img/brand/blue.png"  style="height: 34px!important">
                        </a>
                    </div>
                    <div class="col-6 collapse-close">
                        <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle sidenav">
                            <span></span>
                            <span></span>
                        </button>
                    </div>
                </div>
            </div>
            <!-- Form -->
            <form class="mt-4 mb-3 d-md-none" action="{{route('animal.pesquisa')}}" method="get" >
                <div class="input-group input-group-rounded input-group-merge">
                    <input type="search" class="form-control form-control-rounded form-control-prepended"  name="parameter" placeholder="Buscar Animal" aria-label="Search">
                    <div class="input-group-prepend">
                        <div class="input-group-text">
                            <span class="fa fa-search"></span>
                        </div>
                    </div>
                </div>
            </form>
            <!-- Navigation -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('home') }}">
                        <i class="ni ni-tv-2 text-primary"></i> {{ __('Dashboard') }}
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link {{ request()->is('sistema/animal', 'sistema/animal/adicionar') ? 'collapsed active' : '' }}" href="#animals" data-toggle="collapse" role="button" aria-expanded="{{ request()->is('sistema/animal', 'sistema/animal/adicionar') ? 'true' : 'false' }}" aria-controls="animals">
                        <i class="fas fa-icon fa-paw text-primary"></i>
                        <span class="nav-link-text">{{ __('Animais') }}</span>
                    </a>

                    <div class="collapse  {{ request()->is('sistema/animal', 'sistema/animal/adicionar') ? 'show active' : '' }}" id="animals">
                        <ul class="nav nav-sm flex-column">
                            <li class="nav-item  {{ request()->is('sistema/animal') ? 'active' : '' }}">
                                <a class="nav-link" href="{{ route('animal.index') }}">
                                {{ __(' Todos os Animais') }}</a>
                            </li>
                            <li class="nav-item  {{ request()->is('sistema/animal/adicionar') ? 'active' : '' }}">
                                <a class="nav-link" href="{{ route('animal.create') }}">
                                {{ __(' Novo Animal') }}</a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ request()->is('sistema/adocao', 'sistema/adocao/cachorros', 'sistema/adocao/furoes',  'sistema/adocao/gatos', 'sistema/adocao/roedores', 'sistema/adocao/coelhos', 'sistema/adocao/aves', 'sistema/adocao/tartarugas-jabutis-e-cagados') ? 'collapsed active' : '' }}" href="#adocao" data-toggle="collapse" role="button" aria-expanded="{{ request()->is('sistema/adocao', 'sistema/adocao/cachorros', 'sistema/adocao/furoes', 'sistema/adocao/gatos', 'sistema/adocao/roedores', 'sistema/adocao/coelhos', 'sistema/adocao/aves', 'sistema/adocao/tartarugas-jabutis-e-cagados') ? 'true' : 'false' }}" aria-controls="adocao">
                        <i class="fas fa-icon fa-paw text-primary"></i>
                        <span class="nav-link-text">{{ __('Adoção') }}</span>
                    </a>

                    <div class="collapse  {{ request()->is('sistema/adocao', 'sistema/adocao/cachorros', 'sistema/adocao/furoes', 'sistema/adocao/gatos', 'sistema/adocao/roedores', 'sistema/adocao/coelhos', 'sistema/adocao/aves', 'sistema/adocao/tartarugas-jabutis-e-cagados') ? 'show active' : '' }}" id="adocao">
                        <ul class="nav nav-sm flex-column">
                            <li class="nav-item  {{ request()->is('sistema/adocao') ? 'active' : '' }}">
                                <a class="nav-link" href="{{ route('adocao') }}">
                                {{ __(' Todos') }}</a>
                            </li>
                            <li class="nav-item  {{ request()->is('sistema/adocao/cachorros') ? 'active' : '' }}">
                                <a class="nav-link" href="{{ route('adocao.dog') }}">
                                {{ __(' Cachorros') }}</a>
                            </li>
                            <li class="nav-item  {{ request()->is('sistema/adocao/furoes') ? 'active' : '' }}">
                                <a class="nav-link" href="{{ route('adocao.furao') }}">
                                {{ __(' Furões') }}</a>
                            </li>
                            <li class="nav-item  {{ request()->is('sistema/adocao/gatos') ? 'active' : '' }}">
                                <a class="nav-link" href="{{ route('adocao.cat') }}">
                                {{ __(' Gatos') }}</a>
                            </li>
                            <li class="nav-item  {{ request()->is('sistema/adocao/roedores') ? 'active' : '' }}">
                                <a class="nav-link" href="{{ route('adocao.rodents') }}">
                                {{ __(' Roedores') }}</a>
                            </li>
                            <li class="nav-item  {{ request()->is('sistema/adocao/coelhos') ? 'active' : '' }}">
                                <a class="nav-link" href="{{ route('adocao.rabbits') }}">
                                {{ __(' Coelhos') }}</a>
                            </li>
                            <li class="nav-item  {{ request()->is('sistema/adocao/aves') ? 'active' : '' }}">
                                <a class="nav-link" href="{{ route('adocao.birds') }}">
                                {{ __(' Aves') }}</a>
                            </li>
                            <li class="nav-item  {{ request()->is('sistema/adocao/tartarugas-jabutis-e-cagados') ? 'active' : '' }}">
                                <a class="nav-link" href="{{ route('adocao.turtles') }}">
                                {{ __(' Tartaruga, Jabutis e Cágados') }}</a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ request()->is('sistema/perdidos', 'sistema/perdidos/cachorros', 'sistema/perdidos/furoes', 'sistema/perdidos/gatos', 'sistema/perdidos/roedores', 'sistema/perdidos/adocao/coelhos', 'sistema/perdidos/aves', 'sistema/perdidos/tartarugas-jabutis-e-cagados') ? 'collapsed active' : '' }}" href="#perdidos" data-toggle="collapse" role="button" aria-expanded="{{ request()->is('sistema/perdidos', 'sistema/perdidos/cachorros', 'sistema/perdidos/furoes', 'sistema/perdidos/gatos', 'sistema/perdidos/roedores', 'sistema/perdidos/coelhos', 'sistema/perdidos/aves', 'sistema/perdidos/tartarugas-jabutis-e-cagados') ? 'true' : 'false' }}" aria-controls="perdidos">
                        <i class="fas fa-icon fa-paw text-primary"></i>
                        <span class="nav-link-text">{{ __('Perdidos') }}</span>
                    </a>

                    <div class="collapse  {{ request()->is('sistema/perdidos', 'sistema/perdidos/cachorros', 'sistema/perdidos/furoes', 'sistema/perdidos/gatos', 'sistema/perdidos/roedores', 'sistema/perdidos/coelhos', 'sistema/perdidos/aves', 'sistema/perdidos/tartarugas-jabutis-e-cagados') ? 'show active' : '' }}" id="perdidos">
                        <ul class="nav nav-sm flex-column">
                            <li class="nav-item  {{ request()->is('sistema/perdidos') ? 'active' : '' }}">
                                <a class="nav-link" href="{{ route('perdidos') }}">
                                {{ __(' Todos') }}</a>
                            </li>
                            <li class="nav-item  {{ request()->is('sistema/perdidos/cachorros') ? 'active' : '' }}">
                                <a class="nav-link" href="{{ route('perdidos.dog') }}">
                                {{ __(' Cachorros') }}</a>
                            </li>
                            <li class="nav-item  {{ request()->is('sistema/perdidos/furoes') ? 'active' : '' }}">
                                <a class="nav-link" href="{{ route('perdidos.furao') }}">
                                {{ __(' Furões') }}</a>
                            </li>
                            <li class="nav-item  {{ request()->is('sistema/perdidos/gatos') ? 'active' : '' }}">
                                <a class="nav-link" href="{{ route('perdidos.cat') }}">
                                {{ __(' Gatos') }}</a>
                            </li>
                            <li class="nav-item  {{ request()->is('sistema/perdidos/roedores') ? 'active' : '' }}">
                                <a class="nav-link" href="{{ route('perdidos.rodents') }}">
                                {{ __(' Roedores') }}</a>
                            </li>
                            <li class="nav-item  {{ request()->is('sistema/perdidos/coelhos') ? 'active' : '' }}">
                                <a class="nav-link" href="{{ route('perdidos.rabbits') }}">
                                {{ __(' Coelhos') }}</a>
                            </li>
                            <li class="nav-item  {{ request()->is('sistema/perdidos/aves') ? 'active' : '' }}">
                                <a class="nav-link" href="{{ route('perdidos.birds') }}">
                                {{ __(' Aves') }}</a>
                            </li>
                            <li class="nav-item  {{ request()->is('sistema/perdidos/tartarugas-jabutis-e-cagados') ? 'active' : '' }}">
                                <a class="nav-link" href="{{ route('perdidos.turtles') }}">
                                {{ __(' Tartaruga, Jabutis e Cágados') }}</a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('profile.edit')}}">
                        <i class="ni ni-circle-08 text-blue"></i> {{ __('Perfil') }}
                    </a>
                </li>
                @if(auth()->user()->role=="ong" || auth()->user()->role == "vet")

                <li class="nav-item">
                    <a class="nav-link {{ request()->is('sistema/empregado', 'sistema/empregado/adicionar') ? 'collapsed active' : '' }}" href="#empregado" data-toggle="collapse" role="button" aria-expanded="{{ request()->is('sistema/empregado', 'sistema/empregado/adicionar') ? 'true' : 'false' }}" aria-controls="property">
                        <i class="fas fa-icon fa-users text-primary"></i>
                        <span class="nav-link-text">{{ __('Empregados') }}</span>
                    </a>

                    <div class="collapse  {{ request()->is('sistema/empregado', 'sistema/empregado/adicionar') ? 'show active' : '' }}" id="empregado">
                        <ul class="nav nav-sm flex-column">
                            <li class="nav-item  {{ request()->is('sistema/empregado') ? 'active' : '' }}">
                                <a class="nav-link" href="{{ route('empregado.index') }}">
                                {{ __(' Todos os Empregados') }}</a>
                            </li>
                            <li class="nav-item  {{ request()->is('sistema/empregado/adicionar') ? 'active' : '' }}">
                                <a class="nav-link" href="{{ route('empregado.create') }}">
                                {{ __(' Novo Empregado') }}</a>
                            </li>
                        </ul>
                    </div>
                </li>
                @endif
                @if(auth()->user()->role=="admin")

                <li class="nav-item">
                    <a class="nav-link {{ request()->is('sistema/usuario', 'sistema/usuario/create') ? 'collapsed active' : '' }}" href="#usuario" data-toggle="collapse" role="button" aria-expanded="{{ request()->is('sistema/usuario', 'sistema/usuario/create') ? 'true' : 'false' }}" aria-controls="usuario">
                        <i class="fas fa-icon fa-users text-primary"></i>
                        <span class="nav-link-text">{{ __('Usuarios') }}</span>
                    </a>

                    <div class="collapse  {{ request()->is('sistema/usuario', 'sistema/usuario/create') ? 'show active' : '' }}" id="usuario">
                        <ul class="nav nav-sm flex-column">
                            <li class="nav-item  {{ request()->is('sistema/usuario') ? 'active' : '' }}">
                                <a class="nav-link" href="{{ route('usuario.index') }}">
                                {{ __(' Todos os Usuarios') }}</a>
                            </li>
                            <li class="nav-item  {{ request()->is('sistema/usuario/create') ? 'active' : '' }}">
                                <a class="nav-link" href="{{ route('usuario.create') }}">
                                {{ __(' Novo Novo') }}</a>
                            </li>
                        </ul>
                    </div>
                </li>
                @endif
            </ul>
        </div>
    </div>
</nav>