<tr>
<td class="header a">
<a href="http://findyourpet.com.br" style="display: inline-block;">
@if (trim($slot) === 'FindYourPet')
<img src="https://yac.com.br/wp-content/uploads/2021/02/logobranca.png" class="logo" style="width: 100px!important; height:100px!important" alt="Find Your Pet Logo">
@else
{{ $slot }}
@endif
</a>
</td>
</tr>
