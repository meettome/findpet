@component('mail::message')
<h1>Olá {{ $user->name }},</h1>

<p>Seja bem-vindo ao {{ config('app.name') }}, seu cadastro foi feito com sucesso!</p>

<p>Para entrar em seu usuario, clique no botão a seguir</p>

@component('mail::button', [
	'url' => route('login'),
	'color' => 'red',
])
Entrar no meu usuario
@endcomponent

Obrigado, {{ config('app.name') }}<br>

@endcomponent
