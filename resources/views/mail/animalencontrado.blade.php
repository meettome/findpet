@component('mail::message')
<h1>Olá {{ $user->name }},</h1>

<p>O usuario {{ $usuario->name }} de email {{ $usuario->email }}, viu seu pet no seguinte endereço.</p>

<p>Logradouro: {{$endereco->street}}</p>
<p>Bairro: {{$endereco->district}}</p>
<p>Numero: {{$endereco->number}}</p>
<p>Cidade: {{$endereco->city->name}}</p>
<p>Estado: {{$endereco->state->name}}</p>
<p>Cep: {{$endereco->zip_code}}</p>

Obrigado, {{ config('app.name') }}<br>

@endcomponent
