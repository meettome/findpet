@component('mail::message')
<p>Recebemos um pedido para recuperação de senha para este email.</p>

@component('mail::button', [
	'url' => route('password.reset', $token).'?email='.$user->email,
	'color' => 'red',
])
Recuperar senha
@endcomponent

<p>Para recuperar seu acesso, basta clicar no botão acima. Você tem até 24hs para fazer a alteração. Caso você não tenha feito o pedido para recuperar a senha, basta ignorar este email.</p>

Obrigado,<br>
{{ config('app.name') }}
@endcomponent
