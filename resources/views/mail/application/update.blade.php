@component('mail::message')
<h1>Olá {{ $application->user->name }},</h1>

@if($application->status=='accepted')
<h3>Parabéns!</h3>
<p>Sua candidatura para a vaga de {{ $application->area->name }} em nossa loja localizada no endereço
{{ $application->opportunity->pharmacy->address }} em {{ $application->opportunity->pharmacy->city->name }}, {{ $application->opportunity->pharmacy->city->state->uf }}
foi <b>{{ __('ui.models.application.status_values.'.$application->status) }}</b>.
</p>
<p>Você irá receber um email em breve com mais informações de como continuar seu processo de seleção.<p>
<p>Nos veremos em breve.</p>
@else
<h3>Sentimos muito...</h3>
<p>Mas sua candidatura para a vaga de {{ $application->area->name }} em nossa loja localizada no endereço
{{ $application->opportunity->pharmacy->address }} em {{ $application->opportunity->pharmacy->city->name }}, {{ $application->opportunity->pharmacy->city->state->uf }}
foi <b>{{ __('ui.models.application.status_values.'.$application->status) }}</b>.
</p>
<p>No entanto, seu currículo irá continuar em nosso sistema.</p>
<p>E se em seu cadastro, você ativou as notificações de novas vagas, você irá receber automaticamente mensagens quando estas vagas se fizerem disponíveis.</p>
@endif

Obrigado,<br>
{{ config('app.name') }}
@endcomponent
