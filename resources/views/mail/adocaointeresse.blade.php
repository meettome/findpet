@component('mail::message')
<h1>Olá {{ $user->name }},</h1>

<p>O usuario {{ $usuario->name }} de email {{ $usuario->email }}, ficou interessado pelo seu pet {{$animal->name}}, por favor, entre em contato o mais breve possivel.</p>

@component('mail::button', [
	'url' => route('contrato', ['id'=>$animal->id, 'name'=>$animal->name]),
	'color' => 'red',
])
Baixar Contrato
@endcomponent

Obrigado, {{ config('app.name') }}<br>

@endcomponent
