@component('mail::message')
<h1>Olá {{ $user->name }},</h1>
<h3>Ótimas notícias para você!</h3>
<p>Recentemente em nosso sistema, @choice('{1}uma nova vaga|[2,*]novas vagas', $opportunity->amount) para a área de {{ $opportunity->area->name }} no setor {{ $opportunity->sector->name }} @choice('{1}foi disponibilizada|[2,*]foram disponibilizadas',$opportunity->amount), e acreditamos que @choice('{1}seja|[2,*]sejam', $opportunity->amount) de seu interesse...</p>
<p>Visite nosso sistema agora mesmo para se candidatar, ou para obter mais informações sobre @choice('{1}esta oportunidade|[2,*]estas oportunidades', $opportunity->amount).</p>

@component('mail::button', [
	'url' => config('app.url'),
	'color'=> 'red',
])
Visitar o sistema de Currículos
@endcomponent

<p>Caso não deseje mais receber nossos emails de notificação de novas vagas, é só entrar em nosso sistema e desabilitar o envio em seu perfil.</p>

Atenciosamente,<br>
{{ config('app.name') }}
@endcomponent
