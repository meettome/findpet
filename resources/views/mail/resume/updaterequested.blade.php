@component('mail::message')
<h1>Olá {{ $user->first_name }},</h1>

<p>Demos uma olhada em seu currículo, mas aparentemente já faz algum tempo... Será que você poderia nos enviar uma atualização?</p>
<p>Temos certeza de que você vai achar super fácil! É só clicar no botão abaixo para visitar nosso sistema, efetuar o login e nos enviar um novo arquivo.</p>

@component('mail::button', [
	'url' => config('app.url'),
	'color'=> 'red',
])
Visitar o sistema de Currículos
@endcomponent

Aguardamos seu currículo!<br>
{{ config('app.name') }}
@endcomponent
