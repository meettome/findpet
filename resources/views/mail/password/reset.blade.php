@component('mail::message')
<h1>Olá {{ $user->name }},</h1>

<p>Esquecer a senha é sempre chato. Mas, felizmente você pode facilmente recuperar seu acesso em nosso sistema utilizando o link abaixo:</p>

@component('mail::button', [
	'url' => $resetPassUrl,
	'color' => 'red'
])
Recuperar senha
@endcomponent

Obrigado,<br>
{{ config('app.name') }}
@endcomponent
