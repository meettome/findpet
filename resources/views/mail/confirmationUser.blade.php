@component('mail::message')
<h1>Olá {{ $user->name }},</h1>

<p>Você foi adicionado como funcionario de {{ $clinic }}</p>

<p>Por favor, clique no botão abaixo para confirmar seu endereço de email:</p>

@component('mail::button', [
	'url' => route('public.confirm').'/'.$user->email,
	'color' => 'red',
])
Confirmar email
@endcomponent

Obrigado,<br>
{{ config('app.name') }}
@endcomponent
