@component('mail::message')
<p>Recebemos um pedido para recuperação de senha para este email.</p>

@component('mail::button', [
	'url' => route('password.reset', $token).'?email='.$email,
	'color' => 'red',
])
Recuperar senha
@endcomponent

<p>Para recuperar seu acesso, basta clicar no botão acima. Caso você não tenha feito o pedido para recuperar a senha, basta ignorar este email.</p>

Obrigado,<br>
{{ config('app.name') }}
@endcomponent
