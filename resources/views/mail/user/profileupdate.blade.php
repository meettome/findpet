@component('mail::message')
<h1>Olá {{ $user->name }},</h1>

<p>Demos uma olhada em seu perfil, mas ele está incompleto. Será que você poderia nos dar mais informações sobre você?</p>
<p>Temos certeza de que você vai achar super fácil! É só clicar no botão abaixo para visitar nosso sistema.</p>

@component('mail::button', [
	'url' => config('app.url'),
	'color'=> 'red',
])
Visitar o sistema de Currículos
@endcomponent

Aguardamos sua atualização!<br>
{{ config('app.name') }}
@endcomponent
