  @extends('layouts.app', ['class' => 'bg-default', 'menu' =>'bg-transparent', 'itens' => 'text-white', 'title' => 'Cadastre-se'])

  @section('content')
  @include('layouts.headers.guest', ['descricao' => 'Registre seus dados'])


  <div class="mt--8 pb-5">
    <!-- Table -->
    <div class="row justify-content-center">
        <div class="col-lg-9 col-md-8">
            <div class="card bg-secondary shadow border-0">
                <div class="card-body px-lg-5 py-lg-6">
                    <form role="form" method="POST" action="{{ route('RegisterVet') }}" enctype="multipart/form-data"  autocomplete="off">
                        @csrf

                        <div class="form-group col mb-3{{ $errors->has('name') ? ' has-danger' : '' }}">
                            <div class="input-group input-group-alternative mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="ni ni-hat-3"></i></span>
                                </div>
                                <input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" placeholder="{{ __('Nome completo') }}" type="text" name="name" value="{{ old('name') }}" required autofocus>
                            </div>
                            @if ($errors->has('name'))
                            <span class="invalid-feedback" style="display: block;" role="alert">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group mb-0 row">
                            <div class="col-md-6 {{ $errors->has('email') ? ' has-danger' : '' }}">
                                <div class="input-group input-group-alternative mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                                    </div>
                                    <input class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="{{ __('Email') }}" type="email" name="email" value="{{ old('email') }}" required>
                                </div>
                                @if ($errors->has('email'))
                                <span class="invalid-feedback" style="display: block;" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </div> 
                            <div class="col-md-6 {{ $errors->has('cnpj') ? ' has-danger' : '' }}">
                                <div class="input-group input-group-alternative mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="ni ni-satisfied"></i>
                                        </span>
                                    </div>
                                    <input class="form-control {{ $errors->has('cnpj') ? ' is-invalid' : '' }}" placeholder="{{ __('CNPJ') }}" type="text" name="cnpj" value="{{ old('cnpj') }}" id="cnpj" required>
                                </div>
                                @if ($errors->has('cnpj'))
                                <span class="invalid-feedback" style="display: block;" role="alert">
                                    <strong>{{ $errors->first('cnpj') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group col mb-0{{ $errors->has('site') ? ' has-danger' : '' }}">
                            <div class="input-group input-group-alternative mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="ni ni-planet"></i></span>
                                </div>
                                <input class="form-control{{ $errors->has('site') ? ' is-invalid' : '' }}" placeholder="{{ __('Site') }}" type="site" name="site" value="{{ old('site') }}">
                            </div>
                            @if ($errors->has('site'))
                            <span class="invalid-feedback" style="display: block;" role="alert">
                                <strong>{{ $errors->first('site') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 {{ $errors->has('telphone') ? ' has-danger' : '' }}">
                                <div class="input-group input-group-alternative mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fas fa-icon fa-phone"></i></span>
                                    </div>
                                    <input class="form-control telefone{{ $errors->has('telphone') ? ' is-invalid' : '' }}" placeholder="{{ __('Telefone') }}" type="text" name="telphone" value="{{ old('telphone') }}" >
                                </div>
                                @if ($errors->has('telphone'))
                                <span class="invalid-feedback" style="display: block;" role="alert">
                                    <strong>{{ $errors->first('telphone') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="col-md-6 {{ $errors->has('cellphone') ? ' has-danger' : '' }}">
                                <div class="input-group input-group-alternative mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fas fa-icon fa-phone"></i></span>
                                    </div>
                                    <input class="form-control celular{{ $errors->has('cellphone') ? ' is-invalid' : '' }}" placeholder="{{ __('Celular') }}" type="text" name="cellphone" value="{{ old('cellphone') }}" required>
                                </div>
                                @if ($errors->has('cellphone'))
                                <span class="invalid-feedback" style="display: block;" role="alert">
                                    <strong>{{ $errors->first('cellphone') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 {{ $errors->has('birth_date') ? ' has-danger' : '' }}">
                                <div class="input-group input-group-alternative mb-1">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="fas fa-icon fa-calendar"></i>
                                        </span>
                                    </div>
                                    <input class="form-control{{ $errors->has('birth_date') ? ' is-invalid' : '' }}" placeholder="{{ __('Data de Nascimento') }}" type="date" name="birth_date" value="{{ old('birth_date') }}" required>
                                </div>
                                <div class="mb-2">
                                    <small class="text-white text-uppercase">{{ 'Data de Inicio' }}</small>
                                </div>
                                @if ($errors->has('birth_date'))
                                <span class="invalid-feedback" style="display: block;" role="alert">
                                    <strong>{{ $errors->first('birth_date') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="col-md-6 {{ $errors->has('crmv') ? ' has-danger' : '' }}">
                             <div class="input-group input-group-alternative mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="fas fa-address-book"></i>
                                    </span>
                                </div>
                                <input class="form-control {{ $errors->has('crmv') ? ' is-invalid' : '' }}" placeholder="{{ __('Numero do registro CRMV') }}" type="text" name="crmv" value="{{ old('crmv') }}" required>
                            </div>
                            @if ($errors->has('crmv'))
                            <span class="invalid-feedback" style="display: block;" role="alert">
                                <strong>{{ $errors->first('crmv') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>



                    <div class="form-group row mb-0">
                        <div class="col-md-6 {{ $errors->has('zip_code') ? ' has-danger' : '' }}">
                            <div class="input-group input-group-alternative  mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="ni ni-map-big"></i></span>
                                </div>
                                <input class="form-control{{ $errors->has('zip_code') ? ' is-invalid' : '' }}" placeholder="{{ __('CEP') }}" type="text" name="zip_code" id="cep" value="{{ old('zip_code') }}" required >
                            </div>
                            @if ($errors->has('zip_code'))
                            <span class="invalid-feedback" style="display: block;" role="alert">
                                <strong>{{ $errors->first('zip_code') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="col-md-6{{ $errors->has('district') ? ' has-danger' : '' }}">
                            <div class="input-group input-group-alternative mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="ni ni-pin-3"></i></span>
                                </div>
                                <input class="form-control{{ $errors->has('district') ? ' is-invalid' : '' }}" placeholder="{{ __('Bairro') }}" type="text" name="district" id="bairro" value="{{ old('district') }}" required>
                            </div>
                            @if ($errors->has('district'))
                            <span class="invalid-feedback" style="display: block;" role="alert">
                                <strong>{{ $errors->first('district') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row mb-0">
                        <div class="col-md-9{{ $errors->has('street') ? ' has-danger' : '' }}">
                            <div class="input-group input-group-alternative mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="ni ni-pin-3"></i></span>
                                </div>
                                <input class="form-control{{ $errors->has('street') ? ' is-invalid' : '' }}" placeholder="{{ __('Logradouro') }}" type="text" name="street" id="logradouro" value="{{ old('street') }}" required>
                            </div>
                            @if ($errors->has('street'))
                            <span class="invalid-feedback" style="display: block;" role="alert">
                                <strong>{{ $errors->first('street') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="col-md-3{{ $errors->has('number') ? ' has-danger' : '' }}">
                            <div class="input-group input-group-alternative mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="ni ni-pin-3"></i></span>
                                </div>
                                <input class="form-control{{ $errors->has('number') ? ' is-invalid' : '' }}" placeholder="{{ __('Nº') }}" value="{{ old('number') }}" type="text" name="number" id="number">
                            </div>
                            @if ($errors->has('number'))
                            <span class="invalid-feedback" style="display: block;" role="alert">
                                <strong>{{ $errors->first('number') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group col mb-3{{ $errors->has('complement') ? ' has-danger' : '' }}">
                        <div class="input-group input-group-alternative">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="ni ni-pin-3"></i></span>
                            </div>
                            <input class="form-control{{ $errors->has('complement') ? ' is-invalid' : '' }}" placeholder="{{ __('Complemento') }}" type="text" name="complement" id="complement" value="{{ old('complement') }}">
                        </div>
                        @if ($errors->has('complement'))
                        <span class="invalid-feedback" style="display: block;" role="alert">
                            <strong>{{ $errors->first('complement') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group row mb-0">
                        <div class="col-md-6{{ $errors->has('state_id') ? ' has-danger' : '' }}">
                            <div class="input-group input-group-alternative mb-3">
                                <select id="filterState" name="state_id" class="dynamic form-control" data-endpoint="api/states" data-labelfield="name" data-valuefield="id" data-current="{{ $state_id ?? old('state_id') }}" data-targetid="filterCity" data-updateevent="updateCitySelect">
                                    <option value="">Selecione um estado</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6{{ $errors->has('city_id') ? ' has-danger' : '' }}">
                            <div class="input-group input-group-alternative mb-3">
                                @if(isset($state_id))
                                <select id="filterCity" name="city_id" class="dynamic form-control" data-endpoint="{{ $state_id?"api/cities/{$state_id}":"" }}" data-labelfield="name" data-valuefield="id" data-current="{{ $city_id ?? old('city_id')  }}">
                                    @else
                                    <select id="filterCity" name="city_id" class="dynamic form-control" data-labelfield="name" data-valuefield="id" data-current="{{ $city_id ?? old('city_id') }}">
                                        @endif
                                        <option value="">Selecione uma cidade</option>
                                    </select>
                                </select>
                            </div>
                        </div>

                    </div>

                    <div class="form-group col mb-3{{ $errors->has('photo') ? ' has-danger' : '' }}">
                        <div class="input-group input-group-alternative">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="ni ni-badge"></i></span>
                            </div>
                            <input class="form-control{{ $errors->has('photo') ? ' is-invalid' : '' }}" type="file" name="photo" required>
                        </div>
                        @if ($errors->has('photo'))
                        <span class="invalid-feedback" style="display: block;" role="alert">
                            <strong>{{ $errors->first('photo') }}</strong>
                        </span>
                        @endif
                    </div>
                    <hr>
                    <div class="text-white text-center mt-2 mb-4">{{ __('Redes Sociais') }}</div>
                    <div class="form-group row mb-0" id="origem">

                        <div class="col-md-5">
                            <div class="input-group input-group-alternative mb-3">
                                <select id="filterType" name="type_id[]" class="dynamic form-control" data-endpoint="api/typesnetworks" data-labelfield="type" data-valuefield="id" data-targetid="filterType">
                                    <option value="">Selecione uma Rede Social</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 usuario">
                            <div class="input-group input-group-alternative mb-3">
                                <input class="form-control" type="text" placeholder="Digite o Usuario" name="user[]">
                            </div>
                        </div>
                        <div class="col-md-1">
                            <i class="fas fa-plus-circle adicionar" style="cursor: pointer;" onclick="duplicarCampos();" title="Adicionar"></i>
                            <i class="fas fa-minus-circle remover" title="Remover" style="cursor: pointer;" onclick="removerCampos(this);"></i>
                        </div>
                    </div>
                    <div id="destino">
                    </div>
                    <hr>
                    <div class="form-group col mb-3{{ $errors->has('password') ? ' has-danger' : '' }}">
                        <div class="input-group input-group-alternative">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                            </div>
                            <input class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="{{ __('Senha') }}" type="password" name="password" required>
                        </div>
                        @if ($errors->has('password'))
                        <span class="invalid-feedback" style="display: block;" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group col mb-3">
                        <div class="input-group input-group-alternative">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                            </div>
                            <input class="form-control" placeholder="{{ __('Confirme a senha') }}" type="password" name="password_confirmation" required>
                        </div>
                    </div>
                                <!--div class="text-muted font-italic">
                                    <small>{{ __('password strength') }}: <span class="text-success font-weight-700">{{ __('strong') }}strong</span></small>
                                </div-->
                                <div class="row my-4">
                                    <div class="col-12">
                                        <div class="custom-control custom-control-alternative custom-checkbox">
                                            <input class="custom-control-input" id="customCheckRegister" type="checkbox" required>
                                            <label class="custom-control-label" for="customCheckRegister">
                                                <span class="text-muted">{{ __('Eu concordo com a') }} <a href="#!">{{ __('Política de Privacidade') }}</a></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="text-center">
                                    <button type="submit" class="btn btn-primary mt-4">{{ __('Criar Usuario') }}</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @push('js')
        
        <script src="/js/poly.js" type="text/javascript" charset="utf-8"></script>
        <script src="/js/vendor.js" type="text/javascript" charset="utf-8"></script>
        <script src="/js/main.js" type="text/javascript" charset="utf-8"></script>
        <script src="/js/jquery.mask.js" type="text/javascript" charset="utf-8"></script>
        <script src="/js/buscacep.js" type="text/javascript" charset="utf-8"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                $('.celular').mask('(99) 9 9999-9999');
                $('.telefone').mask('(99) 9999-9999');
                $('#cpf').mask('999.999.999-99');
                $('#cnpj').mask('99.999.999/9999-99');
                $('#cep').mask('99999-999');
            });

            function duplicarCampos(){
                var clone = document.getElementById('origem').cloneNode(true);
                var destino = document.getElementById('destino');
                destino.appendChild (clone);

                var camposClonados = clone.getElementsByTagName('input');

                for(i=0; i<camposClonados.length;i++){
                    camposClonados[i].value = '';
                }                
            }

            function removerCampos(id){
                var node1 = document.getElementById('destino');
                node1.removeChild(node1.childNodes[0]);
            }

        </script>
        @endpush
        @endsection
