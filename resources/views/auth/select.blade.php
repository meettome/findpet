    @extends('layouts.app', ['class' => 'bg-default', 'menu' =>'bg-transparent', 'itens' => 'text-white', 'title' => 'Selecione um perfil'])


    @section('content')
    @include('layouts.headers.guest', ['descricao' => 'Selecione um perfil'])

    <div class="container mt--8 pb-5">
        <!-- Table -->
        <div class="row justify-content-center">
            <div class="col-lg-6 col-md-8">
                <div class="card bg-secondary shadow border-0">
                    <div class="card-header bg-transparent pb-5">
                        <div class="text-white text-center mt-2 mb-4 text-uppercase">{{ __('Cadastre-se como') }}</div>
                        <div class="text-center form-group">
                            <a href="{{ route('vet') }}" class="btn btn-neutral btn-icon col-sm-12 text-uppercase">
                                <span class="btn-inner--icon"><i class="fab fa-reddit"></i></span>
                                <span class="btn-inner--text">{{ __('Clinica Veterinaria') }}</span>
                            </a>
                        </div>
                        <div class="text-center form-group">
                            <a href="{{ route('ongs') }}" class="btn btn-neutral btn-icon col-sm-12 text-uppercase">
                                <span class="btn-inner--icon"><i class="fas fa-paw"></i></span>
                                <span class="btn-inner--text">{{ __('ONGs') }}</span>
                            </a>
                        </div>
                        <div class="text-center form-group">
                            <a href="{{ route('fisico') }}" class="btn btn-neutral btn-icon col-sm-12 text-uppercase">
                                <span class="btn-inner--icon"><i class="ni ni-single-02"></i></span>
                                <span class="btn-inner--text">{{ __('Usuario Normal') }}</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection
