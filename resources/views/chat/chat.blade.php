@extends('layouts.app', ['title' => __('Chat')])

@section('content')
@include('layouts.headers.header', [
'title' => __('Chat'),
'class' => 'col-lg-12'
])
<div class="container">
	<div class="row justify-content-center mt--7" id="app">
		<div class="col-md-6">
			<div class="bg-white" style="border-top-left-radius: 20px; border-top-right-radius: 20px; border: 1px solid #5e72e4">
				<div class="form-group bg-secondary mb-0" style="padding: 20px; border-top-left-radius: 20px; border-top-right-radius: 20px">
					<h2 class="text-white">Conversa com {{$dono->name}}</h2>
				</div>
				<div class="mensagens pt-3 pb-3" style="height: 500px;    overflow-y: auto;">
					@foreach($mensagem as $msg)
					<div class="form-group row mb-1 {{_('text-white')}}" style="padding: 10px;">
						@if($msg->id==auth()->user()->id)
						<div class="col-sm-6">
						</div>
						@endif
						<div class="col-sm-6">
							<div class="@if($msg->id==auth()->user()->id) {{_('bg-secondary')}}@else {{_('bg-default')}} @endif" style="padding:10px; border-radius: 20px">
								<p style="margin-bottom: 0">{{_($msg->body)}}</p>
							</div>
							<small style="margin-bottom: 0; padding:10px; color: black;">@if($msg->id==auth()->user()->id){{_('Você')}} @else {{_($msg->name)}} @endif - {{$msg->created_at->format('d/m/Y H:i') }}</small>
						</div>
					</div>
					@endforeach
				</div>
				<form action="{{ route('mensagem')}}" method="post">
					@csrf
					<div class="row">
						<input type="hidden" name="conversation_id" value="{{$conversation_id}}">
						<input type="hidden" name="dono" value="{{$dono->id}}">
						<div class="col-sm-10" style="padding: 0"><input type="text" id="digita" class="form-control" placeholder="Digite sua mensagem..." name="mensagem"></div><div class="col-sm-2" style="padding: 0"><button class="btn btn-primary w-100" type="submit" onclick="enviamensagem()"><i class="fa fa-paper-plane"></i></button></div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>


@include('layouts.footers.auth')
</div>

@push('js')

<script src="/js/poly.js" type="text/javascript" charset="utf-8"></script>
<script src="/js/vendor.js" type="text/javascript" charset="utf-8"></script>
<script src="/js/main.js" type="text/javascript" charset="utf-8"></script>
<script src="/js/jquery.mask.js" type="text/javascript" charset="utf-8"></script>

@endpush
@endsection
