@extends('layouts.app', ['title' => __($title)])

@section('content')
@include('profile.partials.header', [
'title' => __($title),
'class' => 'col-lg-12'
])
<div class="container-fluid mt--7">
    <div class="row">
        <div class="col-md-12">
            <div class="card shadow">
                <div class="card-header border-0">
                    <div class="row align-items-center">
                        <div class="col-md-8 mt-2 mb-2">
                            <h3>{{ __('Animais') }}</h3>
                        </div>
                        <div class="col-md-4 text-right mb-2">
                            <a href="{{ route('animal.adicionar') }}" class="btn btn-sm btn-primary">{{ __('Adicionar Animais') }}</a>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    @if (session('status'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        {{ session('status') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @endif
                </div>
            </div>
            <div class="form-group row justify-content-center" style="margin-top: 20px">
                <?php $i=1 ?>
                <?php for ($j=0; $j<sizeof($animals);$j++) { ?>
                    <div class="col-md-4"> 
                        <div class="form-group text-center bg-default ">
                            <div class="fusion-image-hovers" style="padding-top: 1.25rem;">
                                @if($animals[$j]->avatar)
                                <div class="bg-background-animals rounded-circle hover-type-liftup  justify-content-center" style="background: url('/storage/animal/featured/{{$animals[$j]->id}}/{{$animals[$j]->avatars}}')">
                                </div>
                                @else
                                <div class="bg-background-animals rounded-circle hover-type-liftup  justify-content-center" style="background: url('/animals/avatar.png')">
                                </div>
                                @endif
                            </div>
                            <div class="card-header bg-transparent">

                                <div class="form-group">
                                    <h2 class="text-center text-white">{{$animals[$j]->name}}</h2>
                                    <h5 class="text-center text-white">{{$animals[$j]->sexo}}</h4>
                                        @if($animals[$j]->lost)
                                        <h5 class="text-center text-white">Perdido</h5>
                                        @else
                                        <h5 class="text-center text-white">Adoção</h5>
                                        @endif
                                        <a class="btn btn-primary mb-2" href="{{ route('animal.edit', $animals[$j]->id) }}" style="width: 100%">Editar</a>
                                        <form action="{{ route('animal.destroy', $animals[$j]->id) }}" method="post">
                                            @csrf
                                            @method('delete')

                                            <button type="button" class="btn btn-danger" onclick="confirm('{{ __("Você tem certeza que quer deletar esse Animal?") }}') ? this.parentElement.submit() : ''" style="width: 100%">
                                                {{ __('Deletar') }}
                                            </button>
                                        </form> 

                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php if($i%3==0) { ?> 
                        </div>
                        <div class="form-group row">
                        <?php } $i++; ?>
                    <?php } ?>

                </div>
                <div class="card-footer py-4">
                    <nav class="d-flex justify-content-end" aria-label="...">
                        {{ $animals->links() }}
                    </nav>
                </div>
            </div>
        </div>


        @include('layouts.footers.auth')
    </div>
    @endsection