@extends('layouts.app', ['title' => __('Edição do Pet')])

@section('content')
@include('profile.partials.header', [
'title' => __('Edição do Pet'),
'class' => 'col-lg-12'
])

<style type="text/css">
    .carousel-indicators li {
        position: relative;
        width: 30px;
        height: 3px;
        margin-right: 3px;
        margin-left: 3px;
        cursor: pointer;
        text-indent: -999px;
        background-color: #32325d;
        flex: 0 1 auto;
    }
    .carousel-indicators .active {
        background-color: #f5365c;
    }
</style>
<div class="container-fluid mt--7">
    <div class="row">
        <div class="col-xl-12 order-xl-1">
            <div class="card bg-secondary shadow">
                <div class="card-header bg-white border-0">
                    <div class="row align-items-center">
                        <div class="col-8">
                            <h3 class="mb-0">{{ __('Atualização de Pet') }}</h3>
                        </div>
                        <div class="col-4 text-right">
                            <a href="{{ route('animal.index') }}" class="btn btn-sm btn-primary">{{ __('Voltar para a lista') }}</a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <form method="post" action="{{ route('animal.update', ['animal' => $animal]) }}" autocomplete="off"  enctype="multipart/form-data">
                        @csrf
                        @method('put')
                        <h6 class="heading-small text-white mb-4">{{ __('Informações') }}</h6>
                        <div class="pl-lg-0">
                            <div class="form-group mb-3 col{{ $errors->has('name') ? ' has-danger' : '' }}">
                                <label class="form-control-label" for="input-name">{{ __('Nome') }}</label>
                                <input type="text" name="name" id="input-name" class="form-control form-control-alternative{{ $errors->has('name') ? ' is-invalid' : '' }}" placeholder="{{ __('Nome') }}" value="{{ $animal->name }}" required autofocus>

                                @if ($errors->has('name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group col mb-0">
                                <h6 class="heading-small text-white mb-3">{{ __('Marque somente as informações que são referentes ao seu animal') }}</h6>
                            </div>
                            <div class="form-group row mb-0">
                                <div class="col-md-3">
                                    <div class="custom-control custom-control-alternative custom-checkbox mb-3">
                                        <input class="custom-control-input" id="castration" name="castration" value="1" type="checkbox" @if($animal->castration==1) {{_('checked=="checked"')}} @endif>
                                        <label class="custom-control-label" for="castration">
                                            <span class="text-white">{{ __('É castrado') }}</span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="custom-control custom-control-alternative custom-checkbox mb-3">
                                        <input class="custom-control-input" id="vaccinated" name="vaccinated" value="1" type="checkbox"@if($animal->vaccinated==1) {{_('checked=="checked"')}} @endif>
                                        <label class="custom-control-label" for="vaccinated">
                                            <span class="text-white">{{ __('É vacinado') }}</span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="custom-control custom-control-alternative custom-checkbox mb-3">
                                        <input class="custom-control-input" id="dewormed" name="dewormed" value="1" type="checkbox"@if($animal->dewormed==1) {{_('checked=="checked"')}} @endif>
                                        <label class="custom-control-label" for="dewormed">
                                            <span class="text-white">{{ __('É vermifugado') }}</span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="custom-control custom-control-alternative custom-checkbox mb-3">
                                        <input class="custom-control-input" id="microchipped" name="microchipped" value="1" type="checkbox" @if($animal->microchipped==1) {{_('checked=="checked"')}} @endif>
                                        <label class="custom-control-label" for="microchipped">
                                            <span class="text-white">{{ __('É microchipado') }}</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group mb-3 col{{ $errors->has('note') ? ' has-danger' : '' }}">
                                <label class="form-control-label" for="note">{{ __('Deixe aqui uma breve descrição de seu animalzinho') }}</label>

                                <textarea class="form-control{{ $errors->has('note') ? ' is-invalid' : '' }}" placeholder="{{ __('Deixe aqui uma breve descrição de seu animalzinho') }}" rows="6"  id="note" name="note" required>{{ $animal->note }}</textarea>

                                @if ($errors->has('note'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('note') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group row mb-0" >
                                <div class="col-md-4{{ $errors->has('rga') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="rga">{{ __('RGA') }}</label>
                                    <div class="input-group input-group-alternative mb-3">
                                        <input class="form-control{{ $errors->has('rga') ? ' is-invalid' : '' }}" id="rga" placeholder="{{ __('RGA') }}" type="text" name="rga" value="{{$animal->rga}}">
                                    </div>
                                    @if ($errors->has('rga'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('rga') }}</strong>
                                    </span>
                                    @endif
                                </div> 
                                <div class="col-md-4{{ $errors->has('sex') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-email">{{ __('Sexo') }}</label>
                                    <div class="input-group input-group-alternative mb-3">
                                        <select class="form-control{{ $errors->has('sex') ? ' is-invalid' : '' }}" name="sex" required>
                                            <option value="">Selecione o Sexo</option>
                                            <option value="{{ 'male' }}" @if($animal->sex=='male') {{"selected='selected'"}}@endif >Macho</option>
                                            <option value="{{ 'female' }}"  @if($animal->sex=='female') {{"selected='selected'"}}@endif >Fêmea</option>
                                        </select>
                                    </div>
                                    @if ($errors->has('sex'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('sex') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="col-md-4 {{ $errors->has('birth_date') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-email">{{ __('Data de Nascimento') }}</label>
                                    <div class="input-group input-group-alternative mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fas fa-icon fa-calendar"></i>
                                            </span>
                                        </div>
                                        <input class="form-control{{ $errors->has('birth_date') ? ' is-invalid' : '' }}" placeholder="{{ __('Data de Nascimento') }}" type="date" name="birth_date" value="{{ $animal->birth_date }}">
                                    </div>
                                    @if ($errors->has('birth_date'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('birth_date') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6{{ $errors->has('breed_id') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="filterType">{{ __('Tipo de Animal') }}</label>
                                    <div class="input-group input-group-alternative mb-3">
                                        <select id="filterType" name="type_id" class="dynamic form-control" data-endpoint="api/type" data-labelfield="name" data-valuefield="id" data-current="{{$animal->type_id}}" data-targetid="filterBreed" data-updateeventtype="updateBreedSelect">
                                            <option value="">Selecione um Tipo</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6{{ $errors->has('breed_id') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="filterBreed">{{ __('Raça') }}</label>
                                    <div class="input-group input-group-alternative mb-3">
                                        @if(isset($type_id))
                                        <select id="filterBreed" name="breed_id" class="dynamic form-control" data-endpoint="{{ $type_id?"api/breed/{$type_id}":"" }}" data-labelfield="name" data-valuefield="id" data-current="{{$type_id}}">
                                            @elseif(isset($animal->type_id))
                                            <select id="filterBreed" name="breed_id" class="dynamic form-control" data-endpoint="{{ $animal->type_id?"api/breed/{$animal->type_id}":"" }}" data-labelfield="name" data-valuefield="id" data-current="{{$animal->breed_id}}">
                                                @else
                                                <select id="filterBreed" name="breed_id" class="dynamic form-control" data-labelfield="name" data-valuefield="id" data-current="{{$animal->breed_id}}">
                                                    @endif
                                                    <option value="">Selecione uma raça</option>
                                                </select>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row mb-0" >
                                    <div class="col-md-6{{ $errors->has('color') ? ' has-danger' : '' }}">
                                        <label class="form-control-label" for="size">{{ __('Cor') }}</label>
                                        <div class="input-group input-group-alternative mb-3">
                                            <input class="form-control{{ $errors->has('color') ? ' is-invalid' : '' }}" id="color" value="{{$animal->color}}" placeholder="{{ __('Cor') }}" type="text" name="color" required>
                                        </div>
                                        @if ($errors->has('color'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('color') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                    <div class="col-md-6 {{ $errors->has('size') ? ' has-danger' : '' }}">
                                        <label class="form-control-label" for="size">{{ __('Porte') }}</label>
                                        <div class="input-group input-group-alternative mb-3">
                                            <select class="form-control{{ $errors->has('size') ? ' is-invalid' : '' }}" name="size" required>
                                                <option value="">Selecione o Porte</option>
                                                <option value="{{ 'micro' }}"  @if($animal->size=='micro') {{"selected='selected'"}}@endif >Muito Pequeno</option>
                                                <option value="{{ 'small' }}"  @if($animal->size=='small') {{"selected='selected'"}}@endif >Pequeno</option>
                                                <option value="{{ 'medium' }}"  @if($animal->size=='medium') {{"selected='selected'"}}@endif >Médio</option>
                                                <option value="{{ 'big' }}"  @if($animal->size=='big') {{"selected='selected'"}}@endif >Grande</option>
                                                <option value="{{ 'giant' }}"  @if($animal->size=='giant') {{"selected='selected'"}}@endif >Muito Grande</option>
                                            </select>
                                        </div>
                                        @if ($errors->has('size'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('size') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                @if($animal->avatar)
                                <div class="form-group m-3" align="center" style="margin:0">
                                    <img src="/storage/animal/featured/{{ $animal->id }}/{{$animal->avatar->filename}}" height="200" align="center">
                                </div>
                                @endif
                                <div class="form-group row mb-0">
                                    <div class="col-sm-12{{ $errors->has('featured') ? ' has-danger' : '' }}">
                                        <label class="form-control-label" for="featured">Foto de Destaque</label>
                                        <div class="input-group input-group-alternative mb-3">
                                            <input type="file" class="form-control{{ $errors->has('featured') ? ' is-invalid' : '' }}" name="featured" id="featured" accept=".jpeg, .png, .jpg, .gif">
                                        </div>
                                        @if ($errors->has('featured'))
                                        <span class="invalid-feedback" style="display: block;" role="alert">
                                            <strong>{{ $errors->first('featured') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                @if(isset($animal->pictures[0]))
                                <div class="form-group mb-3">
                                   <div class="bg-default shadow pt-3 pb-3">


                                    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                                        <ol class="carousel-indicators">
                                            <?php $i=0 ?>
                                            @foreach($animal->pictures as $picture)
                                            <li data-target="#carouselExampleIndicators" data-slide-to="<?=$i?>" @if($i==0) {{ __('class=active') }} @endif></li>
                                            <?php $i++; ?>

                                            @endforeach
                                        </ol>
                                        <div class="carousel-inner align-items-center">
                                            <?php $i=0 ?>

                                            @foreach($animal->pictures as $picture)
                                            <div class="carousel-item @if($i==0) {{ __('active') }} @endif" style="width: 34%; margin-left: 33%; margin-right: 33%">
                                                <p  class="d-block w-100" style="background:url('/storage/animals/galery/{{ $animal->id }}/{{ $picture->filename }}'); background-size: contain; background-color: transparent; background-position: center; background-repeat: no-repeat; padding: 50%; margin-bottom: 0;" alt="{{ $animal->name }}" ></p>
                                            </div>
                                            <?php $i++; ?>
                                            @endforeach
                                        </div>
                                        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                                            <span class="carousel-control-prev-icon" aria-hidden="true" style="background-color: #000000!important; width: 30%; height: 20%;"></span>
                                            <span class="sr-only">Anterior</span>
                                        </a>
                                        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                                            <span class="carousel-control-next-icon" aria-hidden="true" style="background-color: #000000!important; width: 30%; height: 20%;"></span>
                                            <span class="sr-only">Próximo</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col mb-0">
                                <div class="input-group input-group-alternative mb-2">
                                    <label class="form-control-label">O que deseja fazer?</label>
                                </div>
                                <div class="input-group input-group-alternative mb-3">

                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input replace" type="radio" name="troca" id="replace" value="{{'replace'}}">
                                        <label class="form-check-label text-white" for="replace">Substituir Imagens</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input replace" type="radio" name="troca" id="new" value="{{'new'}}" active>
                                        <label class="form-check-label text-white" for="new">Novas Imagens</label>
                                    </div>
                                </div>
                            </div>

                            @endif

                            <div class="form-group row mb-0">
                                <div class="col-sm-12{{ $errors->has('description') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="photo">Fotos do Animal</label>
                                    <div class="input-group input-group-alternative mb-3">
                                        <input type="file" class="form-control{{ $errors->has('photo') ? ' is-invalid' : '' }}" name="photo[]" id="photo" accept=".jpeg, .png, .jpg, .gif" multiple>
                                    </div>
                                    @if ($errors->has('photo'))
                                    <span class="invalid-feedback" style="display: block;" role="alert">
                                        <strong>{{ $errors->first('photo') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group mb-0">
                                <div class="row col-sm-12 mb-2">
                                    <label class="form-control-label" for="photo">O que deseja fazer?</label>
                                </div>
                                <div class="input-group input-group-alternative mb-3 col">

                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input lost" type="radio" name="wish" id="adoption" value="{{'adoption'}}" required @if($animal->adoption) {{"checked='checked'"}}@endif>
                                        <label class="form-check-label text-white" for="adoption" >Colocar para adoção</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input lost" type="radio" name="wish" id="lost" value="{{'lost'}}" required @if($animal->lost) {{"checked='checked'"}}@endif>
                                        <label class="form-check-label text-white" for="lost">Localizar animal perdido</label>
                                    </div>
                                </div>
                            </div>
                            <div class="localizacao">
                                <hr>
                                <h6 class="heading-small text-white mb-4">{{ __('Digite a sua ultima localidade') }}</h6>
                                <div class="form-group row mb-0">
                                    <div class="col-md-6{{ $errors->has('zip_code') ? ' has-danger' : '' }}">
                                        <div class="input-group input-group-alternative mb-0">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="ni ni-map-big"></i></span>
                                            </div>    
                                            <input class="form-control{{ $errors->has('zip_code') ? ' is-invalid' : '' }}" placeholder="{{ __('CEP') }}" type="text" name="zip_code" id="cep" value="@if($animal->lost)@if(sizeof($animal->lost->addresses)>0){{$animal->lost->addresses[0]->zip_code}}@endif @endif" >
                                        </div>
                                        @if ($errors->has('zip_code'))
                                        <span class="invalid-feedback" style="display: block;" role="alert">
                                            <strong>{{ $errors->first('zip_code') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                    <div class="col-md-6{{ $errors->has('district') ? ' has-danger' : '' }}">
                                        <div class="input-group input-group-alternative mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="ni ni-pin-3"></i></span>
                                            </div>
                                            <input class="form-control{{ $errors->has('district') ? ' is-invalid' : '' }}" placeholder="{{ __('Bairro') }}" type="text" name="district" id="bairro" value="@if($animal->lost)@if(sizeof($animal->lost->addresses)>0){{$animal->lost->addresses[0]->district}}@endif @endif">
                                        </div>
                                        @if ($errors->has('district'))
                                        <span class="invalid-feedback" style="display: block;" role="alert">
                                            <strong>{{ $errors->first('district') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row mb-0">
                                    <div class="col-md-9{{ $errors->has('street') ? ' has-danger' : '' }}">
                                        <div class="input-group input-group-alternative mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="ni ni-pin-3"></i></span>
                                            </div>
                                            <input class="form-control{{ $errors->has('street') ? ' is-invalid' : '' }}" placeholder="{{ __('Logradouro') }}" type="text" name="street" id="logradouro" value="@if($animal->lost) @if(sizeof($animal->lost->addresses)>0){{$animal->lost->addresses[0]->street}}@endif @endif">
                                        </div>
                                        @if ($errors->has('street'))
                                        <span class="invalid-feedback" style="display: block;" role="alert">
                                            <strong>{{ $errors->first('street') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                    <div class="col-md-3{{ $errors->has('number') ? ' has-danger' : '' }}">
                                        <div class="input-group input-group-alternative mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="ni ni-pin-3"></i></span>
                                            </div>
                                            <input class="form-control{{ $errors->has('number') ? ' is-invalid' : '' }}" placeholder="{{ __('Nº') }}" value="@if($animal->lost) @if(sizeof($animal->lost->addresses)>0){{ $animal->lost->addresses[0]->number }}@endif @endif" type="text" name="number" id="number">
                                        </div>
                                        @if ($errors->has('number'))
                                        <span class="invalid-feedback" style="display: block;" role="alert">
                                            <strong>{{ $errors->first('number') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group col mb-0{{ $errors->has('complement') ? ' has-danger' : '' }}">
                                    <div class="input-group input-group-alternative mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="ni ni-pin-3"></i></span>
                                        </div>
                                        <input class="form-control{{ $errors->has('complement') ? ' is-invalid' : '' }}" placeholder="{{ __('Complemento') }}" type="text" name="complement" id="complement" @if($animal->lost) value="@if(sizeof($animal->lost->addresses)>0){{$animal->lost->addresses[0]->complement}}@endif"@endif>
                                    </div>
                                    @if ($errors->has('complement'))
                                    <span class="invalid-feedback" style="display: block;" role="alert">
                                        <strong>{{ $errors->first('complement') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group row mb-0">
                                    <div class="col-md-6{{ $errors->has('state_id') ? ' has-danger' : '' }}">
                                        <div class="input-group input-group-alternative mb-3">

                                            <select id="filterState" name="state_id" class="dynamic form-control" data-endpoint="api/states" data-labelfield="name" data-valuefield="id" data-current="@if($animal->lost)@if(sizeof($animal->lost->addresses)>0){{$animal->lost->addresses[0]->state_id}}@endif @endif" data-targetid="filterCity" data-updateevent="updateCitySelect">
                                                <option value="">Selecione um estado</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6{{ $errors->has('city_id') ? ' has-danger' : '' }}">
                                        <div class="input-group input-group-alternative mb-3">

                                            @if(isset($state_id))
                                            <select id="filterCity" name="city_id" class="dynamic form-control" data-endpoint="{{ $state_id?"api/cities/{$state_id}":"" }}" data-labelfield="name" data-valuefield="id" data-current="@if($animal->lost) @if(sizeof($animal->lost->addresses)>0){{$animal->lost->addresses[0]->city_id}}@endif @endif">
                                                @else
                                                <select id="filterCity" name="city_id" class="dynamic form-control" data-labelfield="name" data-valuefield="id" data-current="@if($animal->lost) @if(sizeof($animal->lost->addresses)>0){{$animal->lost->addresses[0]->city_id}}@endif @endif">
                                                    @endif
                                                    <option value="">Selecione uma cidade</option>
                                                </select>
                                            </select>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="text-center">
                                <button type="submit" class="btn btn-primary mt-4">{{ __('Salvar') }}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @include('layouts.footers.auth')
</div>

@push('js')

<script src="/js/poly.js" type="text/javascript" charset="utf-8"></script>
<script src="/js/vendor.js" type="text/javascript" charset="utf-8"></script>
<script src="/js/main.js" type="text/javascript" charset="utf-8"></script>
<script src="/js/jquery.mask.js" type="text/javascript" charset="utf-8"></script>
<script src="/js/buscacep.js" type="text/javascript" charset="utf-8"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $('#cep').mask('99999-999');
    });

    $(document).ready(function(){
        <?php if($animal->lost){?>
            $('.localizacao').show();
            $('#cep').attr('required', 'required');
            $('#logradouro').attr('required', 'required');
            $('#complemento').attr('required', 'required');
            $('#bairro').attr('required', 'required');
            $('#filterState').attr('required', 'required');
            $('#filterCity').attr('required', 'required');
        <?php } else{ ?>
            $('.localizacao').hide();
        <?php } ?>
        $('.lost').change(function (event) {
            if($(this).val() == 'adoption') {
                $('.localizacao').hide();
                $('#cep').removeAttr('required');
                $('#logradouro').removeAttr('required');
                $('#complemento').removeAttr('required');
                $('#bairro').removeAttr('required');
                $('#filterState').removeAttr('required');
                $('#filterCity').removeAttr('required');
            } else {
                $('.localizacao').show();
                $('#cep').attr('required', 'required');
                $('#logradouro').attr('required', 'required');
                $('#complemento').attr('required', 'required');
                $('#bairro').attr('required', 'required');
                $('#filterState').attr('required', 'required');
                $('#filterCity').attr('required', 'required');
            }

        });

    });


</script>
@endpush
@endsection