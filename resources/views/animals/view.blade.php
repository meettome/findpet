@extends('layouts.app', ['title' => __('Visualização de Pet')])

@section('content')
@include('profile.partials.header', [
'title' => __($animal->name),
'description' => '',
'class' => 'col-lg-12'
])
<style type="text/css">
    .carousel-indicators li {
        position: relative;
        width: 30px;
        height: 3px;
        margin-right: 3px;
        margin-left: 3px;
        cursor: pointer;
        text-indent: -999px;
        background-color: #32325d;
        flex: 0 1 auto;
    }
    .carousel-indicators .active {
        background-color: #f5365c;
    }
</style>
<div class="container-fluid mt--7">
    <div class="row">
        <div class="col-md-6 order-xl-1">
            <div class="bg-default shadow pt-3 pb-3">


                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <?php $i=0 ?>
                        @foreach($animal->pictures as $picture)
                        <li data-target="#carouselExampleIndicators" data-slide-to="<?=$i?>" @if($i==0) {{ __('class=active') }} @endif></li>
                        <?php $i++; ?>

                        @endforeach
                    </ol>
                    <div class="carousel-inner">
                        <?php $i=0 ?>

                        @foreach($animal->pictures as $picture)
                        <div class="carousel-item @if($i==0) {{ __('active') }} @endif">
                            <p  class="d-block w-100" style="background:url('/storage/animals/galery/{{ $animal->id }}/{{ $picture->filename }}'); background-size: contain; background-color: transparent; background-position: center; background-repeat: no-repeat; padding: 50%; margin-bottom: 0;" alt="{{ $animal->name }}" ></p>
                        </div>
                        <?php $i++; ?>
                        @endforeach
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true" style="background-color: #000000!important; width: 30%; height: 20%;"></span>
                        <span class="sr-only">Anterior</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true" style="background-color: #000000!important; width: 30%; height: 20%;"></span>
                        <span class="sr-only">Próximo</span>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-md-6 order-xl-1">
            <div class="card shadow pt-4 pb-4">
                @if (session('status'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    {{ session('status') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @endif
                <h2 class="text-center mt-2 mb-3">Fixa Tecnica</h2>
                <div class="row">
                    <div class="col-md-6">
                        <h4 class="mb-1">Nome: {{ __($animal->name) }}</h4>
                        <h4 class="mb-1">Tipo: {{ __($animal->types->name) }}</h4>
                        <h4 class="mb-1">Raça: {{ __($animal->breeds->name) }}</h4>
                        <h4 class="mb-1">Sexo: {{ __($animal->sexo) }}</h4>
                        <h4 class="mb-1">Idade: {{ __($animal->idade) }}</h4>
                        <h4 class="mb-1">Cor: {{ __($animal->color) }}</h4>
                        <h4 class="mb-1">Porte: {{ __($animal->tamanho) }}</h4>
                        @if($animal->adopter != null)
                        <h4 class="mb-1">Castrado? @if($animal->castration==1) {{ __('Sim') }} @else {{ __('Não') }} @endif</h4>
                        <h4 class="mb-1">Vacinado? @if($animal->vaccinated==1) {{ __('Sim') }} @else {{ __('Não') }} @endif</h4>
                        <h4 class="mb-1">Vermifugado? @if($animal->dewormed==1) {{ __('Sim') }} @else {{ __('Não') }} @endif</h4>
                        <h4 class="mb-1">Microchipado? @if($animal->microchipped==1) {{ __('Sim') }} @else {{ __('Não') }} @endif</h4>
                        @endif
                    </div>

                    <div class="col-md-6">
                        <h4 class="mb-1">Detalhes: {{ __($animal->note) }}</h4>
                    </div>
                </div>
                @if($animal->lost!=null)
                <h2 class="text-center mt-1 mb-2">Ultima Localização</h2>
                <div class="row">
                    <div class="col-md-6">
                        <h4 class="mb-1">Logradouro: {{ __($animal->lost->addresses[0]->street) }}</h4>
                        <h4 class="mb-1">Bairro: {{ __($animal->lost->addresses[0]->district) }}</h4>
                        <h4 class="mb-1">Numero: {{ __($animal->lost->addresses[0]->number) }}</h4>
                        <h4 class="mb-1">Complemento: {{ __($animal->lost->addresses[0]->complement) }}</h4>
                    </div>
                    <div class="col-md-6">
                        <h4 class="mb-1">Cep: {{ __($animal->lost->addresses[0]->zip_code) }}</h4>
                        <h4 class="mb-1">Cidade: {{ __($animal->lost->addresses[0]->city->name) }}</h4>
                        <h4 class="mb-1">Estado: {{ __($animal->lost->addresses[0]->state->name) }}</h4>
                    </div>
                </div>
                @endif
                <div class="row justify-content-center form-group">
                    <div class="col-md-12">
                        <h2 class="mt-3 mb-2 text-center">Dados do Dono</h2>
                    </div>
                </div>
                <div class="row justify-content-center form-group">
                    <div class="col-md-12">
                        <h4 class="mb-2 ">Nome: {{$dono->name}}</h4>
                        <h4 class="mb-2">Telefone: {{$dono->cellphone}}</h4>
                        @if($dono->addresses)
                        <h4 class="mb-2">Cidade: {{$dono->addresses->city->name}} - {{$dono->addresses->state->uf}}</h4>
                        @endif
                    </div>
                </div>

                <div class="row justify-content-center form-group">
                    <div class="col-md-12">
                        <h2 class="mt-1 mb-2 text-center">Redes Sociais do Dono</h2>
                    </div>
                </div>
                <div class="row">
                    <?php $i = 1 ?>
                    @foreach($dono->SocialNetworks as $social)
                    <div class="col-md-6">
                        <h4><i class="{{_($social->type->icon)}}"></i> @if($social->type->id == 16 || $social->type->id == 17 || $social->type->id == 14) {{_($social->phone)}} @else {{_($social->user)}} @endif</h4>
                    </div>
                    @if($i%2==0)
                </div>
                <div class="row form-group">
                    @endif
                    <?php $i++ ?>
                    @endforeach

                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-12">
                    @foreach($whatsapp as $whats)
                    <div class="mb-2">
                        <a class="btn btn-success" href="https://api.whatsapp.com/send?phone=55{{$whats->phone}}" style="width: 100%" target="_blank"><i class="{{$whats->type->icon}}"></i> Ir para o whatsapp</a>
                    </div>
                    @endforeach
                    <div class="mb-2">
                        @if($animal->adoption!=null)
                        <a class="btn btn-primary" href="{{ route('emaildono', $animal->id) }}" style="width: 100%">Enviar email para o dono</a>
                        @else
                        <a class="btn btn-primary" href="#" data-toggle="modal" data-target="#myModal" style="width: 100%">Enviar localização que viu</a>
                        @endif
                    </div>
                    <form action="{{route('chat')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="id" value="{{$id}}">
                        <button class="btn btn-primary" style="width: 100%">Falar no chat com o dono</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal p/ Atualização de Avatar -->
<div class="modal" id="myModal">
    <div class="modal-dialog">
        <div class="modal-content bg-default">
            <div class="modal-header">
                <h4 class="modal-title text-white text-uppercase">Ultima Localização</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <form action="{{route('lost.addresses', $animal->id)}}" method="post" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="form-group row mb-0{{ $errors->has('zip_code') ? ' has-danger' : '' }}">
                        <div class="input-group input-group-alternative mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="ni ni-map-big"></i></span>
                            </div>    
                            <input class="form-control{{ $errors->has('zip_code') ? ' is-invalid' : '' }}" placeholder="{{ __('CEP') }}" type="text" name="zip_code" id="cep" >
                        </div>
                        @if ($errors->has('zip_code'))
                        <span class="invalid-feedback" style="display: block;" role="alert">
                            <strong>{{ $errors->first('zip_code') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group row mb-0{{ $errors->has('district') ? ' has-danger' : '' }}">
                        <div class="input-group input-group-alternative mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="ni ni-pin-3"></i></span>
                            </div>
                            <input class="form-control{{ $errors->has('district') ? ' is-invalid' : '' }}" placeholder="{{ __('Bairro') }}" type="text" name="district" id="bairro">
                        </div>
                        @if ($errors->has('district'))
                        <span class="invalid-feedback" style="display: block;" role="alert">
                            <strong>{{ $errors->first('district') }}</strong>
                        </span>
                        @endif
                    </div>

                    <div class="form-group row mb-0{{ $errors->has('street') ? ' has-danger' : '' }}">
                        <div class="input-group input-group-alternative mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="ni ni-pin-3"></i></span>
                            </div>
                            <input class="form-control{{ $errors->has('street') ? ' is-invalid' : '' }}" placeholder="{{ __('Logradouro') }}" type="text" name="street" id="logradouro">
                        </div>
                        @if ($errors->has('street'))
                        <span class="invalid-feedback" style="display: block;" role="alert">
                            <strong>{{ $errors->first('street') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group row mb-0{{ $errors->has('number') ? ' has-danger' : '' }}">
                        <div class="input-group input-group-alternative mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="ni ni-pin-3"></i></span>
                            </div>
                            <input class="form-control{{ $errors->has('number') ? ' is-invalid' : '' }}" placeholder="{{ __('Nº') }}" type="text" name="number" id="number">
                        </div>
                        @if ($errors->has('number'))
                        <span class="invalid-feedback" style="display: block;" role="alert">
                            <strong>{{ $errors->first('number') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group mb-0{{ $errors->has('complement') ? ' has-danger' : '' }}">
                        <div class="input-group input-group-alternative mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="ni ni-pin-3"></i></span>
                            </div>
                            <input class="form-control{{ $errors->has('complement') ? ' is-invalid' : '' }}" placeholder="{{ __('Complemento') }}" type="text" name="complement" id="complement" >
                        </div>
                        @if ($errors->has('complement'))
                        <span class="invalid-feedback" style="display: block;" role="alert">
                            <strong>{{ $errors->first('complement') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group row mb-0 {{ $errors->has('state_id') ? ' has-danger' : '' }}">
                        <div class="input-group input-group-alternative mb-3">
                            <select id="filterState" name="state_id" class="dynamic form-control" data-endpoint="api/states" data-labelfield="name" data-valuefield="id" data-current="{{ $state_id ?? old('state_id') }}" data-targetid="filterCity" data-updateevent="updateCitySelect">
                                <option value="">Selecione um estado</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row mb-0{{ $errors->has('city_id') ? ' has-danger' : '' }}">
                        <div class="input-group input-group-alternative mb-3">
                            @if(isset($state_id))
                            <select id="filterCity" name="city_id" class="dynamic form-control" data-endpoint="{{ $state_id?"api/cities/{$state_id}":"" }}" data-labelfield="name" data-valuefield="id" data-current="{{ $city_id ?? old('city_id')  }}">
                                @else
                                <select id="filterCity" name="city_id" class="dynamic form-control" data-labelfield="name" data-valuefield="id" data-current="{{ $city_id ?? old('city_id') }}">
                                    @endif
                                    <option value="">Selecione uma cidade</option>
                                </select>
                            </select>
                        </div>
                    </div>

                    <div class="text-center">
                        <button type="submit" class="btn btn-primary mt-4">Atualizar</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
@include('layouts.footers.auth')
</div>

@push('js')

<script src="/js/poly.js" type="text/javascript" charset="utf-8"></script>
<script src="/js/vendor.js" type="text/javascript" charset="utf-8"></script>
<script src="/js/main.js" type="text/javascript" charset="utf-8"></script>
<script src="/js/jquery.mask.js" type="text/javascript" charset="utf-8"></script>
<script src="/js/buscacep.js" type="text/javascript" charset="utf-8"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $('.celular').mask('(99) 9 9999-9999');
        $('.telefone').mask('(99) 9999-9999');
        $('#cpf').mask('999.999.999-99');
        $('#cnpj').mask('99.999.999/9999-99');
        $('#cep').mask('99999-999');
    });

</script>
@endpush
@endsection
