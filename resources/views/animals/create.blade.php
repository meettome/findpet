@extends('layouts.app', ['title' => __('Adicionar Pet')])

@section('content')
@include('profile.partials.header', [
'title' => __('Adicionar Pet'),
'class' => 'col-lg-12'
])
<div class="container-fluid mt--7">
	<div class="row">
		<div class="col-xl-12 order-xl-1">
			<div class="card bg-secondary shadow">
				<div class="card-header bg-white border-0">
					<div class="row align-items-center">
						<div class="col-8">
							<h3 class="mb-0">{{ __('Adicionar Pet') }}</h3>
						</div>
						<div class="col-4 text-right">
							<a href="{{ route('animal.index') }}" class="btn btn-sm btn-primary">{{ __('Voltar para a lista') }}</a>
						</div>
					</div>
				</div>
				<div class="card-body">
					<form method="post" action="{{ route('animal.store') }}" autocomplete="off"  enctype="multipart/form-data">
						@csrf	
						<h6 class="heading-small text-white mb-4">{{ __('Informações') }}</h6>
						<div class="pl-lg-0">
							<div class="form-group mb-3 col{{ $errors->has('name') ? ' has-danger' : '' }}">
								<label class="form-control-label" for="input-name">{{ __('Nome') }}</label>
								<input type="text" name="name" id="input-name" class="form-control form-control-alternative{{ $errors->has('name') ? ' is-invalid' : '' }}" placeholder="{{ __('Nome') }}" value="{{ old('Nome') }}" required autofocus>

								@if ($errors->has('name'))
								<span class="invalid-feedback" role="alert">
									<strong>{{ $errors->first('name') }}</strong>
								</span>
								@endif
							</div> 
							<div class="form-group col mb-0">
								<h6 class="heading-small text-white mb-3">{{ __('Marque somente as informações que são referentes ao seu animal') }}</h6>
							</div>
							<div class="form-group row mb-0">
								<div class="col-md-3">
									<div class="custom-control custom-control-alternative custom-checkbox mb-3">
										<input class="custom-control-input" id="castration" name="castration" value="1" type="checkbox">
										<label class="custom-control-label" for="castration">
											<span class="text-white">{{ __('É castrado') }}</span>
										</label>
									</div>
								</div>
								<div class="col-md-3">
									<div class="custom-control custom-control-alternative custom-checkbox mb-3">
										<input class="custom-control-input" id="vaccinated" name="vaccinated" value="1" type="checkbox">
										<label class="custom-control-label" for="vaccinated">
											<span class="text-white">{{ __('É vacinado') }}</span>
										</label>
									</div>
								</div>
								<div class="col-md-3">
									<div class="custom-control custom-control-alternative custom-checkbox mb-3">
										<input class="custom-control-input" id="dewormed" name="dewormed" value="1" type="checkbox">
										<label class="custom-control-label" for="dewormed">
											<span class="text-white">{{ __('É vermifugado') }}</span>
										</label>
									</div>
								</div>
								<div class="col-md-3">
									<div class="custom-control custom-control-alternative custom-checkbox mb-3">
										<input class="custom-control-input" id="microchipped" name="microchipped" value="1" type="checkbox">
										<label class="custom-control-label" for="microchipped">
											<span class="text-white">{{ __('É microchipado') }}</span>
										</label>
									</div>
								</div>
							</div>
							<div class="form-group mb-3 col{{ $errors->has('note') ? ' has-danger' : '' }}">
								<label class="form-control-label" for="input-email">{{ __('Deixe aqui uma breve descrição de seu animalzinho') }}</label>

								<textarea class="form-control{{ $errors->has('note') ? ' is-invalid' : '' }}" placeholder="{{ __('Deixe aqui uma breve descrição de seu animalzinho') }}" rows="6" name="note" required>{{ old('note') }}</textarea>

								@if ($errors->has('note'))
								<span class="invalid-feedback" role="alert">
									<strong>{{ $errors->first('note') }}</strong>
								</span>
								@endif
							</div>
							<div class="form-group row mb-0">
								<div class="col-md-4{{ $errors->has('rga') ? ' has-danger' : '' }}">
									<label class="form-control-label" for="rga">{{ __('RGA') }}</label>
									<div class="input-group input-group-alternative mb-3">
										<input class="form-control{{ $errors->has('rga') ? ' is-invalid' : '' }}" id="rga" placeholder="{{ __('RGA') }}" type="text" name="rga" >
									</div>
									@if ($errors->has('rga'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('rga') }}</strong>
									</span>
									@endif
								</div>
								<div class="col-md-4{{ $errors->has('sex') ? ' has-danger' : '' }}">
									<label class="form-control-label" for="input-email">{{ __('Sexo') }}</label>
									<div class="input-group input-group-alternative mb-3">
										<select class="form-control{{ $errors->has('sex') ? ' is-invalid' : '' }}" name="sex" required>
											<option value="">Selecione o Sexo</option>
											<option value="{{ 'male' }}" >Macho</option>
											<option value="{{ 'female' }}" >Fêmea</option>
										</select>
									</div>
									@if ($errors->has('sex'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('sex') }}</strong>
									</span>
									@endif
								</div>
								<div class="col-md-4{{ $errors->has('birth_date') ? ' has-danger' : '' }}">
									<label class="form-control-label" for="input-email">{{ __('Data de Nascimento') }}</label>
									<div class="input-group input-group-alternative mb-3">
										<div class="input-group-prepend">
											<span class="input-group-text">
												<i class="fas fa-icon fa-calendar"></i>
											</span>
										</div>
										<input class="form-control{{ $errors->has('birth_date') ? ' is-invalid' : '' }}" placeholder="{{ __('Data de Nascimento') }}" type="date" name="birth_date" value="{{ old('birth_date') }}">
									</div>
									@if ($errors->has('birth_date'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('birth_date') }}</strong>
									</span>
									@endif
								</div>
							</div>

							<div class="form-group row mb-0">
								<div class="col-md-6{{ $errors->has('breed_id') ? ' has-danger' : '' }}">
									<label class="form-control-label" for="filterType">{{ __('Tipo de Animal') }}</label>
									<div class="input-group input-group-alternative mb-3">
										<select id="filterType" name="type_id" class="dynamic form-control" data-endpoint="api/type" data-labelfield="name" data-valuefield="id" data-current="{{ $type_id ?? old('type_id') }}" data-targetid="filterBreed" data-updateeventtype="updateBreedSelect">
											<option value="">Selecione um Tipo</option>
										</select>
									</div>
								</div>

								<div class="col-md-6{{ $errors->has('breed_id') ? ' has-danger' : '' }}">
									<label class="form-control-label" for="filterBreed">{{ __('Raça') }}</label>
									<div class="input-group input-group-alternative mb-3">
										@if(isset($type_id))
										<select id="filterBreed" name="breed_id" class="dynamic form-control" data-endpoint="{{ $type_id?"api/breed/{$type_id}":"" }}" data-labelfield="name" data-valuefield="id" data-current="{{ $breed_id ?? old('breed_id')  }}">
											@else
											<select id="filterBreed" name="breed_id" class="dynamic form-control" data-labelfield="name" data-valuefield="id" data-current="{{ $breed_id ?? old('breed_id') }}">
												@endif
												<option value="">Selecione uma raça</option>
											</select>
										</select>
									</div>
								</div>
							</div>

							<div class="form-group row m-0" >
								<div class="col-md-6{{ $errors->has('color') ? ' has-danger' : '' }}">
									<label class="form-control-label" for="size">{{ __('Cor') }}</label>
									<div class="input-group input-group-alternative mb-3">
										<input class="form-control{{ $errors->has('color') ? ' is-invalid' : '' }}" id="color" placeholder="{{ __('Cor') }}" type="text" name="color" required>
									</div>
									@if ($errors->has('color'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('color') }}</strong>
									</span>
									@endif
								</div>
								<div class="col-md-6 {{ $errors->has('size') ? ' has-danger' : '' }}">
									<label class="form-control-label" for="size">{{ __('Porte') }}</label>
									<div class="input-group input-group-alternative mb-3">
										<select class="form-control{{ $errors->has('size') ? ' is-invalid' : '' }}" name="size" required>
											<option value="">Selecione o Porte</option>
											<option value="{{ 'micro' }}" >Muito Pequeno</option>
											<option value="{{ 'small' }}" >Pequeno</option>
											<option value="{{ 'medium' }}" >Médio</option>
											<option value="{{ 'big' }}" >Grande</option>
											<option value="{{ 'giant' }}" >Muito Grande</option>
										</select>
									</div>
									@if ($errors->has('size'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('size') }}</strong>
									</span>
									@endif
								</div>
							</div>

							<div class="form-group row mb-0">
								<div class="col-sm-12{{ $errors->has('featured') ? ' has-danger' : '' }}">
									<label class="form-control-label" for="featured">Foto de Destaque</label>
									<div class="input-group input-group-alternative mb-3">
										<input type="file" class="form-control{{ $errors->has('featured') ? ' is-invalid' : '' }}" name="featured" id="featured" accept=".jpeg, .png, .jpg, .gif" required>
									</div>
									@if ($errors->has('featured'))
									<span class="invalid-feedback" style="display: block;" role="alert">
										<strong>{{ $errors->first('featured') }}</strong>
									</span>
									@endif
								</div>
							</div>
							<div class="form-group row mb-0">
								<div class="col-sm-12{{ $errors->has('photo') ? ' has-danger' : '' }}">
									<label class="form-control-label" for="photo">Fotos do Animal</label>
									<div class="input-group input-group-alternative mb-3">
										<input type="file" class="form-control{{ $errors->has('photo') ? ' is-invalid' : '' }}" name="photo[]" id="photo" accept=".jpeg, .png, .jpg, .gif" multiple>
									</div>
									@if ($errors->has('photo'))
									<span class="invalid-feedback" style="display: block;" role="alert">
										<strong>{{ $errors->first('photo') }}</strong>
									</span>
									@endif
								</div>
							</div>
							<hr>
							<div class="form-group mb-0">
								<div class="row col-sm-12 mb-2">
									<label class="form-control-label" for="photo">O que deseja fazer?</label>
								</div>
								<div class="col">
									<div class="form-check form-check-inline mb-3">
										<input class="form-check-input" type="radio" name="wish" id="adoption" value="{{'adoption'}}" required>
										<label class="form-check-label text-white" for="adoption">Colocar para adoção</label>
									</div>
									<div class="form-check form-check-inline mb-3">
										<input class="form-check-input" type="radio" name="wish" id="lost" value="{{'lost'}}" required>
										<label class="form-check-label text-white" for="lost">Localizar animal perdido</label>
									</div>
								</div>
							</div>
							<div class="localizacao">
								<hr>

								<h6 class="heading-small text-white mb-4">{{ __('Digite a sua ultima localidade') }}</h6>
								<div class="form-group row mb-0">
									<div class="col-md-6{{ $errors->has('zip_code') ? ' has-danger' : '' }}">
										<div class="input-group input-group-alternative mb-3">
											<div class="input-group-prepend">
												<span class="input-group-text"><i class="ni ni-map-big"></i></span>
											</div>
											<input class="form-control{{ $errors->has('zip_code') ? ' is-invalid' : '' }}" placeholder="{{ __('CEP') }}" type="text" name="zip_code" id="cep" value="{{ old('zip_code') }}" >
										</div>
										@if ($errors->has('zip_code'))
										<span class="invalid-feedback" style="display: block;" role="alert">
											<strong>{{ $errors->first('zip_code') }}</strong>
										</span>
										@endif
									</div>
									<div class="col-md-6{{ $errors->has('district') ? ' has-danger' : '' }}">
										<div class="input-group input-group-alternative mb-3">
											<div class="input-group-prepend">
												<span class="input-group-text"><i class="ni ni-pin-3"></i></span>
											</div>
											<input class="form-control{{ $errors->has('district') ? ' is-invalid' : '' }}" placeholder="{{ __('Bairro') }}" type="text" name="district" id="bairro" value="{{ old('district') }}">
										</div>
										@if ($errors->has('district'))
										<span class="invalid-feedback" style="display: block;" role="alert">
											<strong>{{ $errors->first('district') }}</strong>
										</span>
										@endif
									</div>
								</div>

								<div class="form-group row mb-0">
									<div class="col-md-9{{ $errors->has('street') ? ' has-danger' : '' }}">
										<div class="input-group input-group-alternative mb-3">
											<div class="input-group-prepend">
												<span class="input-group-text"><i class="ni ni-pin-3"></i></span>
											</div>
											<input class="form-control{{ $errors->has('street') ? ' is-invalid' : '' }}" placeholder="{{ __('Logradouro') }}" type="text" name="street" id="logradouro" value="{{ old('street') }}">
										</div>
										@if ($errors->has('street'))
										<span class="invalid-feedback" style="display: block;" role="alert">
											<strong>{{ $errors->first('street') }}</strong>
										</span>
										@endif
									</div>
									<div class="col-md-3{{ $errors->has('number') ? ' has-danger' : '' }}">
										<div class="input-group input-group-alternative mb-3">
											<div class="input-group-prepend">
												<span class="input-group-text"><i class="ni ni-pin-3"></i></span>
											</div>
											<input class="form-control{{ $errors->has('number') ? ' is-invalid' : '' }}" placeholder="{{ __('Nº') }}" value="{{ old('number') }}" type="text" name="number" id="number">
										</div>
										@if ($errors->has('number'))
										<span class="invalid-feedback" style="display: block;" role="alert">
											<strong>{{ $errors->first('number') }}</strong>
										</span>
										@endif
									</div>
								</div>
								<div class="form-group col-sm-12{{ $errors->has('complement') ? ' has-danger' : '' }}">
									<div class="input-group input-group-alternative mb-3">
										<div class="input-group-prepend">
											<span class="input-group-text"><i class="ni ni-pin-3"></i></span>
										</div>
										<input class="form-control{{ $errors->has('complement') ? ' is-invalid' : '' }}" placeholder="{{ __('Complemento') }}" type="text" name="complement" id="complement" value="{{ old('complement') }}">
									</div>
									@if ($errors->has('complement'))
									<span class="invalid-feedback" style="display: block;" role="alert">
										<strong>{{ $errors->first('complement') }}</strong>
									</span>
									@endif
								</div>
								<div class="form-group row mb-0">
									<div class="col-md-6{{ $errors->has('state_id') ? ' has-danger' : '' }}">
										<div class="input-group input-group-alternative mb-3">
											<select id="filterState" name="state_id" class="dynamic form-control" data-endpoint="api/states" data-labelfield="name" data-valuefield="id" data-current="{{ $state_id ?? old('state_id') }}" data-targetid="filterCity" data-updateevent="updateCitySelect">
												<option value="">Selecione um estado</option>
											</select>
										</div>
									</div>
									<div class="col-md-6{{ $errors->has('city_id') ? ' has-danger' : '' }}">
										<div class="input-group input-group-alternative mb-3">
											@if(isset($state_id))
											<select id="filterCity" name="city_id" class="dynamic form-control" data-endpoint="{{ $state_id?"api/cities/{$state_id}":"" }}" data-labelfield="name" data-valuefield="id" data-current="{{ $city_id ?? old('city_id')  }}">
												@else
												<select id="filterCity" name="city_id" class="dynamic form-control" data-labelfield="name" data-valuefield="id" data-current="{{ $city_id ?? old('city_id') }}">
													@endif
													<option value="">Selecione uma cidade</option>
												</select>
											</select>
										</div>
									</div>

								</div>
							</div>

							<div class="text-center">
								<button type="submit" class="btn btn-primary mt-4">{{ __('Salvar') }}</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

	@include('layouts.footers.auth')
</div>

@push('js')

<script src="/js/poly.js" type="text/javascript" charset="utf-8"></script>
<script src="/js/vendor.js" type="text/javascript" charset="utf-8"></script>
<script src="/js/main.js" type="text/javascript" charset="utf-8"></script>
<script src="/js/jquery.mask.js" type="text/javascript" charset="utf-8"></script>
<script src="/js/buscacep.js" type="text/javascript" charset="utf-8"></script>

<script type="text/javascript">
	$(document).ready(function(){
		$('#cep').mask('99999-999');
	});

	$(document).ready(function(){

		$('.localizacao').hide();
		$('input[type="radio"]').click(function() {
			if($(this).val() == 'adoption') {
				$('.localizacao').hide();
				$('#cep').removeAttr('required');
				$('#logradouro').removeAttr('required');
				$('#complemento').removeAttr('required');
				$('#bairro').removeAttr('required');
				$('#filterState').removeAttr('required');
				$('#filterCity').removeAttr('required');
			} else {
				$('.localizacao').show();
				$('#cep').attr('required', 'required');
				$('#logradouro').attr('required', 'required');
				$('#complemento').attr('required', 'required');
				$('#bairro').attr('required', 'required');
				$('#filterState').attr('required', 'required');
				$('#filterCity').attr('required', 'required');
			}

		});

	});


</script>
@endpush
@endsection