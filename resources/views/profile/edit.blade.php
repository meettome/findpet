@extends('layouts.app', ['title' => __('Edição de Perfil')])

@section('content')
@include('profile.partials.header', [
'title' => __('Olá') . ' '. auth()->user()->name,
'description' => __('Esta é sua Área de perfil. Mantenha os dados atualizados.'),
'class' => 'col-lg-12'
])
<div class="container-fluid mt--7">
    <div class="row">
        <div class="col-xl-4 order-xl-2 mb-5 mb-xl-0">
            <div class="card card-profile shadow">
                <div class="row justify-content-center">
                    <div class="col-lg-8 order-lg-2">
                        <div class="card-profile-image">
                            <a href="#" data-toggle="modal" data-target="#myModal">
                                <img src="{{ auth()->user()->avatarImage }}" class="rounded-circle">
                                <i class="fas fa-pencil-alt mr-1 rounded-circle" style="position: absolute;left: 50%;max-width: 180px;transition: all .15s ease;transform: translate(-50%, -30%);margin-top: 49%;background-color: #31325f;color: #b09a66;padding: 3%;"></i>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="card-header text-center border-0 pt-8 pt-md-4 pb-0 pb-md-4">

                </div>
                <div class="card-body pt-0 mt-7 pt-md-4">

                    <div class="text-center">
                       @if (session('avatar_status'))
                       <div class="alert alert-success alert-dismissible fade show" role="alert">
                         {{ session('avatar_status') }}
                         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                      </button>
                  </div>
                  @endif
                  <h3>{{ auth()->user()->name }}</h3>
                  <div class="h4 font-weight-500">
                    {{ auth()->user()->perfil }}
                </div>

            </div>
        </div>
    </div>
</div>
<div class="col-xl-8 order-xl-1">
    <div class="card bg-secondary shadow">
        <div class="card-header bg-white border-0">
            <div class="row align-items-center">
                <h3 class="col-12 mb-0">{{ __('Editar Perfil') }}</h3>
            </div>
        </div>
        <div class="card-body">
            <form method="post" action="{{ route('profile.update', auth()->user()->id) }}" autocomplete="off">
                @csrf
                @method('put')

                <h6 class="heading-small text-white mb-4">{{ __('Informações Principais') }}</h6>

                @if (session('status'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    {{ session('status') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @endif

                <div class="pl-lg-0">
                    <div class="form-group col mb-3{{ $errors->has('name') ? ' has-danger' : '' }}">
                        <label class="form-control-label" for="input-name">{{ __('Nome') }}</label>
                        <input type="text" name="name" id="input-name" class="form-control form-control-alternative{{ $errors->has('name') ? ' is-invalid' : '' }}" placeholder="{{ __('Nome') }}" value="{{ old('name', auth()->user()->name) }}" required autofocus>

                        @if ($errors->has('name'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group row mb-0">
                        <div class="col-md-6{{ $errors->has('email') ? ' has-danger' : '' }}">
                            <label class="form-control-label" for="input-email">{{ __('Email') }}</label>
                            <div class="input-group input-group-alternative mb-3">

                                <input type="email" name="email" id="input-email" class="form-control form-control-alternative{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="{{ __('Email') }}" value="{{ old('email', auth()->user()->email) }}" required>
                            </div>
                            @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="col-md-6 {{ $errors->has('cpf') ? ' has-danger' : '' }}">
                            <label class="form-control-label" for="input-email">{{ __('CPF') }}</label>
                            <div class="input-group input-group-alternative mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="ni ni-satisfied"></i>
                                    </span>
                                </div>
                                <input class="form-control numCpf {{ $errors->has('cpf') ? ' is-invalid' : '' }}" placeholder="{{ __('CPF') }}" type="text" name="cpf" id="cpf" value="{{ auth()->user()->cpf, old('cpf') }}" required>
                            </div>
                            @if ($errors->has('cpf'))
                            <span class="invalid-feedback" style="display: block;" role="alert">
                                <strong>{{ $errors->first('cpf') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row mb-0">
                        <div class="col-md-4 {{ $errors->has('cellphone') ? ' has-danger' : '' }}">
                            <label class="form-control-label" for="input-email">{{ __('Celular') }}</label>
                            <div class="input-group input-group-alternative mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-icon fa-phone"></i></span>
                                </div>
                                <input class="form-control celular{{ $errors->has('cellphone') ? ' is-invalid' : '' }}" placeholder="{{ __('Celular') }}" type="text" name="cellphone" value="{{ auth()->user()->phone, old('cellphone') }}" >
                            </div>
                            @if ($errors->has('cellphone'))
                            <span class="invalid-feedback" style="display: block;" role="alert">
                                <strong>{{ $errors->first('cellphone') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="col-md-4 {{ $errors->has('birth_date') ? ' has-danger' : '' }}">
                            <label class="form-control-label" for="input-email">{{ __('Data de Nascimento') }}</label>
                            <div class="input-group input-group-alternative mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="fas fa-icon fa-calendar"></i>
                                    </span>
                                </div>
                                <input class="form-control{{ $errors->has('birth_date') ? ' is-invalid' : '' }}" placeholder="{{ __('Data de Nascimento') }}" type="date" name="birth_date" value="{{ auth()->user()->birth_date, old('birth_date') }}"  required>
                            </div>
                            @if ($errors->has('birth_date'))
                            <span class="invalid-feedback" style="display: block;" role="alert">
                                <strong>{{ $errors->first('birth_date') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="col-md-4 {{ $errors->has('sex') ? ' has-danger' : '' }}">
                            <label class="form-control-label" for="input-email">{{ __('Sexo') }}</label>
                            <div class="input-group input-group-alternative mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="ni ni-single-02"></i>
                                    </span>
                                </div>
                                <select class="form-control{{ $errors->has('sex') ? ' is-invalid' : '' }}" name="sex" required>
                                    <option value="">Selecione o Sexo</option>
                                    <option value="{{ 'male' }}" {{ auth()->user()->sex == 'male' ?  "selected='selected'" : '' }}>Masculino</option>
                                    <option value="{{ 'female' }}" {{ auth()->user()->sex == 'female' ?  "selected='selected'" : '' }}>Feminino</option>
                                    <option value="{{ 'other' }}" {{ auth()->user()->sex == 'other' ?  "selected='selected'": '' }}>Prefiro não dizer</option>
                                </select>
                            </div>
                            @if ($errors->has('sex'))
                            <span class="invalid-feedback" style="display: block;" role="alert">
                                <strong>{{ $errors->first('sex') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row mb-0">
                        <div class="col-md-6{{ $errors->has('zip_code') ? ' has-danger' : '' }}">
                            <label class="form-control-label" for="input-email">{{ __('CEP') }}</label>
                            <div class="input-group input-group-alternative mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="ni ni-map-big"></i></span>
                                </div>
                                <input class="form-control{{ $errors->has('zip_code') ? ' is-invalid' : '' }}" placeholder="{{ __('CEP') }}" type="text" name="zip_code" id="cep"  value="@if(auth()->user()->addresses){{auth()->user()->Addresses->zip_code}}@endif"   >
                            </div>
                            @if ($errors->has('zip_code'))
                            <span class="invalid-feedback" style="display: block;" role="alert">
                                <strong>{{ $errors->first('zip_code') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="col-md-6{{ $errors->has('district') ? ' has-danger' : '' }}">
                            <label class="form-control-label" for="input-email">{{ __('Bairro') }}</label>
                            <div class="input-group input-group-alternative mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="ni ni-pin-3"></i></span>
                                </div>
                                <input  value="@if(auth()->user()->addresses){{auth()->user()->addresses->district}}@endif"  class="form-control{{ $errors->has('district') ? ' is-invalid' : '' }}" placeholder="{{ __('Bairro') }}" type="text" name="district" id="bairro" >
                            </div>
                            @if ($errors->has('district'))
                            <span class="invalid-feedback" style="display: block;" role="alert">
                                <strong>{{ $errors->first('district') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row mb-0">
                        <div class="col-md-9{{ $errors->has('street') ? ' has-danger' : '' }}">
                            <label class="form-control-label" for="input-email">{{ __('Logradouro') }}</label>
                            <div class="input-group input-group-alternative mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="ni ni-pin-3"></i></span>
                                </div>
                                <input class="form-control{{ $errors->has('street') ? ' is-invalid' : '' }}" placeholder="{{ __('Logradouro') }}" type="text" name="street" id="logradouro"  value="@if(auth()->user()->addresses) {{ auth()->user()->addresses->street }} @endif"  >
                            </div>
                            @if ($errors->has('street'))
                            <span class="invalid-feedback" style="display: block;" role="alert">
                                <strong>{{ $errors->first('street') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="col-md-3{{ $errors->has('number') ? ' has-danger' : '' }}">
                            <label class="form-control-label" for="input-email">{{ __('Nº') }}</label>
                            <div class="input-group input-group-alternative mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="ni ni-pin-3"></i></span>
                                </div>
                                <input class="form-control{{ $errors->has('number') ? ' is-invalid' : '' }}" placeholder="{{ __('Nº') }}"  value="@if(auth()->user()->addresses) {{ auth()->user()->addresses->number }}@endif"  type="text" name="number" id="number">
                            </div>
                            @if ($errors->has('number'))
                            <span class="invalid-feedback" style="display: block;" role="alert">
                                <strong>{{ $errors->first('number') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group mb-0 col{{ $errors->has('complement') ? ' has-danger' : '' }}">
                        <label class="form-control-label" for="input-email">{{ __('Complemento') }}</label>
                        <div class="input-group input-group-alternative mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="ni ni-pin-3"></i></span>
                            </div>
                            <input class="form-control{{ $errors->has('complement') ? ' is-invalid' : '' }}" placeholder="{{ __('Complemento') }}" type="text" name="complement" id="complement"  value="@if(auth()->user()->addresses){{ auth()->user()->addresses->complement }}@endif"  >
                        </div>
                        @if ($errors->has('complement'))
                        <span class="invalid-feedback" style="display: block;" role="alert">
                            <strong>{{ $errors->first('complement') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6{{ $errors->has('state_id') ? ' has-danger' : '' }}">
                            <label class="form-control-label" for="input-email">{{ __('Estado') }}</label>
                            <div class="input-group input-group-alternative mb-3">
                                <select id="filterState" name="state_id" class="dynamic form-control" data-endpoint="api/states" data-labelfield="name" data-valuefield="id" @if(auth()->user()->addresses)data-current="{{ auth()->user()->addresses->state_id }}" @else data-current="{{ '' }}" @endif data-targetid="filterCity" data-updateevent="updateCitySelect">
                                    <option value="">Selecione um estado</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6{{ $errors->has('city_id') ? ' has-danger' : '' }}">
                            <label class="form-control-label" for="input-email">{{ __('Cidade') }}</label>
                            <div class="input-group input-group-alternative mb-3">
                                @if(isset($state_id))
                                <select id="filterCity" name="city_id" class="dynamic form-control" data-endpoint="{{ $state_id ?'api/cities/{$state_id}':'' }}" data-labelfield="name" data-valuefield="id" data-current="@if(auth()->user()->addresses) {{ auth()->user()->addresses->city_id }} @endif">
                                    @else
                                    <select id="filterCity" name="city_id" class="dynamic form-control" data-labelfield="name" data-valuefield="id" data-current="@if(auth()->user()->addresses) {{ auth()->user()->addresses->city_id }} @endif">
                                        @endif
                                        <option value="">Selecione uma cidade</option>
                                    </select>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="text-center">
                        <button type="submit" class="btn btn-primary mt-4">{{ __('Salvar') }}</button>
                    </div>
                </div>
            </form>
            <hr class="my-4" />
            <form method="post" action="{{ route('profile.password') }}" autocomplete="off">
                @csrf
                @method('put')

                <h6 class="heading-small text-white mb-4">{{ __('Senha') }}</h6>

                @if (session('password_status'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    {{ session('password_status') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @endif

                <div class="pl-lg-0">
                    <div class="form-group col mb-3{{ $errors->has('old_password') ? ' has-danger' : '' }}">
                        <label class="form-control-label" for="input-current-password">{{ __('Senha atual') }}</label>
                        <input type="password" name="old_password" id="input-current-password" class="form-control form-control-alternative{{ $errors->has('old_password') ? ' is-invalid' : '' }}" placeholder="{{ __('Senha atual') }}" value="" required>

                        @if ($errors->has('old_password'))
                        <span class="invalid-feedback" role="alert">
                            a<strong>{{ $errors->first('old_password') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group col mb-3{{ $errors->has('password') ? ' has-danger' : '' }}">
                        <label class="form-control-label" for="input-password">{{ __('Nova senha') }}</label>
                        <input type="password" name="password" id="input-password" class="form-control form-control-alternative{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="{{ __('Nova senha') }}" value="" required>

                        @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group col mb-3">
                        <label class="form-control-label" for="input-password-confirmation">{{ __('Confirme a nova senha') }}</label>
                        <input type="password" name="password_confirmation" id="inpu
                        t-password-confirmation" class="form-control form-control-alternative" placeholder="{{ __('Confirme a nova senha') }}" value="" required>
                    </div>

                    <div class="text-center">
                        <button type="submit" class="btn btn-primary mt-4">{{ __('Modificar senha') }}</button>
                    </div>
                </div>
            </form><hr class="my-4" />
            <form method="post" action="{{ route('profile.social') }}" autocomplete="off">
                @csrf
                @method('put')

                <h6 class="heading-small text-white mb-4">{{ __('Rede Sociais') }}</h6>

                <div class="pl-lg-0">
                    @foreach(auth()->user()->SocialNetworks as $social)
                    <div class="form-group row">
                        <div class="col-md-6">
                            <div class="input-group input-group-alternative">
                                <select id="filterType" name="type_id[]" class="dynamic form-control" data-endpoint="api/typesnetworks" data-labelfield="type" data-valuefield="id" data-current = "{{$social->type_id}}" data-targetid="filterType">
                                    <option value="">Selecione uma Rede Social</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 usuario">
                            <div class="input-group input-group-alternative">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="{{$social->type->icon}}"></i></span>
                                </div>
                                <input class="form-control" type="text" placeholder="Digite o Usuario" name="user[]" value="{{$social->user}}">
                            </div>
                        </div>
                    </div>
                    @endforeach
                    <h6 class="heading-small text-white mb-4">{{ __('Adicionar uma nova?') }}</h6>
                    <div class="form-group row mb-0" id="origem">

                        <div class="col-md-5">
                            <div class="input-group input-group-alternative mb-3">
                                <select id="filterType" name="newtype_id[]" class="dynamic form-control" data-endpoint="api/typesnetworks" data-labelfield="type" data-valuefield="id" data-targetid="filterType">
                                    <option value="">Selecione uma Rede Social</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 usuario">
                            <div class="input-group input-group-alternative mb-3">
                                <input class="form-control" type="text" placeholder="Digite o Usuario" name="newuser[]">
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="input-group input-group-alternative mb-3  justify-content-center">
                                <i class="fas fa-plus-circle adicionar" style="cursor: pointer;" onclick="duplicarCampos();" title="Adicionar"></i>
                                <i class="fas fa-minus-circle remover" title="Remover" style="cursor: pointer;" onclick="removerCampos(this);"></i>
                            </div>
                        </div>
                    </div>
                    <div id="destino">
                    </div>

                    <div class="text-center">
                        <button type="submit" class="btn btn-primary mt-4">{{ __('Modificar Rede Social') }}</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</div>

<!-- Modal p/ Atualização de Avatar -->
<div class="modal" id="myModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Atualização de Imagem</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <form action="{{route('profile.updateAvatar')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <input type="file" name="photo" class="form-control">
                    <div class="text-center">
                        <button type="submit" class="btn btn-primary mt-4">Atualizar</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>

@include('layouts.footers.auth')
</div>

@push('js')

<script src="/js/poly.js" type="text/javascript" charset="utf-8"></script>
<script src="/js/vendor.js" type="text/javascript" charset="utf-8"></script>
<script src="/js/main.js" type="text/javascript" charset="utf-8"></script>
<script src="/js/jquery.mask.js" type="text/javascript" charset="utf-8"></script>
<script src="/js/buscacep.js" type="text/javascript" charset="utf-8"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $('.celular').mask('(99) 99999-9999');
        $('.telefone').mask('(99) 9999-9999');
        $('#cpf').mask('999.999.999-99');
        $('#cnpj').mask('99.999.999/9999-99');
        $('#cep').mask('99999-999');
    });

    function duplicarCampos(){
        var clone = document.getElementById('origem').cloneNode(true);
        var destino = document.getElementById('destino');
        destino.appendChild (clone);

        var camposClonados = clone.getElementsByTagName('input');

        for(i=0; i<camposClonados.length;i++){
            camposClonados[i].value = '';
        }                
    }

    function removerCampos(id){
        var node1 = document.getElementById('destino');
        node1.removeChild(node1.childNodes[0]);
    }

</script>
@endpush
@endsection
