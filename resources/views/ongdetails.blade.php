@extends('layouts.guestapp', ['class' => 'bg-white',
'menu' =>'bg-transparent',  'itens' => 'text-white', 'title' => 'Detalhes da Ong'])

@section('content')

@include('layouts.headers.banner', ['descricao' => '', 'banner' => 'bg-background-image'])
<style type="text/css">
    .carousel-indicators li {
        position: relative;
        width: 30px;
        height: 3px;
        margin-right: 3px;
        margin-left: 3px;
        cursor: pointer;
        text-indent: -999px;
        background-color: #32325d;
        flex: 0 1 auto;
    }
    .carousel-indicators .active {
        background-color: #f5365c;
    }
</style>
<div class="container-fluid mt--7">
    <div class="row">
        <div class="col-md-6 order-xl-1">
            <div class="bg-secondary shadow pt-3 pb-3">


                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" class=active></li>
                    </ol>
                    <div class="carousel-inner">

                        <div class="carousel-item active">
                            <p  class="d-block w-100" style="background: url('/storage/avatar/{{$ongs->perfil->id}}/{{$ongs->perfil->avatar->filename}}'); background-size: contain; background-color: transparent; background-position: center; background-repeat: no-repeat; padding: 50%; margin-bottom: 0;" alt="{{ $ongs->name }}" ></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 order-xl-1">
            <div class="card shadow pt-4 pb-4">
                @if (session('status'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    {{ session('status') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @endif
                <h2 class="text-center mt-2 mb-3">Doações</h2>
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="mb-1">Dados: {{ __($ongs->donate->account) }}</h4>
                        <h4 class="mb-1">Link: {{ __($ongs->donate->url) }}</h4>
                    </div>

                </div>
                <div class="row justify-content-center form-group">
                    <div class="col-md-12">
                        <h2 class="mt-3 mb-2 text-center">Dados da Ong</h2>
                    </div>
                </div>
                <div class="row justify-content-center form-group">
                    <div class="col-md-12">
                        <h4 class="mb-2 ">Nome: {{$ongs->name}}</h4>
                        <h4 class="mb-2">Celular: {{$ongs->cellphone}}</h4>
                        <h4 class="mb-2">Telefone: {{$ongs->phone}}</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <h4 class="mb-1">Logradouro: {{ __($ongs->addresses->street) }}</h4>
                        <h4 class="mb-1">Bairro: {{ __($ongs->addresses->district) }}</h4>
                        <h4 class="mb-1">Numero: {{ __($ongs->addresses->number) }}</h4>
                        <h4 class="mb-1">Complemento: {{ __($ongs->addresses->complement) }}</h4>
                    </div>
                    <div class="col-md-6">
                        <h4 class="mb-1">Cep: {{ __($ongs->addresses->zip_code) }}</h4>
                        <h4 class="mb-1">Cidade: {{ __($ongs->addresses->city->name) }}</h4>
                        <h4 class="mb-1">Estado: {{ __($ongs->addresses->state->name) }}</h4>
                    </div>
                </div>

                <div class="row justify-content-center form-group">
                    <div class="col-md-12">
                        <h2 class="mt-1 mb-2 text-center">Redes Sociais</h2>
                    </div>
                </div>
                <div class="row">
                    <?php $i = 1 ?>
                    @foreach($ongs->SocialNetworks as $social)
                    <div class="col-md-6">
                        <h4><i class="{{_($social->type->icon)}}"></i> @if($social->type->id == 16 || $social->type->id == 17 || $social->type->id == 14) {{_($social->phone)}} @else {{_($social->user)}} @endif</h4>
                    </div>
                    @if($i%2==0)
                </div>
                <div class="row form-group">
                    @endif
                    <?php $i++ ?>
                    @endforeach

                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-12">
                    @foreach($whatsapp as $whats)
                    <div class="mb-2">
                        <a class="btn btn-success" href="https://api.whatsapp.com/send?phone=55{{$whats->whatsapp}}" style="width: 100%" target="_blank"><i class="{{$whats->type->icon}}"></i> Ir para o whatsapp</a>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>

</div>


@endsection
