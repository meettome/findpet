import React, { Component, Fragment} from 'react';
import { render } from 'react-dom';
import { snakeToCamel, camelToSnake, getRangeData, getFieldData, sanitizeUrl } from 'common/functions';
import PropTypes from 'prop-types';
import axios from 'axios';

import SearchForm from 'components/SearchForm';
import PropertyViewer from 'components/PropertyViewer';

import 'sass/app.scss';


export default class App extends Component {
	static propTypes = {
		apiBaseUrl: PropTypes.object.isRequired,
		name: PropTypes.string.isRequired,
		stateId: PropTypes.number,
		cityId: PropTypes.number,
		districtName: PropTypes.string,
		entityId: PropTypes.number,
		propertyName: PropTypes.string,
		address: PropTypes.object,
		meterageRange: PropTypes.object,
		priceRange: PropTypes.object,
		requirePictures: PropTypes.bool,
		stateData: PropTypes.array,
		cityData: PropTypes.array,
		entityData: PropTypes.array,
		propertyData: PropTypes.array,
	}

	static defaultProps = {
		apiBaseUrl:sanitizeUrl(process.env.APP_URL+'/api'),
		name:'searchForm',
		entity: null,
		propertyName: null,
		address: null,
		meterageRange: {min:70,max:300},
		priceRange: {min:0,max:5000000},
		requirePictures:false
	}

	constructor(props) {
		super(props);

		this.state={
			state: this.props.stateId,
			city: this.props.cityId,
			district: this.props.districtName,
			entity: this.props.entityId,
			propertyName: this.props.propertyName,
			meterageRange: this.props.meterageRange,
			priceRange: this.props.priceRange,
			property: null,
		}
	}
/*
	stateData: this.props.stateData,
	cityData: this.props.cityData,
	entityData: this.props.entityData,
	propertyData: this.props.propertyData,

 */
	ViewProperty(property){
		let url = new URL(window.location.href);
		url.pathname='empreendimentos';
		url.hash = property.url??'search';

		window.open(url.href, '_top');
	}

	componentDidMount() {
		window.addEventListener('hashchange', function() {
			document.location.reload(true);
		}, false);
	}

	render() {
		let {property, ...formProps } = this.state;
		let { stateData, cityData, entityData, propertyData } = this.props;
		let url = new URL(window.location.href);

		property = propertyData.find(prop=>prop.url.replace(/\#/g, '') == url.hash.replace(/\#/g, ''));
		if(property){
			property.cost = parseFloat(property.cost);
			property.metreage = parseFloat(property.metreage);
		}

		if(['search',''].includes(url.hash) || !property)
			return <SearchForm {...formProps} {...{stateData, cityData, entityData, propertyData}} ViewPropertyEvent={this.ViewProperty.bind(this)} />
		else
			return <PropertyViewer {...property} ViewPropertyEvent={this.ViewProperty.bind(this, null)} />
	}
}


let node = document.querySelector('#searchForm'), attrs={};

if(node)
	for (let i = node.attributes.length - 1; i>0; i--){
		if(node.attributes[i].name.match(/^data-/)){
			//get attr Name
			let attrName = snakeToCamel(node.attributes[i].name.replace(/^data-/,''));
			//get attr value
			let attrValue = node.attributes[i].value;
			if(attrName.match(/Id$/)){
				attrs[attrName] = parseInt(attrValue);
			} else if(attrName.match(/Data/)){
				attrs[attrName] = JSON.parse(attrValue);
			} else if(attrName.match(/Range/)){
				//get the range, min or max
				let range=getRangeData(attrName, false);
				attrName = getRangeData(attrName);
				//is it already in our attrs?
				if(attrs[attrName]){
					attrs[attrName][range] = parseFloat(attrValue);
				} else {
					let value = {};value[range] = parseFloat(attrValue);
					attrs[attrName] = value;
				}
			} else if(attrName.match(/apiBaseUrl/)) {
				attrs[attrName] = sanitizeUrl(attrValue);
			} else if(attrName.match(/address/)) {
				let value = attrs[getFieldData(attrName)] || {};
				let sub = getFieldData(attrName, false)
				switch(sub){
					case "street":
						value[sub] = attrValue;
						break;
					case "number":
					case "state_id":
					case "city_id":
						value[sub] = parseInt(attrValue);
						break;

				}
				attrs[getFieldData(attrName)]=value;
			} else {
				switch(attrName){
					case 'entity':
						attrs[attrName] = parseInt(attrValue);
						break;
					case 'requirePictures':
						attrs[attrName] = ['1', 1, 'true', true].includes(attrValue);
						break;
					default:
						attrs[attrName] = attrValue;
				}
			}
			node.removeAttribute(node.attributes[i].name);
		}
	}

if(!attrs.stateData || !attrs.cityData || !attrs.entityData || !attrs.propertyData)
	axios.get(process.env.APP_URL+'/api/search/data')
	//axios.get('https://sistema.imovelemaisnegocio.com.br/api/search/data')
		.then(response=>{
			let {stateData, cityData, entityData, propertyData } = response.data;

			attrs = Object.assign(attrs, {stateData, cityData, entityData, propertyData});

			render(<App {...attrs} />, node);
		})
		.catch(error=>{
			console.log(error);
		});
else
	render(<App {...attrs} />, node);
