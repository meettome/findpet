
$(document).ready(function(){
    function limpa_formulário_cep() {
        $("#logradouro").val("");
        $("#bairro").val("");
        $("#cidade").val("");
        $("#uf").val("");
        $("#complemento").val("");
    }

    $("#cep").blur(function() {
        var cep = $(this).val().replace(/\D/g, '');

        if (cep != "") {
            var validacep = /^[0-9]{8}$/;

            if(validacep.test(cep)) {
                $("#logradouro").val("...");
                $("#bairro").val("...");
                $("#cidade").val("...");
                $("#uf").val("...");
                $("#complemento").val("...");
                $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                    if (!("erro" in dados)) {
                        $("#logradouro").val(dados.logradouro);
                        $("#bairro").val(dados.bairro);
                        switch (dados.uf) {
                            case "AC" : data = "Acre";                  break;
                            case "AL" : data = "Alagoas";               break;
                            case "AM" : data = "Amazonas";              break;
                            case "AP" : data = "Amapá";                 break;
                            case "BA" : data = "Bahia";                 break;
                            case "CE" : data = "Ceará";                 break;
                            case "DF" : data = "Distrito Federal";      break;
                            case "ES" : data = "Espírito Santo";        break;
                            case "GO" : data = "Goiás";                 break;
                            case "MA" : data = "Maranhão";              break;
                            case "MG" : data = "Minas Gerais";          break;
                            case "MS" : data = "Mato Grosso do Sul";    break;
                            case "MT" : data = "Mato Grosso";           break;
                            case "PA" : data = "Pará";                  break;
                            case "PB" : data = "Paraíba";               break;
                            case "PE" : data = "Pernambuco";            break;
                            case "PI" : data = "Piauí";                 break;
                            case "PR" : data = "Paraná";                break;
                            case "RJ" : data = "Rio de Janeiro";        break;
                            case "RN" : data = "Rio Grande do Norte";   break;
                            case "RO" : data = "Rondônia";              break;
                            case "RR" : data = "Roraima";               break;
                            case "RS" : data = "Rio Grande do Sul";     break;
                            case "SC" : data = "Santa Catarina";        break;
                            case "SE" : data = "Sergipe";               break;
                            case "SP" : data = "São Paulo";             break;
                            case "TO" : data = "Tocantíns";             break;
                        }

                        var estado = data.trim();
                        var opt = $('#filterState option').filter(function() {
                            return $(this).text().trim() === estado;
                        });
                        opt.attr('selected', true);

                        var cidade = dados.localidade.trim();

                        var opt = $('#filterCity option').filter(function() {
                            return $(this).text().trim() === cidade;
                        });

                        opt.attr('selected', true);

                        $("#number").focus();
                        $("#complemento").val("");
                    }else {
                        limpa_formulário_cep();
                        alert("CEP não encontrado.");
                    }
                });
            }else {
                limpa_formulário_cep();
                alert("Formato de CEP inválido.");
            }
        }else {
            limpa_formulário_cep();
        }

    });
});