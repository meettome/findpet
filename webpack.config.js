const webpack = require('webpack');
const Dotenv = require('dotenv');
const fs = require('fs'); // to check if the file exists
const TerserPlugin = require('terser-webpack-plugin');

const path = require('path'); // to get the current path

const BUILD_DIR = path.resolve(__dirname, '.\\public\\js');
const APP_DIR = path.resolve(__dirname, '.\\src\\jsx');

module.exports = (env) => {

	const currentPath = path.join(__dirname);
	const basePath = currentPath + '\\.env';
	const envPath = basePath + '.' + env.ENVIRONMENT;
	const finalPath = fs.existsSync(envPath) ? envPath : basePath;

	const fileEnv = Dotenv.config({ path: finalPath }).parsed;
	const EnvKeys = Object.keys(fileEnv).reduce((prev, next) => {
		prev[`process.env.${next}`] = JSON.stringify(fileEnv[next]);
		return prev;
	}, {});

	let commands=[];

	return {
		watchOptions: {
			aggregateTimeout: 300,
			//poll: 1000,
			ignored: /(node_modules|vendor)/
		},
		entry: {
			poly:"@babel/polyfill",
			main:APP_DIR + '\\main.jsx',
		},
		output: {
			path: BUILD_DIR,
			filename: '[name].js',
			chunkFilename: 'vendor.js'
		},
		resolve:{
			extensions: [".js",".jsx",".json"],
			alias:{
				admin: path.resolve('src\\jsx\\admin\\'),
				components: path.resolve('src\\jsx\\components\\'),
				models: path.resolve('src\\jsx\\models\\'),
				public: path.resolve('src\\jsx\\public\\')
			}
		},
		optimization: {
			splitChunks: {
				cacheGroups: {
					vendor: {
						chunks: 'initial',
						name: 'vendor',
						test: /node_modules/,
						enforce: true
					},
				}
			},
			minimizer: [
				new TerserPlugin({
					terserOptions: {
						output: {
							comments: false
							}
						}
					})
			]
		},
		module : {
		rules : [
				{
					test : /\.jsx/,
					include : APP_DIR,
					exclude : /(node_modules|vendor|bower_components)/,
					loader : 'babel-loader',
					options : {
						"presets" : ["@babel/preset-env", "@babel/preset-react"],
						"plugins" : [
							"@babel/plugin-proposal-class-properties",
							"@babel/plugin-proposal-export-default-from",
						]
					}
				},
				{
					test: /\.css$/i,
					use: ['style-loader', 'css-loader'],
				}
			]
		},
		plugins:[
			new webpack.DefinePlugin(EnvKeys)
		]
	}
}
